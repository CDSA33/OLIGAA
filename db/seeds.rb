#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
# For settings
Website.create() unless Website.count > 0

# Fixed menus
fixed_menus = [
  { title: "Accueil", uid: "home"},
  { title: "Calendrier", uid: "calendar"},
  { title: "Lettres d'information", uid: "newsletters"},
  { title: "Activités", uid: "activity_categories"},
  { title: "Où pratiquer", uid: "member_groups"},
  { title: "Zone membre", uid: "member_zone"},
  { title: "Pied de page", uid: "footer"},
  { title: "Info des clubs Sport Adapté Girondins", uid: "club_novels"}
]
fixed_menus.each do |fixed_menu|
  unless Menu.find_by_uid(fixed_menu[:uid])
    Menu.create!(title: fixed_menu[:title], uid: fixed_menu[:uid])
  end
end
