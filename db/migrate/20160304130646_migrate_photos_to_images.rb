#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class MigratePhotosToImages < ActiveRecord::Migration
  def up
    base_path = Dir.pwd + "/public/system/photos"
    # Migrate
    say_with_time "Cleaning up orphaned newsletter items" do
      (NewsletterItem.all - NewsletterItem.where(newsletter_id: Newsletter.all)).each { |i| i.destroy }
    end
    say_with_time "Moving old photos to new images" do
      NewsletterItem.where('photo_file_name is not null').each do |item|
        file_name = item.photo_file_name
        file_path = "#{base_path}/#{item.id}/original/#{file_name}"
        img_name = file_name.chomp(file_name.scan(/\.\w+\z/).first)
        img = Image.new(title: img_name)
        begin
          img.img_file = File.open(file_path)
          img.save
          item.images << img
        rescue Errno::ENOENT
          puts "#{file_path} does not exist..."
        end
      end
    end
    # Cleanup
    say_with_time "Deleting useless photos directory" do
      FileUtils.rm_rf(base_path)
    end
    remove_column :newsletter_items, :photo_file_name
    remove_column :newsletter_items, :photo_content_type
    remove_column :newsletter_items, :photo_file_size
    remove_column :newsletter_items, :photo_updated_at
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
