#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class RefactorMemberGroupsSettingsRelation < ActiveRecord::Migration[5.1]
  def up
    website = Website.first
    create_table :member_group_setting_bases do |t|
      t.integer :member_group_id
      t.integer :website_id
      t.string :type
      t.timestamps
    end
    map_mgs = MemberGroup.where(website_id: website.id)
    website.map_member_groups << map_mgs.to_a
    remove_column :member_groups, :website_id
  end

  def down
    website = Website.first
    add_column :member_groups, :website_id, :integer
    map_mgs = website.map_member_groups.ids
    MemberGroup.where(id: map_mgs).update_all(website_id: website.id)
    drop_table :member_group_setting_bases
  end
end
