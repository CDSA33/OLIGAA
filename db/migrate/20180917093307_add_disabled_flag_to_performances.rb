class AddDisabledFlagToPerformances < ActiveRecord::Migration[5.1]
  def change
    add_column :performances, :disabled, :boolean, default: false
  end
end
