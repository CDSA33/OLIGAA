#!/bin/bash

if [ $LEADER ]; then
	# Install dependencies
	bundle check || bundle install || exit 1

	# Cleanup
	rm -f tmp/pids/server.pid
	# Prepare DB
	bundle exec rails db:setup
else
	# Wait for it…
	until curl --output /dev/null --silent --head --fail "app:3000"; do
	sleep 1
	done
fi

# Start
exec "$@"
