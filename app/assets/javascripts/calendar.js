/*
 * OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
 * des Associations" which is an online tool for associations to manage news,
 * actors and activities.
 * Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
 * 33) <contact@cdsa33.org>
 *
 * This file is part of OLIGAA.
 *
 * OLIGAA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * OLIGAA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
 */
function configure_calendar() {
  var cal = $('#calendar');
  var events = cal.data('events');
  var month = cal.data('year')+'-'+cal.data('month');
  var default_date = month+'-01';
  var currentMonth = moment(month);
  var previousMonth = currentMonth.clone().add(-1, 'months');
  var nextMonth = currentMonth.clone().add(1, 'months');

  cal.fullCalendar({
    locale: 'fr',
    timeFormat: 'HH:mm',
    defaultDate: default_date,
    weekNumbers: true,
    customButtons: {
      previousMonthButton: {
        text: '< '+previousMonth.format('MMMM'),
        click: function() {
          window.location.href = '/calendar/'+previousMonth.year()+'/'+(previousMonth.month()+1);
        }
      },
      nextMonthButton: {
        text: nextMonth.format('MMMM')+' >',
        click: function() {
          window.location.href = '/calendar/'+nextMonth.year()+'/'+(nextMonth.month()+1);
        }
      }
    },
    header : {
      left: 'previousMonthButton',
      center: 'title',
      right: 'nextMonthButton'
    },
    events: events
  })
}
