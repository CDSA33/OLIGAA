/*
 * OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
 * des Associations" which is an online tool for associations to manage news,
 * actors and activities.
 * Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
 * 33) <contact@cdsa33.org>
 *
 * This file is part of OLIGAA.
 *
 * OLIGAA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * OLIGAA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
 */

$(document).ready(function() {
  var medicoSocialOrgIdSelector = "select[name='subscription[adherent_attributes][medico_social_org_id]']";
  var customMedicoSocialOrgSelector = "input[name='subscription[adherent_attributes][other_medicosocial_org]']";
  var performanceIdsSelector = "input[name='subscription[performance_ids][]']";
  var subscriptionOptionDivSelector = "div#subscription-option";
  var submitButtonSelector = "input[type='submit']";

  var toggleOtherMedicosoacialStructureField = function(selected) {
    var field = "div.subscription_adherent_other_medicosocial_org"
    selected == "" ? expand_field(field) : close_field(field);
  }

  var refreshSubscriptionOption = function() {
    var params = {
      medicosocial_org_id: $(medicoSocialOrgIdSelector).val(),
      other_medicosocial_org: $(customMedicoSocialOrgSelector).val()
    };

    toggleOtherMedicosoacialStructureField(params.medicosocial_org_id);

    var performance_ids = [];
    $(performanceIdsSelector+":checked").each(function(index) {
      performance_ids.push($(this).val())
    });
    params.performance_ids = performance_ids;
    $(subscriptionOptionDivSelector).load(refreshSubscriptionOptionPath+'?'+$.param(params), function() {
      var optionAvailability = $(subscriptionOptionDivSelector+" > ul").data("option-available");
      $(submitButtonSelector).prop("disabled",!optionAvailability);
    });
  }

  if ($(subscriptionOptionDivSelector).length) {
    refreshSubscriptionOption();
  }

  $(medicoSocialOrgIdSelector).change(refreshSubscriptionOption);
  $(performanceIdsSelector).change(refreshSubscriptionOption);
})
