/*
 * OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
 * des Associations" which is an online tool for associations to manage news,
 * actors and activities.
 * Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
 * 33) <contact@cdsa33.org>
 *
 * This file is part of OLIGAA.
 *
 * OLIGAA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * OLIGAA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
 */

$(function () {

	var $form = $('form')
	var $recipients_by_contact_types_checkbox = $('#email_recipients_by_contact_types')
	var $recipients_by_member_groups = $('#recipients_by_member_groups')
	var $recipients_by_contact_types = $('#recipients_by_contact_types')

	$("div#recipients").on( "click", "a.expendable", function(event) {
    toggle_link($(this), event);
	});
	$("div#recipients").on( "click", "a.checkable", function(event) {
    check_link($(this), event);
	});

	$.get(recipients_by_member_groups_path).done(function (data) {
		$recipients_by_member_groups.html(data);
		display_selected_contacts_in($recipients_by_member_groups);
	});

	$.get(recipients_by_contact_types_path).done(function (data) {
		$recipients_by_contact_types.html(data);
		display_selected_contacts_in($recipients_by_contact_types);
	});

	$recipients_by_contact_types_checkbox.switchButton({
		height: 15,
		width: 40,
		off_label: 'Groupes de membres',
		on_label: 'Types de contacts'
	}).change(function (event) {
		var by_contact_types = $(this).prop("checked");

		if (by_contact_types) {
			$recipients_by_member_groups.hide()
			$recipients_by_contact_types.show()
		}
		else {
			$recipients_by_contact_types.hide()
			$recipients_by_member_groups.show()
		}
	}).change()

	var $other_recipients_checkbox = $('#email_other_recipients_enabled')
	var $other_recipients_text_area = $('#email_other_recipients')

	$other_recipients_checkbox.switchButton({
		height: 15,
		width: 40,
		off_label: '',
		on_label: 'Autres destinataires'
	}).change(function () {
		var other_recipients_enabled = $(this).prop("checked");

		if (other_recipients_enabled) {
			$other_recipients_text_area.show()
		}
		else {
			$other_recipients_text_area.hide()
		}
	}).change();

	$form.submit(function () {
		var by_contact_types = $recipients_by_contact_types_checkbox.prop("checked");

		if (by_contact_types) {
			$recipients_by_member_groups.find(':checked').prop("checked", false);
		}
		else {
			$recipients_by_contact_types.find(':checked').prop("checked", false);
		}

		var other_recipients_enabled = $other_recipients_checkbox.prop("checked");

		if (!other_recipients_enabled) {
			$other_recipients_text_area.text('');
		}
	})
})
