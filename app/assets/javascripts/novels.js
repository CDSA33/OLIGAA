/*
 * OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
 * des Associations" which is an online tool for associations to manage news,
 * actors and activities.
 * Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
 * 33) <contact@cdsa33.org>
 *
 * This file is part of OLIGAA.
 *
 * OLIGAA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * OLIGAA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
 */

function resizeHomeNovels() {
  $("#home_news #news > li").each(function () {
    var collapsedHeight = '8em'
    var container = $(this)
    container.height(collapsedHeight)

    var content = container.children('.content')

    if (content.height() > container.height()) {
      var thumb = container.children('.thumb')
      // thumb.height(container.height())

      var more_info = content.children('.more-info')
      more_info.click(toggleContainer(container, content, container.height(), more_info))
      more_info.show();
    }
  });
}

function toggleContainer(container, content, collapsedHeight, toggler) {
  showExpandingToggler(toggler);

  return function (event) {
    event.preventDefault()

    if (content.height() > container.height()) {
      container.css('height', 'auto')
      showCollapsingToggler(toggler)
    }
    else {
      container.height(collapsedHeight)
      showExpandingToggler(toggler)
    }
  }
}

function showExpandingToggler(toggler) {
  var togglerWording = 'En savoir plus...'

  toggler.children('span').text(togglerWording)
  toggler.css('cursor','s-resize');
}

function showCollapsingToggler(toggler) {
  var togglerWording = 'En voir moins...'

  toggler.children('span').text(togglerWording)
  toggler.css('cursor','n-resize');
}
