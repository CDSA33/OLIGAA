#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class Email < ApplicationRecord
  attr_accessor :recipients_by_contact_types
  attr_accessor :other_recipients_enabled

  belongs_to :novel
  accepts_nested_attributes_for :novel
  belongs_to :member

  validates_presence_of :sender
  validates_presence_of :recipients, unless: ->(email) { email.other_recipients.present? }

  default_scope { order("sent_at DESC") }
  scope :unsent, -> { where(sent_at: nil) }
  scope :sent, -> { where("sent_at IS NOT NULL") }

  before_save :sanitize_other_recipients

  def self.per_page
    20
  end

  def member_name
    member ? member.name : "Inconnu"
  end

  def contacts
    contacts = []
    array_of_recipients.each do |email|
      contacts << Contact.where(email: email)
    end
    contacts.flatten!.uniq unless contacts.empty?
    contacts
  end

  def unused_contacts
    if self.recipients.blank?
      Contact.all
    else
      Contact.where.not(id: self.contacts.map(&:id))
    end
  end

  def all_recipients
    unless other_recipients.empty?
      all_recipients = array_of_recipients
      other_recipients.gsub(/\s/,'').split(/,/).each do |recipient|
        all_recipients << recipient unless all_recipients.include?(recipient)
      end
      all_recipients
    else
      array_of_recipients
    end
  end

  def send_off
    delivered = self.all_recipients.collect { |recipient| result = Mailer.delay.send_info(self, recipient) }
    raise "All e-mails could not be delivered." unless delivered.all? { |mail| !mail.nil? }

    self.sent_at = Time.now
    self.save
  end

  private

  def sanitize_other_recipients
    unless self.other_recipients.empty?
      self.other_recipients = self.other_recipients.scan(/[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/i).join(', ')
    end
  end

  def array_of_recipients
    recipients ? recipients.gsub(/\s/,'').split(/,/) : []
  end

end
