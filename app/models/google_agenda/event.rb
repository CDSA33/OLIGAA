#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
module GoogleAgenda
  class Event

    def self.find_by_id_or_create(id, calendar_id)
      return self.new if id.nil? || calendar_id.nil?

      if api_client = GoogleAgenda::Service.instance.api_client
        calendar_api = GoogleAgenda::Service.instance.calendar_api

        result = api_client.execute(api_method: calendar_api.events.get, parameters: {
          'calendarId' => calendar_id,
          'eventId' => id
        })

        self.new(JSON.parse(result.body))
      else
        raise "No GA service available to find/create event #{id} on calendar #{calendar_id}."
      end
    end

    def initialize(data = {})
      @data = data
    end

    def update record
      self.destroy

      self.save record
    end

    def save record
      return nil if record.begin_at.nil? || record.end_at.nil?
      return nil unless calendar_id = record.event_type.ga_cal_id

      if api_client = GoogleAgenda::Service.instance.api_client
        calendar_api = GoogleAgenda::Service.instance.calendar_api

        result = api_client.execute(api_method: calendar_api.events.insert,
          parameters: {
            'calendarId' => calendar_id
          },
          body: {
            summary: record.title,
            description: record.description,
            location: record.location,
            start: { dateTime: record.begin_at.to_datetime.rfc3339 },
            end: { dateTime: record.end_at.to_datetime.rfc3339 },
          }.to_json,
          headers: {'Content-Type' => 'application/json'})

        data = JSON.parse(result.body)
        return data['id'], data['organizer']['email']
      else
        raise "No GA service available to delete event #{id} on calendar #{calendar_id}."
      end

    end

    def destroy
      return nil unless id = @data['id']

      calendar_id = @data['organizer']['email']

      if api_client = GoogleAgenda::Service.instance.api_client
        calendar_api = GoogleAgenda::Service.instance.calendar_api

        result = api_client.execute(api_method: calendar_api.events.delete, parameters: {
          'calendarId' => calendar_id,
          'eventId' => id
        })
      else
        raise "No GA service available to delete event #{id} on calendar #{calendar_id}."
      end
    end

  end
end