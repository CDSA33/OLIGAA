#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class Logo < ApplicationRecord
  belongs_to :page
  has_attached_file :img, styles: { thumb: "100x86", midi: "330x248", maxi: "760x570"}, convert_options: { all: '-strip -colorspace RGB'}
  validates_attachment_size :img, less_than: 1.megabyte, message: " : doit être inférieur à 1 Mo"
  do_not_validate_attachment_file_type :img

  scope :displayed, -> { where(displayed: true) }
end
