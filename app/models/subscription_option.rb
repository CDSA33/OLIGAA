#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class SubscriptionOption

  def initialize(medicosocial_org,performances)
    @medicosocial_org = medicosocial_org
    @performances = performances
  end

  def label
    return 'Aucune formule disponible pour ce cas' unless self.valid?
    org_label = org_is_collaborator? ? 'Structure médico-sociale partenaire' : 'Structure médico-sociale non partenaire'
    activities_label =  several_activities? ? '(plusieurs activités)' : '(une seule activité)'
    "#{org_label} #{activities_label}"
  end

  def price
    return nil unless self.valid?
    method_to_call = if org_is_collaborator?
      multipass_available? ? :multi_activities_collaborating_price : :mono_activity_collaborating_price
    else
      multipass_available?  ? :multi_activities_standard_price : :mono_activity_standard_price
    end
    Settings.instance.send method_to_call
  end

  def valid?
    @performances.present?
  end

  private

  def org_is_collaborator?
    @medicosocial_org.nil? ? false : @medicosocial_org.collaborating_medicosocial_org?
  end

  def several_activities?
    @performances.map(&:activity_id).uniq.length > 1
  end

  def several_clubs?
    @performances.map(&:member_id).uniq.length > 1
  end

  def multipass_available?
    several_clubs? || several_activities?
  end

end
