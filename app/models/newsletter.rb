#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class Newsletter < ApplicationRecord
  include URLEncoding

  has_many :items, class_name: "NewsletterItem", :dependent => :destroy
  accepts_nested_attributes_for :items, :allow_destroy => true
  has_and_belongs_to_many :partners

  validates :title, presence: true
  validates_associated :items

  scope :published, -> { where(published: true) }
  scope :ordered, -> { order("created_at DESC") }
  scope :for_nav, -> { published.ordered.limit(3) }

  def pdf_filename
    "#{ENV['APP_NAME']} - #{my_url_encode(title.chomp)}.pdf"
  end

  def has_pdf?
    File.exists?(pdf_path)
  end

  def pdf_path
    Rails.root + "public/system/pdfs/newsletters/" + pdf_filename
  end
end
