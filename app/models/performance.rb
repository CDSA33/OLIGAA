#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class Performance < ApplicationRecord
  enum day: { monday: 1, tuesday: 2, wednesday: 3, thursday: 4, friday: 5, saturday: 6, sunday: 7 }

  belongs_to :member
  belongs_to :activity
  has_many :performance_subscriptions
  has_many :subscriptions, through: :performance_subscriptions

  validates :day, presence: true
  validates :start_at, presence: true
  validates :end_at, presence: true
  validate :end_at_after_start_at

  scope :for_member, ->(member) { where(member: member) }
  scope :disabled, -> { where(disabled: true)}
  scope :enabled, -> { where.not(id: disabled)}

  def full_title
    "[#{activity.title}] #{I18n.t('date.day_names')[Date::DAYNAMES.index(day.camelize)].camelize} #{human_readable_range}"
  end

  def title_for_subscriptions_list
    "#{member.name} (#{activity.title})"
  end

  def title_for_subscription_form
    "#{self.member.name} - #{self.full_title}"
  end

  protected

  def end_at_after_start_at
    if end_at < start_at
      errors.add(:end_at, " : l'heure de fin ne peut être antérieure à celle de début de la séance ;)")
    end unless start_at.blank? || end_at.blank?
  end

  private

  def human_readable_range
    "de #{human_readable_time(start_at)} à #{human_readable_time(end_at)}"
  end

  def human_readable_time(time)
    I18n.l(time, format: :hour_min)
  end
end
