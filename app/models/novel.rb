#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class Novel < ApplicationRecord
  belongs_to :novel_type, optional: true
  has_many :attached_files, :as => :attachable, :dependent => :destroy
  accepts_nested_attributes_for :attached_files, :allow_destroy => true
  has_many :emails, :dependent => :destroy
  has_and_belongs_to_many :activities
  has_attached_file :picture, styles: { thumb: "120x120"}, convert_options: { all: '-strip -colorspace RGB'}

  validates_presence_of :title, :content
  do_not_validate_attachment_file_type :picture

  # Scopes
  default_scope { order('updated_at DESC') }
  scope :templates, -> { where(template: true) }
  scope :without_templates, -> { where(template: false) }
  scope :public_ones, -> { where(public: true) }
  scope :untyped, -> { where(novel_type_id: nil) }


  def self.per_page
    6
  end

  def self.duplicate(id)
    novel_template = Novel.find(id)
    novel = novel_template.dup
    novel.template = false
    novel.public = false
    novel_template.attached_files.each do |attcht|
      new_attcht = attcht.dup
      new_attcht.file = attcht.file.clone
      novel.attached_files << new_attcht
    end
    novel.save
    return novel
  end

  # Presenters

  def public_label
    self.public ? "OUI" : "NON"
  end

  def template_label
    self.template ? "OUI" : "NON"
  end

  def novel_type_label
    self.novel_type.nil? ? "Aucun" : self.novel_type.title
  end

  def activities_label
    activities.empty? ? "Aucune activité en particulier." : activities.map(&:title).join(' / ')
  end

  def smart_picture_url
    return picture.url(:thumb) if picture.present?

    activity_with_poster = activities.find { |activity| activity.poster.present? }
    return activity_with_poster.poster.url(:standard) unless activity_with_poster.nil?
    return nil
  end
end
