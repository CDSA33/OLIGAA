#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class Website < ApplicationRecord
  has_many :map_member_group_settings, class_name: 'MemberGroupSetting::Map'
  has_many :map_member_groups, through: :map_member_group_settings, source: :member_group
  has_many :platform_collaborating_club_settings, class_name: 'MemberGroupSetting::PlatformCollaboratingClub'
  has_many :platform_collaborating_clubs, through: :platform_collaborating_club_settings, source: :member_group
  has_many :platform_medicosocial_orgs_settings, class_name: 'MemberGroupSetting::PlatformMedicosocialOrg'
  has_many :platform_medicosocial_orgs, through: :platform_medicosocial_orgs_settings, source: :member_group
  has_many :platform_collaborating_medicosocial_orgs_settings, class_name: 'MemberGroupSetting::PlatformCollaboratingMedicosocialOrg'
  has_many :platform_collaborating_medicosocial_orgs, through: :platform_collaborating_medicosocial_orgs_settings, source: :member_group
  has_many :partners
  has_attached_file :site_logo, :styles => { :standard => "300x110" }, :convert_options => { :all => '-strip -colorspace RGB'}
  has_attached_file :motto_logo, :styles => { :standard => "300x110" }, :convert_options => { :all => '-strip -colorspace RGB'}

  attr_accessor :_destroy_site_logo, :_destroy_motto_logo

  validates :contact_email, email_format: true, allow_blank: true

  validates_attachment_size :site_logo, :less_than => 1.megabyte, :message => "doit faire moins de 1 Mo", :if => Proc.new { |file| !file.site_logo_file_name.blank? }
  do_not_validate_attachment_file_type :site_logo
  validates_attachment_size :motto_logo, :less_than => 1.megabyte, :message => "doit faire moins de 1 Mo", :if => Proc.new { |file| !file.site_logo_file_name.blank? }
  do_not_validate_attachment_file_type :motto_logo

  validates_date :season_birthdate, allow_blank: true
end
