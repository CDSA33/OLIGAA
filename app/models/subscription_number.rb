#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class SubscriptionNumber
  def initialize(subscription)
    @subscription = subscription
  end

  def label
    member = @subscription.member
    if (affiliation_number = member.affiliation_number).present?
      season_label = @subscription.season.shorten
      prefix = "#{affiliation_number.gsub('/','')}/#{season_label}/"
      existing_platform_numbers = member.subscriptions.where("platform_number LIKE '#{prefix}%'").pluck(:platform_number)
      max_number = existing_platform_numbers.map { |num| num.gsub!(prefix, '') }.map(&:to_i).max || 0
      new_number = ( max_number + 1).to_s.rjust(3,'0')
      "#{prefix}#{new_number}"
    else
      "Numéro d'affiliation du membre non renseigné"
    end
  end
end
