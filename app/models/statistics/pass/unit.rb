#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class Statistics::Pass::Unit

  def initialize(member, season)
    @member = member
    @season = season
  end

  def total
    return_values = {
      none: 0,
      club: @member.subscriptions.for_season(@season).count,
      medicosocial_org: Subscription.for_season(@season).of_medicosocial_org(@member).count
    }
    check_collaboration_or_return(return_values)
  end

  def multi
    return_values = {
      none: 0,
      club: @member.subscriptions.for_season(@season).with_multiactivities_pass.length,
      medicosocial_org: Subscription.for_season(@season).of_medicosocial_org(@member).with_multiactivities_pass.length
    }
    check_collaboration_or_return(return_values)
  end

  def mono
    total - multi
  end

  def label
    return_values = {
      none: nil,
      club: "(#{multi} multi, #{mono} mono sur #{total})",
      medicosocial_org: "(#{multi} multi, #{mono} mono sur #{total})"
    }
    check_collaboration_or_return(return_values)
  end

  private

  def check_collaboration_or_return(values)
    if @member.collaborating_club?
      values[:club]
    elsif @member.medicosocial_org?
      values[:medicosocial_org]
    else
      values[:none]
    end
  end

end
