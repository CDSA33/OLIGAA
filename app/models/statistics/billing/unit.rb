#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class Statistics::Billing::Unit

  def initialize(member, season)
    @member = member
    @season = season
  end

  def total
    check_collaboration_or_return(0) do
      @member.subscriptions.for_season(@season).with_multiactivities_pass.length
    end
  end

  def billed
    check_collaboration_or_return(0) do
      @member.subscriptions.for_season(@season).billed.count
    end
  end

  def label
    check_collaboration_or_return(nil) do
      "(€ : #{billed} / #{total})"
    end
  end

  private

  def check_collaboration_or_return(value)
    return value unless @member.collaborating_club?
    yield
  end

end
