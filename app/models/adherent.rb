#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class Adherent < ApplicationRecord
  has_many :subscriptions
  belongs_to :medico_social_org, class_name: 'Member', optional: true

  validates :last_name, :first_name, :born_on, :permit_number, presence: true
  validates_uniqueness_of :permit_number, message: "Ce numéro de licence est déjà pris pour cette saison. Si vous pensez qu'il s'agit du bon numéro, merci de prendre contact avec le CDSA33."
  validate :at_least_one_medicosocial_org

  before_validation :cleanup_medicosocial_org

  def self.available_for_current_season(params)
    current_season = Season.new(Date.today)
    adherent = if (id = params[:id]).present?
      Adherent.find(id)
    elsif (permit_number = params[:permit_number]).present?
      Adherent.find_by_permit_number(permit_number)
    end

    if adherent && !adherent.subscriptions.map(&:season).map(&:shorten).uniq.include?(current_season.shorten)
      adherent
    else
      params.delete(:id)
      Adherent.new(params)
    end
  end

  def name
    "#{first_name} #{last_name}"
  end

  def medico_social_org_name
    self.medico_social_org.try(:name) || self.other_medicosocial_org
  end

  private

  def at_least_one_medicosocial_org
    unless medico_social_org.present? || other_medicosocial_org.present?
      errors.add(:other_medicosocial_org, "Une structure médico-sociale doit être fournie.")
    end
  end

  def cleanup_medicosocial_org
    self.other_medicosocial_org = nil if medico_social_org.present?
  end
end
