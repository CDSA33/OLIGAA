#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class Partner < ApplicationRecord
  belongs_to :website, optional: true
  belongs_to :group, class_name: "PartnersGroup", optional: true
  has_attached_file :logo, :styles => { :thumb => "100x86", :standard => "160x120" }, :convert_options => { :all => '-strip -colorspace RGB'}
  do_not_validate_attachment_file_type :logo
  validates_attachment_size :logo, :less_than => 1.megabyte, :message => "doit faire moins de 1 Mo"
  has_and_belongs_to_many :newsletters

  acts_as_list scope: :group

  validates :name, presence: true
  validates :url, url_format: true, allow_blank: true

  default_scope { order("position ASC") }
  scope :ungrouped, -> { where(group_id: nil) }

  def self.by_name
    unscoped.order("name ASC")
  end

  def check_scope
    scope_name = "group_id"
    if changes.include?(scope_name)
      old_scope_id = changes[scope_name].first
      new_scope_id = changes[scope_name].last
      self[scope_name] = old_scope_id
      send("decrement_positions_on_lower_items")
      self.position = nil
      self[scope_name] = new_scope_id
      send("add_to_list_bottom")
    end
  end
end
