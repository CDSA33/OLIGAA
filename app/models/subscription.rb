#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class Subscription < ApplicationRecord
  belongs_to :member
  belongs_to :adherent
  accepts_nested_attributes_for :adherent
  has_many :performance_subscriptions, dependent: :destroy
  has_many :performances, through: :performance_subscriptions

  after_create :set_platform_number
  before_destroy :destroy_adherent_if_necessary

  scope :for_performances, ->(performance_ids) { joins(:performances).where(performance_subscriptions: {performance_id: performance_ids}).distinct }
  scope :by_adherent_name, -> { joins(:adherent).order('adherents.first_name','adherents.last_name') }
  scope :billed, -> { where.not(billed_at: nil) }
  scope :of_medicosocial_org, ->(member) { joins(:adherent).where(adherents: {medico_social_org: member}) }
  scope :for_season, ->(season) { where(created_at: (season.beginning..season.end) )}

  def self.with_multiactivities_pass
    all.reject { |sub| !sub.multipass? }
  end

  def performances_list
    "#{performances.to_a.map(&:title_for_subscriptions_list).join(', ')}"
  end

  def external_clubs
    members = self.performances.to_a.map(&:member).uniq
    members.delete(self.member)
    members
  end

  def season
    Season.new(self.created_at)
  end

  def multipass?
    SubscriptionOption.new(self.adherent.medico_social_org,self.performances).send(:multipass_available?)
  end

  def toggle_billing
    self.update_attribute(:billed_at, (billed_at.nil? ? DateTime.now.in_time_zone : nil))
  end

  def billing_status
    billed_at ? 'billed' : 'not-billed'
  end

  private

  def set_platform_number
    self.update_attribute(:platform_number, SubscriptionNumber.new(self).label)
  end

  def destroy_adherent_if_necessary
    adherent.destroy if Subscription.where(adherent: self.adherent).count == 1
  end
end
