#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class EventType < ApplicationRecord
  include SeasonedEventsConcern

  has_many :events

  validates_presence_of :title

  after_save :manage_ga_calendar
  before_destroy :cleanup_ga_events

  def cleanup_ga_events
    self.events.each do |event|
      event.update_column(:ga_evt_id, nil)
      event.update_column(:event_type_id, nil)
    end
  end

  def manage_ga_calendar
    if self.should_have_gcal?
      self.save_ga_calendar
    else
      self.update_column :ga_cal_id, nil
    end
  end

  def save_ga_calendar
    ga_id = ga_cal.update(self)
    self.update_column :ga_cal_id, ga_id
  end
  handle_asynchronously :save_ga_calendar

  def ga_cal
    GoogleAgenda::Calendar.find_by_id_or_create(self.ga_cal_id)
  end
end
