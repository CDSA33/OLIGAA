#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class Season
  FIRST_MONTH = 8
  FIRST_DAY = 15

  attr_reader :beginning
  attr_reader :end

  def initialize(date)
    year = (date < Season.birthdate.change(year: date.year) ? date.year-1 : date.year)
    @beginning = Season.birthdate.change(year: year)
    @end = @beginning + 1.year - 1.day
    return self
  end

  def self.default_birthdate
    Date.new(1970,FIRST_MONTH, FIRST_DAY)
  end

  def self.birthdate
    Settings.instance.reload.season_birthdate || self.default_birthdate
  end

  def self.all(klass, date_field=:begin_at)
    min_date = klass.send(:minimum, date_field)
    max_date = klass.send(:maximum, date_field)
    return [] unless min_date && max_date

    [].tap do |seasons|
      date = min_date
      loop do
        seasons << self.new(date).short_label
        date += 1.year
        break if date > max_date
      end
      seasons << self.new(max_date).short_label
      seasons.uniq!
    end
  end

  def self.all_for(items, date_field=:begin_at)
    result = {}
    items.each do |item|
      year = item.send(date_field).year
      if item.send(date_field)>= Season.birthdate.change(year: year)
        (result["Saison #{year.to_s}-#{(year+1).to_s}"] ||= []) << item
      else
        (result["Saison #{(year-1).to_s}-#{year.to_s}"] ||= []) << item
      end
    end
    result.sort.reverse
  end

  def self.from_short_label(label)
    self.new(self.birthdate.change(year: label.split('-').first.to_i))
  end

  def self.from_label(label)
    self.from_short_label(label.gsub(/Saison /,''))
  end

  def shorten
    shorten_year = @beginning.year.% 100
    "#{shorten_year}#{shorten_year+1}"
  end

  def short_label
    "#{@beginning.year}-#{@beginning.year+1}"
  end

  def label
    "Saison #{short_label}"
  end
end
