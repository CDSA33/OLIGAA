#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class Image < ApplicationRecord
  belongs_to :imageable, polymorphic: true, optional: true
  has_attached_file :img_file,
    :styles => { :thumb => "160x120^", :large => "1280x960>" },
    :convert_options => { :all => '-strip -colorspace RGB'}

  validates_attachment_presence :img_file, :message => " : est obligatoire!"
  validates_attachment_size :img_file, :less_than => 5.megabyte, :message => " : doit être inférieur à 5 Mo"
  do_not_validate_attachment_file_type :img_file

  validates_presence_of :title

end
