#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class NewsletterItem < ApplicationRecord
  belongs_to :newsletter
  has_and_belongs_to_many :activities

  acts_as_list scope: :newsletter

  has_many :attached_files, :as => :attachable, :dependent => :destroy
  accepts_nested_attributes_for :attached_files, :allow_destroy => true
  has_many :images, :as => :imageable, :dependent => :destroy
  accepts_nested_attributes_for :images, :allow_destroy => true

  attr_accessor :delete_photo

  validates :title, :content, presence: true

  before_validation do
    photo.clear if delete_photo == '1'
  end

  scope :ordered, -> { order("position ASC") }
end
