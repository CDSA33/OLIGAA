module SeasonedEventsConcern
  extend ActiveSupport::Concern

  included do

    def seasoned_events
      Season.all_for(self.events.with_dates.visible.reverse_chronology)
    end
  end
end
