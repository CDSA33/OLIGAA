#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'csv'

class Member < ApplicationRecord
  has_and_belongs_to_many :activities
  has_and_belongs_to_many :member_groups, :join_table => 'groupmemberships'
  has_many :contacts, :dependent => :destroy
  accepts_nested_attributes_for :contacts, :allow_destroy => true
  has_and_belongs_to_many :public_types
  has_many :performances, dependent: :destroy
  has_many :subscriptions, dependent: :destroy

  geocoded_by :address
  after_validation :geocode, if: ->(obj){ obj.address.present? and obj.address_changed? }

  scope :by_name, -> { order("name ASC") }
  scope :admins, -> { where(admin: true) }
  scope :blocked, -> { where(blocked: true) }
  scope :published, -> { where(published: true) }
  scope :not_published, -> { where(published: false) }
  scope :with_activities, ->(acts) { joins(:activities).where("activities.id" => acts).distinct }
  scope :with_public_types, ->(pts) { joins(:public_types).where("public_types.id" => pts).distinct }
  scope :of_member_groups, ->(mgs) { joins(:member_groups).where(member_groups: { id: mgs}).distinct }
  scope :localized, -> { where "latitude IS NOT NULL OR longitude IS NOT NULL" }

  def self.ungrouped
    Member.all - Member.joins(:member_groups).distinct
  end

  def self.for_years(years)
    sqls = []
    years.each do |year|
      sqls << "(members.registry_date between '#{year}-01-01' and '#{year}-12-31')"
    end
    self.where(sqls.join(' or '))
  end

  def self.with_contacts(search_text)
    search_pattern = "%#{search_text}%"
    joins(:contacts).where("contacts.last_name like ? or contacts.first_name like ? or contacts.email like ?", search_pattern, search_pattern, search_pattern).distinct
  end

  def self.registry_years
    all.map { |m| m.registry_date.year.to_s if m.registry_date.present? }.compact.uniq.sort
  end

  validates_presence_of :login, :name
  validates_uniqueness_of :login
  validates_format_of :login, :with => /\A[a-zA-Z0-9\.\-_@]+\z/, :message => "ne peut contenir que des lettres (non accentuées), des chiffres et certains caractères spéciaux ('.', '-', '_' ou '@')"
  validates_presence_of :contacts
  validates :website, url_format: true, allow_blank: true

  before_save :save_contacts_if_necessary
  after_save :cleanup_coordinates

  def save_contacts_if_necessary
    contacts.each do |contact|
      contact.save if contact.new_function.present?
    end
  end

  def cleanup_coordinates
    if address.nil? || address.blank?
      reset_ccordinates
    end
  end

  def reset_ccordinates
    self.update_column(:latitude, nil)
    self.update_column(:longitude, nil)
  end

  def collaborating_club?
    Settings.instance.reload.platform_collaborating_clubs.any? do |member_group|
      self.member_groups.include?(member_group)
    end
  end

  def collaborating_medicosocial_org?
    Settings.instance.reload.platform_collaborating_medicosocial_orgs.any? do |member_group|
      self.member_groups.include?(member_group)
    end
  end

  def medicosocial_org?
    Settings.instance.reload.platform_medicosocial_orgs.any? do |member_group|
      self.member_groups.include?(member_group)
    end
  end

  # Presenters

  def admin_label
    self.admin ? "OUI" : 'NON'
  end

  def blocked_label
    self.blocked ? "OUI" : 'NON'
  end

  def published_label
    self.published ? "OUI" : 'NON'
  end

  def localized_status
    geocoded? ? "Affiché sur la carte" : "Non trouvé sur la carte"
  end

  # Custom accessors

  def login=(value)
    write_attribute :login, (value ? value.downcase : nil)
  end

  # Password management

  attr_accessor :updating_password

  def should_update_password?
    updating_password || false
  end

  def password=(password)
    if should_update_password?
      write_attribute(:salt,OpenSSL::Digest::SHA1.new(OpenSSL::Random.random_bytes(256)).hexdigest)
      write_attribute(:password, hashed_and_salted_password(password, salt))
      write_attribute(:default_password, nil)
    end
  end

  def random_digits(n)
    SecureRandom.base64(n)
  end

  def create_default_password
    self.default_password = random_digits(12)
  end

  def reset_password
    write_attribute(:salt,nil)
    write_attribute(:password, nil)
    create_default_password
    self.save ? true : false
  end

  def first_connection?
    self.password ? false : true
  end

  def authenticate(pass)
    if self.first_connection?
      pass == self.default_password
    else
      self.hashed_and_salted_password(pass, self.salt) == self.password
    end
  end

  def hashed_and_salted_password(p, s)
    OpenSSL::Digest::SHA1.new([s,p].join.to_s).hexdigest
  end

  # CSV Export

  def self.all_to_csv(member_group)
    members = self.includes({contacts: [:contact_type, :phone_numbers]})
    members = members.joins(:member_groups).where(member_groups: {id: member_group.id}) if member_group
    CSV.generate :col_sep => ';' do |csv|
      # header row
      csv << ["Membre","Type de contact","Civilité","Last Name","First Name","E-mail","Rue","Code postal","Ville", "Tel1","Tel2","Tel3"]
      # data rows
      members.each do |member|
        member.contacts.each do |contact|
          line = []
          line << [member.name, (contact.contact_type ? contact.contact_type.title : nil), contact.civility, contact.last_name, contact.first_name, contact.email, contact.street, contact.postal_code, contact.city]
          contact.phone_numbers.each do |phone|
            line << phone.number
          end
          csv << line.flatten
        end
      end
    end
  end
end
