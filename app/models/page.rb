#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class Page < ApplicationRecord
  include URLEncoding

  belongs_to :menu, optional: true
  has_many :attached_files, :as => :attachable, :dependent => :destroy
  accepts_nested_attributes_for :attached_files, :allow_destroy => true
  has_many :logos, :dependent => :destroy
  accepts_nested_attributes_for :logos, :allow_destroy => true

  acts_as_list :scope => :menu

  default_scope { order("position ASC") }

  validates_presence_of :title
  validates_presence_of :content

  before_update :check_scope

  def to_param
    "#{id}-#{my_url_encode(title)}"
  end

  def should_be_displayed?
    self.displayed
  end

  def check_scope
    scope_name = "menu_id"
    if changes.include?(scope_name)
      old_scope_id = changes[scope_name].first
      new_scope_id = changes[scope_name].last
      self[scope_name] = old_scope_id
      send("decrement_positions_on_lower_items")
      self.position = nil
      self[scope_name] = new_scope_id
      send("add_to_list_bottom")
    end
  end
end
