#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class EmailsController < ApplicationController
  before_action :authorize

  def index
    @unsent_emails = Email.unsent
    @sent_emails = Email.sent.paginate(page: params[:page])
    @total_sent_count = Email.sent.count
  end

  def show
      @email = Email.find(params[:id])
  end

  def new
    @email = Email.new
    @contacts = []
    if params[:novel_id]
      @email.novel = Novel.find(params[:novel_id])
    else
      @email.novel = Novel.new
    end
  end

  def edit
    @email = Email.find(params[:id])
    @contacts = @email.contacts
  end

  def create
    manage_recipients_for(params[:email])
    @email = Email.new
    if (novel_id = params[:email][:novel_attributes][:id]).present?
      @email.novel = Novel.find(novel_id)
    end
    @email.attributes = email_params
    if @email.save
      flash[:notice] = "Email créé avec succès."
      redirect_to @email
    else
      @contacts = @email.contacts
      flash[:error] = t('flash.error')
      render "new"
    end
  end

  def update
    manage_recipients_for(params[:email])
    @email = Email.find(params[:id])
    if @email.update_attributes(email_params)
      flash[:notice] = "Email modifié avec succès."
      redirect_to @email
    else
      @contacts = @email.contacts
      flash[:error] = t('flash.error')
      render "edit"
    end
  end

  def send_off
    @email = Email.find(params[:id])
    if @email.send_off
      flash[:notice] = "L'envoi de l'e-mail a bien été planifié. Il partira dans quelques secondes."
      redirect_to @email
    else
      flash[:error] = "Une erreur a été rencontrée lors de la planification de l'envoi de l'e-mail."
      render @email
    end
  rescue Exception => e
    flash[:error] = "L'erreur suivante est survenue : <#{e.message}>. Si vous ne voyez pas quel est le problème, merci de notifier le développeur avec le message d'erreur complet ;)"
    redirect_to @email
  end

  def destroy
    @email = Email.find(params[:id])
    if @email.destroy
      flash[:notice] = "Email supprimé avec succès."
    end
    redirect_to(emails_path)
  end

  def recipients_by_member_groups
    @contacts = Contact.find(params[:contacts].split(','))

    @member_groups = MemberGroup.all_not_empty
    @ungrouped_members = Member.ungrouped

    render partial: 'recipients_by_member_groups'
  end

  def recipients_by_contact_types
    @contacts = Contact.find(params[:contacts].split(','))

    @contact_types = ContactType.all_not_empty
    @untyped_contacts = Contact.untyped

    render partial: 'recipients_by_contact_types'
  end

  private

  def manage_recipients_for(p)
    p[:recipients] = ""
    p[:recipients] = p[:contacts].uniq.join(', ') if p[:contacts]
    p.delete(:contacts)
  end

  def email_params
    permitted_params = [
      :sender, :recipients_by_contact_types, :other_recipients_enabled, :other_recipients,
      :member_id, :recipients, novel_attributes: novel_permitted_attributes
    ]
    params.require(:email).permit(permitted_params)
  end

end
