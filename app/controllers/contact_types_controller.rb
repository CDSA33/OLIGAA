#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class ContactTypesController < ApplicationController
  before_action :authorize

  def index
    @contact_types = ContactType.all
  end

  def new
    @contact_type = ContactType.new
  end

  def edit
    @contact_type = ContactType.find(params[:id])
  end

  def create
    @contact_type = ContactType.new(contact_type_params)
    if @contact_type.save
      flash[:notice] = "Type de contact créé avec succès"
      redirect_to contact_types_url
    else
      render :action => "new"
    end
  end

  def update
    @contact_type = ContactType.find(params[:id])
    if @contact_type.update_attributes(contact_type_params)
      flash[:notice] = "Type de contact mis à jour avec succès"
      redirect_to contact_types_url
    else
      render :action => "edit"
    end
  end

  def destroy
    @contact_type = ContactType.find(params[:id])
    if @contact_type.destroy
      flash[:notice] = "Type de contact supprimé avec succès"
    end
    redirect_to contact_types_url
  end

  private

  def contact_type_params
    params.require(:contact_type).permit(:title)
  end
end
