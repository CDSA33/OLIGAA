#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class NovelTypesController < ApplicationController
  before_action :authorize

  def show
    @novel_type = NovelType.find(params[:id])
  end

  def new
    @novel_type = NovelType.new
  end

  def edit
    @novel_type = NovelType.find(params[:id])
  end

  def create
    @novel_type = NovelType.new(novel_type_params)
    if @novel_type.save
      flash[:notice] = "Type d'information créé avec succès."
      redirect_to @novel_type
    else
      render :action => "new"
    end
  end

  def update
    @novel_type = NovelType.find(params[:id])
    if @novel_type.update_attributes(novel_type_params)
      flash[:notice] = "Type d'information mis à jour avec succès."
      redirect_to @novel_type
    else
      render :action => "edit"
    end
  end

  def destroy
    @novel_type = NovelType.find(params[:id])
    if @novel_type.destroy
      flash[:notice] = "Type d'information supprimé avec succès."
    end
    redirect_to novels_url
  end

  private

  def novel_type_params
    params.require(:novel_type).permit(:title, :description)
  end
end
