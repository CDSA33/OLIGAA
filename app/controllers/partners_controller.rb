#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class PartnersController < ApplicationController
  before_action :authorize, except: :display

  def index
    @partners_groups = PartnersGroup.includes(:partners)
    @ungrouped_partners = Partner.ungrouped
  end

  def display
    @groups = PartnersGroup.includes(:partners)
    @ungrouped_partners = Partner.ungrouped
  end

  def new
    @partner = Partner.new
  end

  def edit
    @partner = Partner.find(params[:id])
  end

  def create
    @partner = Partner.new(partner_params)
    if @partner.save
      flash[:notice] = 'Partenaire créé avec succès.'
      redirect_to partners_path
    else
      flash[:error] = t('flash.error')
      render :new
    end
  end

  def update
    @partner = Partner.find(params[:id])
    if params[:partner][:_destroy] == "1"
      @partner.logo.destroy
    end
    if @partner.update_attributes(partner_params)
      flash[:notice] = 'Partenaire mis à jour avec succès.'
      redirect_to partners_path
    else
      flash[:error] = t('flash.error')
      render :edit
    end
  end

  def move_up
    @partner = Partner.find(params[:id])
    @partner.move_higher
    redirect_to partners_url
  end

  def move_down
    @partner = Partner.find(params[:id])
    @partner.move_lower
    redirect_to partners_url
  end

  def destroy
    if Partner.find(params[:id]).destroy
      flash[:notice] = "Partenaire supprimé avec succès."
    else
      flash[:error] = "Une erreur a été rencontrée."
    end
    redirect_to partners_path
  end

  private

  def partner_params
    params.require(:partner).permit(:name, :url, :website_id, :logo, :group_id)
  end
end
