#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class MenusController < ApplicationController
  before_action :authorize

  def new
    @menu = Menu.new
  end

  def edit
    @menu = Menu.find(params[:id])
  end

  def create
    @menu = Menu.new(menu_params)
    if @menu.save
      flash[:notice] = 'Le menu a été créé avec succès.'
      redirect_to pages_url
    else
      render :action => "new"
    end
  end

  def update
    @menu = Menu.find(params[:id])
    if @menu.update_attributes(menu_params)
      flash[:notice] = 'Le menu a été modifié avec succès.'
      redirect_to pages_url
    else
      render :action => "edit"
    end
  end

  def move_up
    @menu = Menu.find(params[:id])
    @menu.move_higher
    redirect_to pages_url
  end

  def move_down
    @menu = Menu.find(params[:id])
    @menu.move_lower
    redirect_to pages_url
  end

  def destroy
    @menu = Menu.find(params[:id])
    @menu.destroy
    flash[:notice] = 'Le menu a été supprimé avec succès.'
    redirect_to pages_url
  end

  private

  def menu_params
    params.require(:menu).permit(:title, :uid)
  end
end
