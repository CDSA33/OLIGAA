#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class PartnersGroupsController < ApplicationController
  before_action :authorize

  def new
    @partners_group = PartnersGroup.new
  end

  def edit
    @partners_group = PartnersGroup.find(params[:id])
  end

  def create
    @partners_group = PartnersGroup.new(partner_group_params)
    if @partners_group.save
      flash[:notice] = 'Groupe de partenaires créé avec succès.'
      redirect_to partners_url
    else
      render :new
    end
  end

  def update
    @partners_group = PartnersGroup.find(params[:id])
    if @partners_group.update_attributes(partner_group_params)
      flash[:notice] = 'Groupe de partenaires mis à jour avec succès.'
      redirect_to partners_url
    else
      render :edit
    end
  end

  def move_up
    @partners_group = PartnersGroup.find(params[:id])
    @partners_group.move_higher
    redirect_to partners_url
  end

  def move_down
    @partners_group = PartnersGroup.find(params[:id])
    @partners_group.move_lower
    redirect_to partners_url
  end

  def destroy
    PartnersGroup.find(params[:id]).destroy
    flash[:notice] = "Groupe de partenaires supprimé avec succès."
    redirect_to partners_url
  end

  def partner_group_params
    params.require(:partners_group).permit(:title)
  end
end
