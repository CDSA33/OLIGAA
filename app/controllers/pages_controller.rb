#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class PagesController < ApplicationController
  before_action :authorize, :except => [ :show ]

  def index
    @menus = Menu.all
    @unmenued_pages = Page.where(menu_id: nil)
  end

  def show
    @page = Page.find(params[:id])
    unless @page.displayed?
      authorize unless (current_member && current_member.admin?)
    end
  rescue ActiveRecord::RecordNotFound
      redirect_to root_path
  end

  def new
    @page = Page.new
  end

  def edit
    @page = Page.find(params[:id])
  end

  def create
    @page = Page.new(page_params)
    if @page.save
      flash[:notice] = 'Page créée avec succès.'
      redirect_to(@page)
    else
      render_with_error :new
    end
  end

  def update
    @page = Page.find(params[:id])
    if @page.update_attributes(page_params)
      flash[:notice] = 'Page mise à jour avec succès.'
      redirect_to(@page)
    else
      render_with_error :edit
    end
  end

  def move_up
    @page = Page.find(params[:id])
    @page.move_higher
    redirect_to pages_path
  end

  def move_down
    @page = Page.find(params[:id])
    @page.move_lower
    redirect_to pages_path
  end

  def destroy
    @page = Page.find(params[:id])
    @page.destroy
    flash[:notice] = 'Page supprimée avec succès.'
    redirect_to(pages_url)
  end

  private

  def render_with_error(method)
    flash[:error] = t('flash.error')
    render action: method
  end

  def page_params
    params.require(:page).permit(:title, :content, :menu_id, :displayed,
      logos_attributes: [:id, :img, :displayed, :_destroy],
      attached_files_attributes: attached_file_permitted_attributes)
  end
end
