#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class ActivitiesController < ApplicationController
  before_action :authorize, :except => [ :show ]

  def index
    @activity_categories = ActivityCategory.all
    @uncategorized_activities = Activity.uncategorized
  end

  def show
    @activity = Activity.find(params[:id])
    @seasoned_events = @activity.seasoned_events
    @available_member_groups = settings.map_member_groups & MemberGroup.for_activity(@activity)
  end

  def new
    @activity = Activity.new
  end

  def edit
    @activity = Activity.find(params[:id])
  end

  def create
    @activity = Activity.new(activity_params)
    if @activity.save
      flash[:notice] = 'Activité créée avec succès.'
      redirect_to @activity
    else
      flash[:error] = t('flash.error')
      render :action => "new"
    end
  end

  def update
    @activity = Activity.find(params[:id])
    if params[:activity][:_destroy] == "1"
      @activity.poster.destroy
    end
    if @activity.update_attributes(activity_params)
      flash[:notice] = 'Activité mise à jour avec succès.'
      redirect_to @activity
    else
      flash[:error] = t('flash.error')
      render :action => "edit"
    end
  end

  def destroy
    @activity = Activity.find(params[:id])
    @activity.destroy
    flash[:notice] = 'Activité supprimée avec succès.'
    redirect_to activities_url
  end

  private

  def activity_params
    params.require(:activity).permit(:title, :presentation, :description, :poster, :filterable, :listed, attached_files_attributes: attached_file_permitted_attributes, activity_category_ids: [])
  end
end
