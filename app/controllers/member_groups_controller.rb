#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class MemberGroupsController < ApplicationController
  before_action :authorize, :except => [:show, :filter_members]

  def show
    prepare_markers
  end

  def filter_members
    prepare_markers
    flash.now[:error] = "Aucune réponse n'a été trouvée. Merci d'élargir votre tri." if @markers.empty?
    render :show
  end

  def members
    respond_to do |format|
      format.js do
        @members = case params[:id]
        when "ungrouped"
          Member.by_name.ungrouped
        when "admins"
          Member.by_name.admins
        when "blocked"
          Member.by_name.blocked
        else
          member_group = MemberGroup.find(params[:id])
          member_group.members.by_name
        end
        @mg_id = member_group.try(:id) || params[:id]
      end
    end
  end

  def new
    @member_group = MemberGroup.new
    prepare_groups
    @selected_members = []
  end

  def create
    @member_group = MemberGroup.new(member_group_params)
    if @member_group.save
      flash[:notice] = "Le groupe de membres a été créé avec succès."
      redirect_to @member_group
    else
      prepare_groups
      @selected_members = @member_group.members
      flash[:error] = "Une erreur a été rencontrée lors de la création du groupe de membres."
      render :action => 'new'
    end
  end

  def edit
    @member_group = MemberGroup.find(params[:id])
    prepare_groups
    @selected_members = @member_group.members.by_name
  end


  def update
    @member_group = MemberGroup.find(params[:id])
    if @member_group.update_attributes(member_group_params)
      flash[:notice] = 'Groupe de membres mis à jour avec succès.'
      redirect_to @member_group
    else
      prepare_groups
      @selected_members = @member_group.members
      render :action => "edit"
    end
  end

  def destroy
    @member_group = MemberGroup.find(params[:id])
    @member_group.destroy
    flash[:notice] = 'Groupe de membres supprimé avec succès.'
    redirect_to members_url
  end

  private

  def prepare_groups
    @member_groups = MemberGroup.all
    @ungrouped_members = Member.ungrouped
  end

  def prepare_markers
    @member_group = MemberGroup.find(params[:id])
    if admin? || @member_group.published?
      @activities = @member_group.activities.filterable.distinct
      base_members = @member_group.members.by_name
      base_members = base_members.with_activities(session[:activities]) if session[:activities] = params[:activities]
      base_members = base_members.with_public_types(session[:public_types]) if session[:public_types] = params[:public_types]
      base_members = base_members.localized
      @published_members = base_members.published
      @private_members = base_members.not_published
      @markers = base_members.map do |member|
        {
          latlng: [member.latitude, member.longitude],
          title: member.name,
          popup: render_to_string(partial: "/members/infowindow", locals: { member: member}),
          icon: (member.published ? :blue : :grey)
        }
      end
    else
      authorize
    end
  end

  def member_group_params
    params.require(:member_group).permit(:title, :description, member_ids: [])
  end

end
