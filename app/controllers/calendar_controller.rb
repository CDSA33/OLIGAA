#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class CalendarController < ApplicationController

  def show
    session[:cal_month] = params[:month]
    session[:cal_year] = params[:year]
    @all_event_types = EventType.all
    @event_types = event_types
    @month = session[:cal_month].to_i
    @year = session[:cal_year].to_i
    @shown_month = Date.civil(@year, @month)
    @first_day_of_week = 1
    events = Event.chronology.with_dates.visible.joins(:event_type).where(event_type_id: @event_types)
    @fullcalendar_events = prepare_for_fullcalendar(events.for_calendar(@month, @year))
    @events = events.for_list(@month, @year)
    @events.empty? ? @last_update = nil : @last_update = @events.max { |e1, e2| e1.updated_at <=> e2.updated_at }.updated_at
    respond_to do |format|
      format.html
      format.pdf do
        pdf = PDFKit.new(calendar_url(year: session[:cal_year], month: session[:cal_month], event_types: session[:cal_event_types]), orientation: "landscape", zoom: "0.85").to_pdf
        send_data pdf, filename: "#{ENV['APP_NAME']} - #{session[:cal_year]}-#{session[:cal_month]}.pdf", type: 'application/pdf'
        expires_now
      end
    end
  end

  def update
    session[:cal_event_types] = []
    params[:event_types] ||= []
    params[:event_types].each do |et|
      session[:cal_event_types] << et unless session[:cal_event_types].include?(et)
    end
    redirect_to calendar_path(year: params[:year], month: params[:date][:month])
  end

  private

  def event_types
    if params[:event_types]
      EventType.find(params[:event_types])
    elsif session[:cal_event_types]
      EventType.find(session[:cal_event_types])
    else
      EventType.all
    end
  end

  def prepare_for_fullcalendar(events)
    events.map do |event|
      {
        title: event.title,
        start: event.begin_at.iso8601,
        end: event.end_at.iso8601,
        url: event_url(event),
        color: event.event_type.color
      }
    end.to_json
  end
end
