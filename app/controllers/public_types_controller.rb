#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class PublicTypesController < ApplicationController
  before_action :authorize

  def index
    @public_types = PublicType.all
  end

  def show
    @public_type = PublicType.find(params[:id])
  end

  def new
    @public_type = PublicType.new
  end

  def edit
    @public_type = PublicType.find(params[:id])
  end

  def create
    @public_type = PublicType.new(public_type_params)
    if @public_type.save
      flash[:notice] = "Type de public créé avec succès"
      redirect_to(@public_type)
    else
      render :action => "new"
    end
  end

  def update
    @public_type = PublicType.find(params[:id])
    if @public_type.update_attributes(public_type_params)
      flash[:notice] = "Type de public mis à jour avec succès"
      redirect_to(@public_type)
    else
      render :action => "edit"
    end
  end

  def destroy
    @public_type = PublicType.find(params[:id])
    if @public_type.destroy
      flash[:notice] = "Type de public supprimé avec succès"
    end
    redirect_to(public_types_url)
  end

  private

  def public_type_params
    params.require(:public_type).permit(:title, :description)
  end
end
