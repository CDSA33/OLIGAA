#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
module ApplicationHelper

  def title(title_page)
    content_for(:title) { title_page }
  end

  def logged_in?
    current_member.nil? ? false : true
  end

  def menu_to title, target, options = {}
    options ||= {}
    options[:class] = [options[:class]].flatten << 'current' if current_page?(target) || in_breadcrumbs?(target)

    content_tag :li, options do
      link_to textilize_without_paragraph(title), target
    end
  end

  def custom_css
    top_color = settings.main_color
    bottom_color = settings.secondary_color
    h1_color = settings.h1_color
    h2_color = settings.h2_color
      content_tag :style do
        scss = <<SCSS
@media screen {
  body {
    background: -webkit-gradient(linear, 50% 0%, 50% 100%, color-stop(0%, #{top_color}), color-stop(100%, #{bottom_color}));
    background: -webkit-linear-gradient(#{top_color}, #{bottom_color});
    background: -moz-linear-gradient(#{top_color}, #{bottom_color});
    background: -o-linear-gradient(#{top_color}, #{bottom_color});
    background: linear-gradient(#{top_color}, #{bottom_color});
    -pie-background: linear-gradient(#{top_color}, #{bottom_color});
    behavior: url(/assets/PIE.htc);
  }
  h1, li.current, li.newsletter_item div.item_desc h2 {
    color: #{h1_color};
  }
  h2 {
    color: #{h2_color};
  }
  li.current:before, li.current:after {
    background-color: #{h1_color};
  }
}
@media print {
}
SCSS
      end
  end

  def redcloth_help
    link_to t('help.redcloth'), redcloth_help_path, :class => "externe"
  end

  def expand_fields(title, div_id, selectable = true)
    fields = "<div>"
    fields << toggle_link(title, div_id)
    if selectable
      fields << check_all_links(:male, div_id, :with_after_action, class: "action_link")
    end
    fields << "</div>"
    fields.html_safe
  end

  def toggle_link(title, field, options = {})
    link_to title, "#", options.merge!({class: 'expendable', data: {field: field}})
  end

  def fetch_or_toggle_link(title, field, options = {})
    link_to title, "#", options.merge!({class: 'fetchable', data: {field: field}})
  end

  def check_all_links(gender, field, after_action = :without_after_action, options= {})
    check_options = append_merge(options,{class: 'checkable', data: {field: field, check_action: :check}})
    uncheck_options = append_merge(options,{class: 'checkable', data: {field: field, check_action: :uncheck}})
    if after_action == :with_after_action
      check_options = append_merge(check_options, {data: {after_action: :expand}})
      uncheck_options = append_merge(uncheck_options, {data: {after_action: :close}})
    end
    content_tag :span do
      link_to((gender == :male ? 'Tous' : 'Toutes'), "#", check_options) +
      link_to((gender == :male ? 'Aucun' : 'Aucune'), "#", uncheck_options)
    end
  end

  def sort_by_method(array, method)
    result = array.sort_by { |item| item.send(method).downcase }
    result
  end

  def rails_version
    "| Rails #{Rails.version}" if ["development", "testing"].include?(Rails.env)
  end

  def copyright
    "© 2010#{Date.today.year == 2010 ? '' : "-"+Date.today.year.to_s} #{ENV['APP_NAME']}"
  end

  def status_icon(obj, method, titles = ["OUI", "NON"])
    icon, severity, title = obj.send(method) ? ['fa-check-square-o', 'success', titles[0]] : ['fa-ban', 'danger', titles[1]]
    content_tag(:i, '', class: "fa #{icon} #{severity}", title: title)
  end

  private

  def in_breadcrumbs? target
    crumbs_urls = breadcrumbs.tap do |links|
      links.collect(&:url)
    end.compact.drop(1) # Removing non-link and home crumbs

    !crumbs_urls.find { |url| url_for(target) == url }.nil?
  end

  def append_merge(h1, h2)
    h1.merge(h2) do |key, oldval, newval|
      if oldval.is_a?(Hash)
        append_merge(oldval, newval)
      else
        "#{oldval} #{newval}"
      end
    end
  end
end
