#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
module NewslettersHelper
  def partner_link(partner, size = :standard, with_title = true)
    data = ""
    if partner.url.present? && partner.logo?
      # image cliquable + nom normal
      data << "<span>#{partner.name}</span>" if with_title
      data << link_to(image_tag(partner.logo.url(size), alt: partner.name), partner.url, class: "externe")
    elsif partner.url.present? && !partner.logo?
      # nom cliquable
      data << link_to(partner.name, partner.url, class: "externe")
    elsif !partner.url.present? && partner.logo?
      # image normale + nom normal
      data << "<span>#{partner.name}</span>"
      data << image_tag(partner.logo.url(size), alt: partner.name)
    else
      # nom normal
      data << "<span>#{partner.name}</span>"
    end
    data.html_safe
  end
end