#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
module LinksHelper

  def custom_link(icon = nil, text = nil, path = '#', version = nil, options = {})
    options[:class] ||= []
    options[:class] << 'AdminCard-action'
    options[:title] = text

    link_to path, options do
      link_text = ''.html_safe
      link_text += content_tag(:i, '', class: "fa #{icon}") if icon
      link_text += content_tag(:span, text) if version != :short
      link_text
    end
  end

  def custom_add_link(text, path, version = :normal, options = {})
    options[:class] ||= []
    options[:class] << 'success'

    custom_link('fa-plus', text, path, version, options)
  end

  def custom_show_link(path, text, version = :normal, options = {})
    custom_link('fa-eye', text, path, version, options)
  end

  def custom_primary_show_link(path, version = :normal, options = {})
    options[:class] ||= []
    options[:class] << 'primary'

    custom_link('fa-eye', 'Afficher', path, version, options)
  end

  def custom_delete_link(path, version = :normal, options = {})
    options[:class] ||= []
    options[:class] << 'danger'
    (options[:data] ||= {})[:confirm] = 'Etes-vous sûr ?'
    options[:method] = :delete

    custom_link('fa-trash', 'Supprimer', path, version, options)
  end

  def custom_edit_link(path, version = :normal, options = {})
    options[:class] ||= []
    options[:class] << 'warning'

    custom_link('fa-pencil', 'Éditer', path, version, options)
  end

  def custom_move_up_link(path, version = :normal, options = {})
    options[:class] ||= []
    options[:method] = :patch

    custom_link('fa-arrow-circle-up', 'Monter', path, version, options)
  end

  def custom_move_down_link(path, version = :normal, options = {})
    options[:class] ||= []
    options[:method] = :patch

    custom_link('fa-arrow-circle-down', 'Descendre', path, version, options)
  end

  def custom_list_link(path, version = :normal, options = {})
    custom_link('fa-list', 'Retour à la liste', path, version, options)
  end

  def custom_mail_link(path, version = :normal, options = {})
    options[:class] ||= []
    options[:class] << 'primary'

    custom_link('fa-send-o', 'Envoyer par mail', path, version, options)
  end

  def custom_csv_export_link(path, version = :normal, options = {})
    (options[:data] ||= {})[:confirm] = 'Etes-vous sûr ?'

    custom_link 'fa-file-excel-o', 'Export CSV', path, version, options
  end

  def custom_copy_link(path, version = :normal, options = {})
    options[:class] ||= []
    options[:class] << 'success'

    custom_link 'fa-files-o', 'Dupliquer', path, version, options
  end

  def custom_print_card_link(path, version = :normal, options = {})
    options[:class] ||= []
    options[:class] << 'primary'

    custom_link 'fa-id-card-o', 'Imprimer carte', path, version, options
  end

  def custom_bill_link(path, version = :normal, options = {})
    options[:class] ||= []
    options[:method] = :patch
    (options[:data] ||= {})[:confirm] = 'Etes-vous sûr de vouloir (dé-)facturer cet élément ?'

    custom_link 'fa-eur', '€', path, version, options
  end

  def custom_disable_link(path, version = :normal, options = {})
    options[:class] << 'warning'
    options[:method] = :patch
    (options[:data] ||= {})[:confirm] = 'Etes-vous sûr de vouloir désactiver cet élément ?'

    custom_link 'fa-toggle-off', 'Désactiver', path, version, options
  end

  def custom_enable_link(path, version = :normal, options = {})
    options[:class] << 'warning'
    options[:method] = :patch
    (options[:data] ||= {})[:confirm] = 'Etes-vous sûr de vouloir activer cet élément ?'

    custom_link 'fa-toggle-on', 'Activer', path, version, options
  end

  def custom_redo_link(path, version = :normal, options = {})
    options[:class] ||= []
    options[:class] << 'primary'

    custom_link 'fa-repeat', 'Renouveler', path, version, options
  end
end
