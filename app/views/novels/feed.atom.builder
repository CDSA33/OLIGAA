atom_feed(:language => 'fr') do |feed|
  feed.title("Informations du <%= ENV['APP_NAME'] %>")
  feed.updated(@novels.first.updated_at)

  @novels.each do |novel|
    feed.entry(novel) do |entry|
      entry.title(novel.title)
      entry.content(textilize(raw novel.content), :type => 'html')
      entry.updated(novel.updated_at)
    end
  end
end
