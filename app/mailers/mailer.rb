#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
class Mailer < ActionMailer::Base

  def contact_us(email, sent_at = Time.now)
    @mail = email
    mail  :from => ENV['DEFAULT_MAIL_FROM'],
          :to => Settings.instance.contact_email,
          :reply_to => @mail.email,
          :subject => @mail.subject,
          :date => sent_at
  end

  def send_info(email, recipient, sent_at = Time.now)
    @novel = email.novel

    @novel.attached_files.each do |attached_file|
      attachments[attached_file.file_file_name] = File.read(attached_file.file.path)
    end

    mail :from => email.sender,
          :to => recipient,
          :subject => email.novel.title,
          :date => sent_at
  end

  def reset_password(member, recipient, sent_at = Time.now)
    @member = member
    mail  :from => Settings.instance.contact_email,
          :to => recipient,
          :date => sent_at,
          :subject => "#{ENV['APP_NAME']} - réinitialisation du mot de passe"
  end
end
