# Outil en Ligne d'Information et de Gestion des Activités des Associations

Projet initié par le Comité départemental du sport adapté de Gironde (CDSA33), cet outil est à destination des associations désirant faciliter la gestion et la communication autour des actions qu'elles entreprennent. Commanditaire des développements depuis 2010, le CDSA33 libére le projet en 2016, sensible aux arguments de la [démarche d'essaimage](http://www.infopiiaf.fr/blog/2015/07/07/libre-et-agile-un-dessin-d-essaim.html).

Ainsi, toute organisation se retrouvant dans les fonctionnalités imaginées et développées au fil des ans est maintenant libre de les utiliser, voire de les améliorer, en ajouter…

## Quelques fonctionnalités

Grâce à OLIGAA :

* vous organisez vos événements et en rendez visible le calendrier ;
* vous mettez en avant les actions et les acteurs du changement que vous promouvez ;
* vous partagez votre actualité par des articles en ligne et une lettre d'information ;
* vous constituez un carnet d'adresses partagé entre tous les collaborateurs, avec la rédaction et l'envoi de courriels ciblés.

Retrouvez plus de détails sur la [page dédiée au projet](http://www.infopiiaf.fr/realisations/cdsa33.html) chez les développeurs ayant commis les premières versions :-)

Un effort particulier a notamment été fait d'un point de vue accessibilité pour permettre aux personnes ayant des difficultés d'avoir accès aux informations.

### Et des idées pour la suite

Ce logiciel n'est pas terminé, loin de là. Il reste de nombreuses fonctionnalités à imaginer et à développer. Citons par exemple :

* Un système d'invitation/inscription aux événements ;
* ~~Le choix entre des cartes _Google Maps_ ou [OpenStreetMap](http://www.openstreetmap.org/)~~ (OSM cf. #12) ;
* Un _web service_ de synchronisation des contacts pour carnet d'adresses sur ordinateur de bureau ou téléphone.

Vous trouverez tout en bas de ce document les modalités pour apporter votre pierre à l'édifice.

## Comment l'utiliser ?

Cet outil respecte les us et coutumes du cadriciel grâce auquel il est développé : [Ruby on Rails](http://www.rubyonrails.org/). Les instructions que vous trouverez ci-après ne devraient pas surprendre un habitué… Pour lancer OLIGAA, vous devez pouvoir exécuter Ruby dans la version mentionnée par le [.ruby-version](.ruby-version). Si vous devez l'installer, nous vous recommandons l'utilisation de [rbenv](https://github.com/rbenv/rbenv) et son greffon [ruby-build](https://github.com/rbenv/ruby-build).

Si ce que vous venez de lire ne vous parle pas, nous vous invitons à vous rapprocher d'une personne à l'aise avec les mots-clés ci-dessus :-)

### Poste de développement

Pour simplifier la mise en place d'un environnement de développement le choix a été fait d'utiliser [Docker](https://www.docker.com/), mais il est tout à fait possible d'installer l'application de manière conventionnelle, modulo quelques adaptations (*mysql*, *mailcatcher*, *overmind*).

Pour Docker :
1. installer [Docker](https://docs.docker.com/engine/install/)
1. installer [docker-compose](https://docs.docker.com/compose/install/)
1. construire l'image : `docker-compose build`
1. récupérer les images et lancer les conteneurs : `docker-compose up`
1. lister des conteneurs actifs : `docker-compose ps`
1. accéder au terminal du conteneur de l'application : `docker-compose exec app bash`
1. arrêter les conteneurs : `docker-compose down`

Avant de démarrer l'application pour la première fois, vous devez :

1. Accéder au terminal du conteneur si vous utilisez Docker ;
1. Installer les dépendances : `bundle install` ;
1. Configurer votre environnement
    1. Copier le fichier `.env.example` vers `.env` ;
    1. Modifier les informations dans ce nouveau fichier.
1. Initialiser la base de données : `bundle exec rake db:setup` ;
1. Créer le compte administrateur : `bundle exec rake oligaa:create_admin`
     * N'oubliez pas de noter le mot de passe affiché dans votre terminal.

Vous pouvez alors vous connecter avec l'identifiant `admin` et le mot de passe noté plus tôt.

#### Exécution des tests

Tout nouveau développement doit être accompagné de son jeu de tests, qui garantira qu'aucune régression ne pourra se glisser dans le futur. Pour leur écriture, nous utilisons la bibliothèque [RSpec](http://rspec.info/) pour tous les types de test. Pour vous assurer que tout fonctionne comme attendu, vous pouvez lancer la suite de tests complète et vérifier que tout se déroule sans erreur, grâce à la commande habituelle :
```
$ bundle exec rake
[…]
XXX examples, 0 failures
```

Avec Docker :
1. se connecter au conteneur de l'application : `docker-compose exec app bash`
1. lancer les tests : `xvfb-run bundle exec rake`

### Déploiement sur serveur

OLIGAA se déploie comme nombre d'applications Rails, sur hébergement dédié ou mutualisé. Les solutions habituelles telles que [Phusion Passenger](https://www.phusionpassenger.com/) ou _Nginx/Apache + Puma/Unicorn/Thin…_ fonctionnent sans problème. À titre d'exemple, le CDSA33 utilise un serveur dédié servi par _Apache + Passenger_.

Vous devez également avoir à disposition une base de données relationnelle : MariaDB est recommandée car c'est celle qui est utilisée en production et pour les tests, mais PostgreSQL devrait faire l'affaire sans problème.

**NB :** OLIGAA n'a pas été testé sur les plateformes de _cloud PaaS_ telles que Heroku.

## Comment contribuer ?

**Le code source n'est pas la seule contribution de valeur**. Avis, problèmes, guides d'utilisation… Entrez dans la danse et appréciez la construction collaborative de biens communs offerte par le logiciel libre.

Informations détaillées et coordonnées dans le [guide de contribution](CONTRIBUTING.md).
