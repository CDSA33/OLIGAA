#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
namespace :oligaa do
  desc "create an admin member to connect to the admin zone"
  task :create_admin => :environment do
    admin = Member.create!({
      :login => 'admin',
      :name => 'admin',
      :contacts_attributes => {
        '0' => {
          :last_name => 'admin',
          :first_name => 'admin',
          :email => 'admin@admin.fr'
        },
      }
    })
    admin.update_column(:admin, true)
    admin.reset_password
    puts "The password of the admin user is '#{admin.default_password}'"
  end

  namespace :audit do
    desc "Check for virtual attachments"
    task :attached_files => :environment do
      AttachedFile.all.each do |attcht|
        begin
          novel = attcht.attachable
          File.open(attcht.file.path)
        rescue Errno::ENOENT => e
          puts "Attachment #{attcht.id} (#{attcht.name}) is virtual for novel '#{novel.title}' with id #{novel.id.to_s}"
        end
      end
    end
  end

  namespace :cleanup do
    desc "Check for virtual attachments"
    task :attached_files => :environment do
      AttachedFile.all.each do |attcht|
        begin
          novel = attcht.attachable
          File.open(attcht.file.path)
        rescue Errno::ENOENT => e
          if attcht.destroy
            puts "Virtual attachment #{attcht.id} (#{attcht.name}) for novel '#{novel.title}' with id #{novel.id.to_s} has been removed"
          end
        end
      end
    end
  end

  namespace :members do
    desc "geocode members address"
    task :geocode => :environment do
      Member.all.each do |member|
        address = member.address
        member.update_column(:address, nil)
        member.address = address
        member.save!
      end
    end
  end
end

namespace :jobs do
  desc "Restart delayed_job_monitor if no worker yet running"
  task :restart do
    maintenance_file = Rails.root + 'public/system/maintenance.html'
    unless maintenance_file.exist?
      cmd = "#{Rails.root}/script/delayed_job"
      args = ["start", "-m"]
      exec cmd, *args
    end
  end
end