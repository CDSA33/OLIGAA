#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
crumb :root do
  link "Accueil", root_path
end

crumb :menu do |menu|
  link menu.nil? ? 'Aucun menu' : menu.title
end

crumb :page do |page|
  link page.title, page
  parent :menu, page.menu
end

crumb :activity_category_menu do
  link 'Activités'
end

crumb :activity_category do |activity_category|
  link activity_category.title, activity_category
  parent :activity_category_menu
end

crumb :activity do |activity|
  link activity.title
  parent :activity_category_menu
end

crumb :member_group_menu do
  link 'Où pratiquer'
end

crumb :member_group do |member_group|
  link member_group.title
  parent :member_group_menu
end

crumb :member do |member|
  link member.name
  parent :member_group_menu
end

crumb :calendar do |date|
  link "Calendrier #{I18n.localize(date, :format => "%B %Y")}", calendar_path(year: date.year, month:date.month)
end

crumb :event do |event|
  link event.title, event
  parent :calendar, event.begin_at unless event.begin_at.nil?
end

crumb :novel do |novel|
  link novel.title, novel
end

crumb :newsletter_menu do
  link "Lettres d'information"
end

crumb :newsletter_archives do
  link "Archives", newsletters_path
  parent :newsletter_menu
end

crumb :newsletter do |newsletter|
  link newsletter.title, newsletter
  parent :newsletter_menu
end

# If you want to split your breadcrumbs configuration over multiple files, you
# can create a folder named `config/breadcrumbs` and put your configuration
# files there. All *.rb files (e.g. `frontend.rb` or `products.rb`) in that
# folder are loaded and reloaded automatically when you change them, just like
# this file (`config/breadcrumbs.rb`).