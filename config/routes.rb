#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
Rails.application.routes.draw do
  resources :events
  resources :event_types do
    patch :update_ga_events, :on => :member
  end
  resources :menus, :except => [:index, :show] do
    member do
      patch :move_up
      patch :move_down
    end
  end
  resources :pages do
    member do
      patch :move_up
      patch :move_down
    end
  end
  resources :activity_categories, :except => :index
  resources :activities
  resources :sessions, :only => [:new, :create, :destroy]
  resource :settings, :only => [:edit, :update]
  resources :novel_types, :except => :index
  resources :novels do
    collection do
      get :public_index
      get :feed
    end
    get :duplicate, :on => :member
  end
  resources :members do
    get :raz_pass, :on => :member
    post :search_contacts, on: :collection
    get :map, on: :collection
    resources :performances, except: [:index, :show] do
      patch :toggle, on: :member
    end
    resources :subscriptions do
      get :option, on: :collection
      get :card, on: :member
      patch :toggle_billing, on: :member
      get :renew, on: :member
    end
  end
  resources :member_groups, :except => [:index] do
    member do
      patch :filter_members
      get :members
    end
  end
  resources :public_types
  resources :emails do
    patch :send_off, :on => :member
    get :recipients_by_member_groups, :on => :member
    get :recipients_by_contact_types, :on => :member
  end
  resources :contact_types, :except => :show
  resource :contact_us, :only => [:new, :create], :path_names => { :new => "write"}
  resources :newsletters
  resources :partners do
    member do
      patch :move_up
      patch :move_down
    end
    get :display, on: :collection
  end
  resources :partners_groups, except: [:index, :show] do
    member do
      patch :move_up
      patch :move_down
    end
  end
  resources :club_novels, except: :show

  patch '/newsletter_items/:id/move_up' => 'newsletter_items#move_up', as: 'move_up_item'
  patch '/newsletter_items/:id/move_down' => 'newsletter_items#move_down', as: 'move_down_item'

  get 'login' => 'sessions#new', :as => :login
  get 'logout' => 'sessions#destroy', :as => :logout

  get '/calendar(/:year(/:month))' => 'calendar#show', :as => :calendar, :constraints => {:year => /\d{4}/, :month => /\d{1,2}/}
  get '/calendar(/:year(/:month.pdf))' => 'calendar#show', :as => :calendar_pdf, :format => :pdf, :constraints => {:year => /\d{4}/, :month => /\d{1,2}/}
  patch 'filter_calendar' => 'calendar#update', :as => :filter_calendar

  get '/help/redcloth' => 'help#redcloth', :as => :redcloth_help

  get '/sitemap' => 'sitemap#index', :as => :sitemap

  get '/platform' => 'platform#index', as: :platform

  root :to => 'novels#public_index'
end
