ValidatesTimeliness.setup do |config|
  config.enable_multiparameter_extension!
  config.enable_date_time_select_extension!
end
