#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
source 'https://rubygems.org'

gem 'rails', '5.1.7'
gem 'sass-rails'
gem 'uglifier'
gem 'coffee-rails'
gem 'jquery-rails'

gem 'dotenv-rails'
gem "paperclip"
gem "hirb"
gem "acts_as_list"
gem "pdfkit"
gem "will_paginate"
gem "truncate_html"
gem 'google-api-client', '~> 0.8.0'
gem 'delayed_job'
gem 'delayed_job_active_record'
gem 'daemons'
gem 'rdoc'
gem 'fullcalendar-rails'
gem 'momentjs-rails'
gem 'formatize', git: 'https://github.com/infoPiiaf/formatize.git'
gem 'simple_form'
gem "nested_form"
gem 'geocoder'
gem 'leaflet-rails'
gem 'gretel'
gem 'gretel-trails', git: 'https://github.com/infoPiiaf/gretel-trails.git', branch: 'fix-alias-method-chain-error'
gem 'piwik_analytics', git: 'https://git.framasoft.org/infopiiaf/piwik_analytics.git'
gem 'jquery-ui-rails'
gem 'compass-rails'
gem 'susy'
gem 'clipboard-rails'
gem 'fitvidsjs_rails'
gem 'font-awesome-rails'
gem 'jquery-minicolors-rails'
gem 'prettyphoto-rails'
gem 'validates_timeliness', '~> 5.0.0.alpha3'
gem 'mysql2'

group :development, :test do
  gem 'rspec-rails'
  gem 'byebug'
  gem 'listen'
end

group :test do
  gem 'simplecov', '>= 0.4.0', require: false
  gem 'webmock', '~> 1.17.4'
  gem 'vcr', '~> 2.9.3'
  gem 'timecop', '~> 0.8.0'
  gem 'capybara'
  gem 'launchy'
  gem 'factory_girl_rails'
  gem 'shoulda-matchers', git: 'https://github.com/thoughtbot/shoulda-matchers.git', branch: 'rails-5'
  gem 'test-unit', '~> 3.0' # For shoulda-matchers with ruby 2.3+
  # see https://github.com/thoughtbot/capybara-webkit/issues/1065
  gem 'capybara-webkit', github: 'thoughtbot/capybara-webkit', branch: 'master'
  gem 'fuubar'
  gem 'faker'
end

group :development do
  gem 'brakeman'
  gem 'puma'
  gem 'web-console'
end
