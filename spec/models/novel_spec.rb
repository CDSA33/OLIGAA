#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

describe Novel do
  describe "associations" do
    it { is_expected.to belong_to(:novel_type) }
    it { is_expected.to have_many(:attached_files) }
    it { is_expected.to have_many(:emails) }
    it { is_expected.to have_and_belong_to_many(:activities) }
    it { is_expected.to have_attached_file(:picture) }
  end

  describe "validations" do
    it "default factory is valid" do
      expect(build(:novel)).to be_valid
    end
    it { is_expected.to validate_presence_of(:title) }
    it { is_expected.to validate_presence_of(:content) }
  end

  describe "scopes" do
    it "sorts by descending updated date by default" do
      n1 = create :novel
      n2 = create :novel
      n1.update_attribute(:title, "Nouveau titre")
      expect(Novel.all).to eq [n1, n2]
    end

    it ".templates returns only template novels" do
      n1 = create :novel
      n2 = create :novel, template: true
      expect(Novel.templates).to eq [n2]
    end

    it ".without_templates returns only non template novels" do
      n1 = create :novel
      n2 = create :novel, template: true
      expect(Novel.without_templates).to eq [n1]
    end

    it ".public_ones returns only published novels" do
      n1 = create :novel
      n2 = create :novel, public: true
      expect(Novel.public_ones).to eq [n2]
    end

    it ".untyped returns only novels without type" do
      nt = create :novel_type
      n1 = create :novel
      n2 = create :novel, novel_type: nt
      expect(Novel.untyped).to eq [n1]
    end
  end

  describe "behaviors" do
    it "#public_label presents the published status" do
      n = create :novel, public: false
      expect(n.public_label).to eq("NON")
      n.update_attribute :public, true
      expect(n.public_label).to eq("OUI")
    end

    it "#template_label presents the template status" do
      n = create :novel, template: false
      expect(n.template_label).to eq("NON")
      n.update_attribute :template, true
      expect(n.template_label).to eq("OUI")
    end

    it "#novel_type_label presents the novel type status" do
      nt = create :novel_type
      n = create :novel, novel_type: nil
      expect(n.novel_type_label).to eq("Aucun")
      n.update_attribute :novel_type_id, nt.id
      expect(n.novel_type_label).to eq(nt.title)
    end

    it "#activities_label presents the associated activities" do
      n = create :novel
      expect(n.activities_label).to eq("Aucune activité en particulier.")
      a1 = create :activity, title: "Athle"
      a2 = create :activity, title: "Foot"
      n.update_attributes(activities: [a1, a2])
      expect(n.activities_label).to eq("Athle / Foot")
    end

    it ".duplicate duplicates a novel as a non template private one" do
      n1 = create :novel_with_picture
      attached_file = create :attached_file, attachable: n1
      n2 = Novel.duplicate(n1.id)
      expect(n2.template).to be_falsey
      expect(n2.public).to be_falsey
      expect(n2).not_to eq n1
      expect(n2).to be_persisted
      expect(n2.attached_files.count).to eq(1)
      expect(n2.attached_files.first.name).to eq(attached_file.name)
      expect(n2.picture).not_to be_nil
      expect(n2.picture).not_to eq(n1.picture)
      expect(n2.activities).to eq(n1.activities)
    end

    describe ".smart_picture_url" do
      it "returns the picture url specified in the novel itsef" do
        novel = create :novel_with_picture
        # Then
        expect(novel.smart_picture_url).to eq(novel.picture.url(:thumb))
      end

      it "returns the first activity poster url if no picture is specified in the novel" do
        a1 = create :activity_with_poster
        a2 = create :activity_with_poster
        novel = create :novel
        novel.update_attributes(activities: [a1, a2])
        # Then
        expect(novel.smart_picture_url).to eq(a1.poster.url(:standard))
      end

      it "returns the first activity with poster" do
        a1 = create :activity
        a2 = create :activity_with_poster
        novel = create :novel
        novel.update_attributes(activities: [a1, a2])
        # Then
        expect(novel.smart_picture_url).to eq(a2.poster.url(:standard))
      end

      it "returns nil of no picture is available" do
        novel = create :novel
        # Then
        expect(novel.smart_picture_url).to be_nil
      end
    end
  end
end
