#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

describe Contact do
  describe "associations" do
    it {is_expected.to have_many(:phone_numbers) }
    it {is_expected.to belong_to(:member)}
    it {is_expected.to belong_to(:contact_type)}
  end

  describe "validations" do
    it "default factory is valid" do
      expect(build(:contact)).to be_valid
    end
    it { is_expected.to validate_presence_of(:last_name) }
    it { is_expected.to validate_presence_of(:first_name) }
    it { is_expected.to validate_presence_of(:email) }
    it { is_expected.to allow_value("contact@example.com").for(:email)}
    it { is_expected.not_to allow_value("contact").for(:email) }
    it { is_expected.not_to allow_value("contact@example").for(:email) }
  end

  describe "behaviors" do
    it "#full_info displays customized info" do
      ct = create :contact_type, title: "Président"
      contact = create :contact, first_name: "Jean", last_name: "Dupont", email: "jean.dupont@example.com", contact_type: ct
      expect(contact.full_info).to eq("Jean Dupont, Président (jean.dupont@example.com)")
      contact = create :contact, first_name: "Jean", last_name: "Dupont", email: "jean.dupont@example.com", contact_type: nil
      expect(contact.full_info).to eq("Jean Dupont (jean.dupont@example.com)")
    end

    it "creates a new contact type if proper field filled" do
      expect(ContactType.find_by_title("Président")).to be_nil
      create :contact, new_function: "Président"
      expect(ContactType.find_by_title("Président")).not_to be_nil
    end

    it ".untyped lists all contacts with no associated type" do
      ct = create :contact_type
      (c1 = build(:contact, contact_type: ct, member: nil)).save(validate: false)
      (c2 = build(:contact, contact_type: nil, member: nil)).save(validate: false)
      expect(Contact.untyped).to eq [c2]
    end
  end
end
