#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

describe Activity do
  describe "associations" do
    it {is_expected.to have_and_belong_to_many(:activity_categories) }
    it {is_expected.to have_and_belong_to_many(:events) }
    it {is_expected.to have_and_belong_to_many(:members) }
    it {is_expected.to have_and_belong_to_many(:novels) }
    it {is_expected.to have_many(:attached_files) }
    it {is_expected.to have_many(:performances) }
  end

  describe "validations" do
    it "default factory is valid" do
      expect(build(:activity)).to be_valid
    end
    it { is_expected.to validate_presence_of(:title) }
    it { is_expected.to validate_presence_of(:presentation) }
  end

  describe "scopes" do
    it "sorts by ascending title by default" do
      a1 = create(:activity, title: "B")
      a2 = create(:activity, title: "A")
      expect(Activity.all).to eq [a2, a1]
    end

    it ".uncategorized returns activities with no category" do
      cat = create :activity_category
      a1 = create :activity
      a1.activity_categories << cat
      a2 = create :activity
      expect(Activity.uncategorized).to eq [a2]
    end

    it ".filterable only returns activities available for members filter" do
      a1 = create :activity
      a2 = create :activity, filterable: false
      expect(Activity.filterable).to eq [a1]
    end

    it ".listed only returns activities used for the activity category show page" do
      a1 = create :activity, listed: true
      a2 = create :activity, listed: false
      expect(Activity.listed).to eq [a1]
    end
  end

  describe "behaviors" do
    it_behaves_like "seasoned_events"
  end
end
