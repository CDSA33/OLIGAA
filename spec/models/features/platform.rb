#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper.rb'

describe Features::Platform do

  it "is enabled when all settings prices provided" do
    expect(Features::Platform.enabled?).to be_truthy
  end

  it "is disabled when no settings price provided" do
    # Given
    platform_prices = {
      mono_activity_collaborating_price: nil,
      mono_activity_standard_price: nil,
      multi_activities_collaborating_price: nil,
      multi_activities_standard_price: nil
    }
    @settings.update_attributes(platform_prices)
    # When
    # Then
    expect(Features::Platform.enabled?).to be_falsy
  end

  it "is disabled when any settings price not provided" do
    @settings.update_attribute(:mono_activity_standard_price, nil)
    # When
    # Then
    expect(Features::Platform.enabled?).to be_falsy
  end
end
