#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

RSpec.describe Adherent, type: :model do
  describe "associations" do
    it {is_expected.to have_many(:subscriptions)}
  end

  describe "validations" do
    it "default factory is valid" do
      expect(build(:adherent)).to be_valid
    end
    it { is_expected.to validate_presence_of(:last_name) }
    it { is_expected.to validate_presence_of(:first_name) }
    it { is_expected.to validate_presence_of(:born_on) }
    it { is_expected.to validate_presence_of(:permit_number) }
    it "two adherents can't have the same permit number" do
      create :adherent, permit_number: "123456"
      new_adherent = build :adherent, permit_number: "123456"
      expect(new_adherent).not_to be_valid
      expect(new_adherent.errors[:permit_number].first).to eq "Ce numéro de licence est déjà pris pour cette saison. Si vous pensez qu'il s'agit du bon numéro, merci de prendre contact avec le CDSA33."
    end
    context "regarding medicosocial org" do
      it "can be associated to an existing one" do
        medico_social_org = create :member
        adherent = create :adherent, medico_social_org: medico_social_org, other_medicosocial_org: ""
        expect(adherent.medico_social_org).to eq medico_social_org
        expect(adherent.other_medicosocial_org).to be_blank
      end
      it "can have a custom one" do
        adherent = build :adherent, medico_social_org: nil, other_medicosocial_org: ""
        expect(adherent).not_to be_valid
        adherent.other_medicosocial_org = "Mon asso"
        adherent.save! and adherent.reload
        expect(adherent.medico_social_org).to be_nil
        expect(adherent.other_medicosocial_org).to eq "Mon asso"
      end
      it "forces to the existing one if a custom one is also provided" do
        medico_social_org = create :member
        adherent = create :adherent, medico_social_org: medico_social_org, other_medicosocial_org: "Mon asso"
        expect(adherent.medico_social_org).to eq medico_social_org
        expect(adherent.other_medicosocial_org).to be_blank
      end
    end
  end

  describe "behaviors" do
    it "#name returns concatenated value" do
      adherent = build :adherent, last_name: "Durand", first_name: "Nicolas"
      expect(adherent.name).to eq "Nicolas Durand"
    end

    it "#medico_social_org_name returns the name of the associated medico-social org" do
      medico_social_org = create :member, name: "IME one"
      adherent = build :adherent, medico_social_org: medico_social_org
      expect(adherent.medico_social_org_name).to eq "IME one"
      adherent = build :adherent, medico_social_org: nil, other_medicosocial_org: "IME two"
      expect(adherent.medico_social_org_name).to eq "IME two"
    end
  end

  describe "scopes" do

    context ".available_for_current_season" do

      let(:params) {
        {
          first_name: "Jean",
          last_name: "Dupont",
          born_on: "14/07/1989",
          permit_number: "123456",
          medico_social_org_id: nil,
          other_medicosocial_org: nil
        }
      }

      context "adherent does not exist" do
        it "returns a just built one" do
          # Given
          # When
          result = Adherent.available_for_current_season(params)
          # Then
          expect(result).to be_a Adherent
          expect(result).not_to be_persisted
        end
      end

      context "adherent alreaday exists" do

        let!(:existing_adherent) { create :adherent, id: 42, permit_number: "123456" }

        context "within another season" do

          before do
            Timecop.freeze(Date.new(2016,9,10))
            create :subscription, adherent: existing_adherent
          end

          after { Timecop.return }

          context "when :id provided (renewal)" do
            it "returns the adherent" do
              # Given
              Timecop.travel(Date.new(2017,9,10))
              params[:id] = 42
              # When
              result = Adherent.available_for_current_season(params)
              # Then
              expect(result).to eq existing_adherent
              expect(result).to be_persisted
            end
          end

          context "when :id not provided (subscription wihtin another club)" do
            it "returns the adherent" do
              # Given
              Timecop.travel(Date.new(2017,9,10))
              # When
              result = Adherent.available_for_current_season(params)
              # Then
              expect(result).to eq existing_adherent
              expect(result).to be_persisted
            end
          end
        end

        context "Within the same season" do

          before do
            Timecop.freeze(Date.new(2018,9,10))
            create :subscription, adherent: existing_adherent
          end

          after { Timecop.return }

          it "returns a just built one (for validations purpose)" do
            # Given
            params[:id] = 42
            # When
            result = Adherent.available_for_current_season(params)
            # Then
            expect(result).not_to eq existing_adherent
            expect(result).not_to be_persisted
            expect(result.id).to be_nil
          end
        end
      end
    end
  end
end
