#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

describe ActivityCategory do
  describe "associations" do
    it {is_expected.to have_and_belong_to_many(:activities) }
  end

  describe "validations" do
    it "default factory is valid" do
      expect(build(:activity_category)).to be_valid
    end
    it { is_expected.to validate_presence_of(:title) }
  end



  describe "scopes" do
    it "sorts by ascending title by default" do
      cat1 = create(:activity_category, title: "B")
      cat2 = create(:activity_category, title: "A")
      expect(ActivityCategory.all).to eq [cat2, cat1]
    end
  end

  describe "behaviors" do
    it "#to_params customizes the URL with id then text" do
      page = create(:activity_category, title: "éèê à ïî ùû ôö $%€")
      expect(page.to_param).to eq("#{page.id}-eee_a_ii_uu_oo_")
    end
  end
end