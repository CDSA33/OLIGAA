#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

describe Website do
  describe "validations" do
    specify "default factory is valid" do
      expect(build(:website)).to be_valid
    end
    it { is_expected.to allow_value("contact@example.com").for(:contact_email) }
    it { is_expected.to allow_value("contact-all@net.example.com").for(:contact_email) }
    it { is_expected.not_to allow_value("contact").for(:contact_email) }
    it { is_expected.not_to allow_value("contact@example").for(:contact_email) }

    describe "regarding season birthdate" do
      it "validates the date is correct" do
        # Only check one value, the purpose is not to
        # test again the validates_timeliness gem ;)
        # Given
        settings = build :website
        birthdate_attributes = {
          "season_birthdate(3i)": "31",
          "season_birthdate(2i)": "2",
          "season_birthdate(1i)": "1970"
        }
        # When
        settings.assign_attributes(birthdate_attributes)
        # Then
        expect(settings).not_to be_valid
      end
    end
  end
end
