#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

RSpec.describe Subscription, type: :model do
  describe "associations" do
    it {is_expected.to belong_to(:adherent)}
  end

  describe "validations" do
    it "default factory is valid" do
      expect(build(:subscription)).to be_valid
    end
  end

  describe "scopes" do
    it ".for_performances returns subscriptions for specific performances" do
      member1 = create :member
      member2 = create :member
      activity1 = create :activity
      activity2 = create :activity
      perf1 = create :performance, member: member1, activity: activity1
      perf2 = create :performance, member: member1, activity: activity2
      perf3 = create :performance, member: member2, activity: activity1
      adherent1 = build :adherent
      adherent2 = build :adherent
      subscription1 = create :subscription, adherent: adherent1, performances: [perf1,perf3], member_id: member1.id
      subscription2 = create :subscription, adherent: adherent2, performances: [perf2], member_id: member2.id
      expect(Subscription.for_performances([perf1,perf3].map(&:id))).to eq [subscription1]
    end

    it ".by_adherent_name returns subscriptions ordered by ascending adherent name" do
      member = create :member
      activity = create :activity
      perf = create :performance, member: member, activity: activity
      jean_dupont = build :adherent, first_name: "Jean", last_name: "Dupont"
      arthur_latrouille = build :adherent, first_name: "Arthur", last_name: "Latrouille"
      jean_dupond = build :adherent, first_name: "Jean", last_name: "Dupond"
      adh_jean_dupont = create :subscription, adherent: jean_dupont, performances: [perf], member_id: member.id
      adh_arthur_latrouille = create :subscription, adherent: arthur_latrouille, performances: [perf], member_id: member.id
      adh_jean_dupond = create :subscription, adherent: jean_dupond, performances: [perf], member_id: member.id
      expect(Subscription.by_adherent_name).to eq [adh_arthur_latrouille,adh_jean_dupond,adh_jean_dupont]
    end

    it ".with_multiactivities_pass returns only the ones for multi activities" do
      member = create :member
      perf1 = create :performance
      perf2 = create :performance
      multi_sub1 = create :subscription, performances: [perf1, perf2], member_id: member.id
      multi_sub2 = create :subscription, performances: [perf1, perf2], member_id: member.id
      mono_sub = create :subscription, performances: [perf1], member_id: member.id
      expect(member.subscriptions.with_multiactivities_pass).to eq [multi_sub1, multi_sub2]
    end

    it ".billed returns only billed subscriptions" do
      subscription1 = create :subscription
      subscription2 = create :subscription, billed_at: Time.now
      expect(Subscription.billed).to eq [subscription2]
    end

    it ".of_medicosocial_org returns subscriptions of a specific medicosocial org" do
      medicosocial_org = create :member
      specific_adherent = create :adherent, medico_social_org: medicosocial_org
      sub1 = create :subscription, adherent: specific_adherent
      sub2 = create :subscription
      expect(Subscription.of_medicosocial_org(medicosocial_org)).to eq [sub1]
    end

    it ".for_season returns subscription between beginning and end of a season" do
      # Given
      season = Season.new(Date.new(2017,9,1))
      sub1 = create :subscription, created_at: DateTime.new(2017,1,1,10,10,0)
      sub2 = create :subscription, created_at: DateTime.new(2017,10,1,10,10,0)
      sub3 = create :subscription, created_at: DateTime.new(2018,1,1,10,10,0)
      sub4 = create :subscription, created_at: DateTime.new(2018,10,1,10,10,0)
      # When
      result = Subscription.for_season(season)
      # Then
      expect(result).to eq [sub2, sub3]
    end
  end

  describe "behaviors" do
    it "#external_clubs returns other members where adherent practices" do
      main_club = create :member
      other_club1 = create :member
      other_club2 = create :member
      activity = create :activity
      perf1 = create :performance, member: main_club, activity: activity
      perf2 = create :performance, member: other_club1, activity: activity
      perf3 = create :performance, member: other_club2, activity: activity
      adherent = build :adherent
      subscription = create :subscription, adherent: adherent, performances: [perf1, perf2, perf3], member_id: main_club.id
      expect(subscription.external_clubs).to eq [other_club1, other_club2]
    end

    it "#season returns the season the subscription belongs to" do
      date = Date.new(2017,9,19)
      subscription = create :subscription, created_at: date
      expect(subscription.season).to be_a Season
    end

    describe "#multipass? " do
      let(:perf1) { create :performance }
      let(:perf2) { create :performance }

      it "returns true if subscription associated to multi activities" do
        subscription = create :subscription, performances: [perf1, perf2]
        expect(subscription.multipass?).to be_truthy
      end

      it "returns false if subscription associated to mono activity" do
        subscription = create :subscription, performances: [perf1]
        expect(subscription.multipass?).to be_falsy
      end
    end

    context "#toggle_billing sets billed_at to" do
      it "current date if subscription was not billed" do
        subscription = create :subscription, billed_at: nil
        subscription.toggle_billing
        expect(subscription.reload.billed_at).not_to be_nil
      end

      it "nil if subscription was previously billed" do
        subscription = create :subscription, billed_at: DateTime.now.in_time_zone
        subscription.toggle_billing
        expect(subscription.reload.billed_at).to be_nil
      end
    end

    context "#billing_status returns" do
      it "'not-billed' when not billed" do
        subscription = create :subscription, billed_at: nil
        expect(subscription.billing_status).to eq 'not-billed'
      end

      it "'billed' when billed" do
        subscription = create :subscription, billed_at: DateTime.now
        expect(subscription.billing_status).to eq 'billed'
      end
    end

    context "on deletion" do
      it "deletes adherent if not associated to any other subscription" do
        subscription = create :subscription
        expect {
          subscription.destroy
        }.to change(Adherent, :count).from(1).to(0)
      end

      it "does not delete adherent if associated to another subscription" do
        adherent = create :adherent
        subscription = create :subscription, adherent: adherent
        create :subscription, adherent: adherent
        expect {
          subscription.destroy
        }.to_not change(Adherent, :count)
        expect(Subscription.count).to eq 1
      end
    end
  end
end
