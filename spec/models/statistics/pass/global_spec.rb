#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

RSpec.describe Statistics::Pass::Global, type: :model do

  let!(:season) { season = Season.new(Date.new(2017,9,1)) }

  context "regading initialization" do
    context "sets @member_units" do
      it "as an array when array provided" do
        members = []
        2.times { members << create(:member) }
        stats = Statistics::Pass::Global.new(members, season)
        expect(stats.instance_variable_get(:@member_units).length).to eq 2
        stats.instance_variable_get(:@member_units).each do |unit|
          expect(unit).to be_a Statistics::Pass::Unit
        end
      end
    end
  end

  context "regarding calculations" do

    before do
      @mg = create :member_group
      member1 = create :member, member_groups: [@mg]
      member2 = create :member, member_groups: [@mg]
      member3 = create :member
      perf1 = create :performance, member: member1
      perf2 = create :performance, member: member2
      5.times { create :subscription, performances: [perf1], member_id: member1.id, created_at: "15/09/2017" }
      3.times { create :subscription, performances: [perf1, perf2], member_id: member1.id, created_at: "15/09/2017" }
      2.times { create :subscription, performances: [perf1], member_id: member2.id, created_at: "15/09/2017" }
      5.times { create :subscription, performances: [perf1, perf2], member_id: member2.id, created_at: "15/09/2017" }
      3.times { create :subscription, performances: [perf1], member_id: member3.id, created_at: "15/09/2017" }
      2.times { create :subscription, performances: [perf1, perf2], member_id: member3.id, created_at: "15/09/2017" }
      @stats = Statistics::Pass::Global.new([member1, member2, member3], season)
    end

    context "the members are not collaborating clubs" do
      it "#total returns nil" do
        expect(@stats.total).to eq 0
      end

      it "#multi returns the number of its multi activities subscriptions" do
        expect(@stats.multi).to eq 0
      end

      it "#mono returns the number of its mono activity subscriptions" do
        expect(@stats.mono).to eq 0
      end

      it "#label returns '(3 multi, 5 mono sur 8)'" do
        expect(@stats.label).to be_nil
      end
    end

    context "some members are collaborating clubs" do
      before { Settings.instance.reload.platform_collaborating_clubs << @mg }

      it "#total returns the sum of subscriptions of each collaborator member" do
        expect(@stats.total).to eq 15
      end

      it "#multi returns the sum of multi activities subscriptions of each collaborator member" do
        expect(@stats.multi).to eq 8
      end

      it "#mono returns the sum of mono activity subscriptions of each collaborator member" do
        expect(@stats.mono).to eq 7
      end

      it "#label returns '(8 multi, 7 mono sur 15)'" do
        expect(@stats.label).to eq '(8 multi, 7 mono sur 15)'
      end
    end
  end
end
