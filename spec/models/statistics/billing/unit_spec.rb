#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

RSpec.describe Statistics::Billing::Unit, type: :model do

  let!(:season) { season = Season.new(Date.new(2017,9,1)) }

  context "initialization" do
    it "sets @member" do
      member = create(:member)
      stats = Statistics::Billing::Unit.new(member, season)
      expect(stats.instance_variable_get(:@member)).to eq member
      expect(stats.instance_variable_get(:@season)).to eq season
    end
  end

  context "calculations" do

    before do
      @mg = create :member_group
      @member = create :member, member_groups: [@mg]
      perf1 = create :performance, member: @member
      perf2 = create :performance, member: @member
      3.times { create :subscription, performances: [perf1], member_id: @member.id, created_at: "15/09/2016" }
      3.times { create :subscription, performances: [perf1], member_id: @member.id, created_at: "15/09/2017" }
      5.times { create :subscription, performances: [perf1, perf2], member_id: @member.id, created_at: "15/09/2016" }
      5.times { create :subscription, performances: [perf1, perf2], member_id: @member.id, created_at: "15/09/2017" }
      @stats = Statistics::Billing::Unit.new(@member, season)
    end

    context "the member is not a collaborating club" do
      it "#total returns nil" do
        expect(@stats.total).to eq 0
      end

      it "#billed returns nil" do
        expect(@stats.billed).to eq 0
      end

      it "#label returns nil" do
        expect(@stats.label).to be_nil
      end
    end

    context "the member is a collaborating club" do
      before { Settings.instance.reload.platform_collaborating_clubs << @mg }

      context "multipass subscriptions not billed" do
        it "#total returns the number of its multi activivities subscriptions" do
          expect(@stats.total).to eq 5
        end

        it "#billed returns the number of its billed multi activities subscriptions" do
          expect(@stats.billed).to eq 0
        end

        it "#label returns '(€ : 0 / 5)'" do
          expect(@stats.label).to eq '(€ : 0 / 5)'
        end
      end

      context "some multipass subscriptions billed" do
        before do
          @member.subscriptions.for_season(season).with_multiactivities_pass[0,3].each do |subscription|
            subscription.toggle_billing
          end
        end

        it "#total returns the number of its multi activivities subscriptions" do
          expect(@stats.total).to eq 5
        end

        it "#billed returns the number of its billed multi activities subscriptions" do
          expect(@stats.billed).to eq 3
        end

        it "#label returns '(€ : 3 / 5)'" do
          expect(@stats.label).to eq '(€ : 3 / 5)'
        end
      end
    end
  end
end
