#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

describe MemberGroup do
  describe "associations" do
    it {is_expected.to have_and_belong_to_many(:members) }
    it {is_expected.to have_many(:activities) }
  end

  describe "validations" do
    it "default factory is valid" do
      expect(build(:member_group)).to be_valid
    end
    it { is_expected.to validate_presence_of(:title) }
  end

  describe "scopes" do
    it ".by_title sorts by ascending title by default" do
      mg1 = create :member_group, title: "B"
      mg2 = create :member_group, title: "A"
      expect(MemberGroup.by_title).to eq [mg2, mg1]
    end

    it ".all_minus rejects a specific member group" do
      mg1 = create :member_group
      mg2 = create :member_group
      expect(MemberGroup.all_minus(mg1)).to eq [mg2]
    end

    it ".all_not_empty rejects member groups without members" do
      mg1 = create :member_group
      mg1.members << create(:member)
      mg2 = create :member_group
      expect(MemberGroup.all_not_empty).to eq [mg1]
    end

    it ".for_activity returns only the ones with members practicing a given activity" do
      athle = create :activity
      foot = create :activity
      mg1 = create :member_group
      mg2 = create :member_group
      mg3 = create :member_group
      m1 = create :member, activities: [athle], member_groups: [mg1]
      m2 = create :member, activities: [foot], member_groups: [mg1, mg2]
      m3 = create :member, activities: [athle, foot], member_groups: [mg3]
      m4 = create :member, activities: [athle, foot], member_groups: [mg1]
      expect(MemberGroup.for_activity(athle)).to eq [mg1, mg3]
    end
  end

  describe "behaviors" do
    it "#published? returns publication status of settings" do
      mg = create :member_group
      expect(mg).not_to be_published
      Settings.instance.map_member_groups << mg
      expect(mg).to be_published
    end

    it "does not delete members when deleted" do
      mg = create :member_group
      member = create(:member)
      mg.members << member
      mg.destroy
      expect(MemberGroup.count).to eq(0)
      expect(Member.all).to eq [member]
    end
  end


end
