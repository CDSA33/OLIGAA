#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

RSpec.describe ClubNovel, type: :model do

  context "associations" do
    it { is_expected.to have_many(:attached_files) }
    it { is_expected.to have_many(:images) }
    it { is_expected.to have_and_belong_to_many(:activities) }
  end

  context "validations" do
    it "default factory is valid" do
      expect(build(:club_novel)).to be_valid
    end
  end

  context "behavior" do
    describe "#published" do
      it "returns true when published_at set" do
        # Given
        club_novel = create :club_novel, :published
        # When
        result = club_novel.published
        # Then
        expect(result).to be_truthy
      end

      it "returns false when published_at not set" do
        # Given
        club_novel = create :club_novel
        # When
        result = club_novel.published
        # Then
        expect(result).to be_falsy
      end
    end
  end

  context "scopes" do

    context "regarding publication status" do
      let!(:published) { create :club_novel, :published }
      let!(:unpublished) { create :club_novel }

      it ".published returns only published club novels" do
        # Given
        # When
        result = ClubNovel.published
        # Then
        expect(result).to eq [published]
      end

      it ".unpublished returns only not published club novels" do
        # Given
        # When
        result = ClubNovel.unpublished
        # Then
        expect(result).to eq [unpublished]
      end
    end

    it ".ordered returns club novels by descending publication date" do
      # Given
      oldest = create :club_novel, published_at: Time.zone.now - 1.hour
      newest = create :club_novel, published_at: Time.zone.now
      # When
      result = ClubNovel.ordered
      # Then
      expect(result).to eq [newest, oldest]
    end
  end
end
