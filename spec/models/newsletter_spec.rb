#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

describe Newsletter do
  context "associations" do
    it { is_expected.to have_many(:items) }
    it { is_expected.to have_and_belong_to_many(:partners) }
  end

  context "validations" do
    it "default factory is valid" do
      expect(build(:newsletter)).to be_valid
    end
    it { is_expected.to validate_presence_of :title }
  end

   context "scopes" do
    it ".for_nav returns the 3 last published newsletters ordered by creation date" do
      n1 = create :newsletter, published: true
      n2 = create :newsletter, published: true
      n3 = create :newsletter, published: true
      n4 = create :newsletter, published: true
      n5 = create :newsletter, published: false
      expect(Newsletter.for_nav).to eq [n4, n3, n2]
    end

    it ".published returns only ppublished newsletters" do
      n1 = create :newsletter, published: true
      n2 = create :newsletter, published: false
      expect(Newsletter.published).to eq [n1]
    end

    it ".ordered returns newsletters by descending creation date" do
      n1 = create :newsletter
      n2 = create :newsletter
      n3 = create :newsletter
      n2.update_attribute(:title, "Nouveau titre")
      expect(Newsletter.ordered).to eq([n3, n2, n1])
    end
  end
end
