#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

describe Partner do
  describe "associations" do
    it { is_expected.to belong_to(:website) }
    it { is_expected.to belong_to(:group) }
    it { is_expected.to have_and_belong_to_many(:newsletters) }
  end

  describe "validations" do
    it "default factory is valid" do
      expect(build(:partner)).to be_valid
    end
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to allow_value("").for(:url) }
    it { is_expected.to allow_value("http://www.example.com").for(:url) }
    it { is_expected.to allow_value("https://example.com").for(:url) }
    it { is_expected.not_to allow_value("example.com").for(:url) }
    it { is_expected.not_to allow_value("ftp://example.com").for(:url) }
  end

  describe "scopes" do
    it ".ungrouped returns the partners associated to no group" do
      pg = create :partners_group
      p1 = create :partner, group: pg
      p2 = create :partner
      expect(Partner.ungrouped).to eq [p2]
    end

    it ".by_name returns by ascending name" do
      pg = create :partners_group
      p1 = create :partner, group: pg, name: "C"
      p2 = create :partner, group: pg, name: "B"
      p3 = create :partner, name: "A"
      expect(Partner.by_name).to eq [p3, p2, p1]
    end
  end

  describe "behaviors" do
    context "regarding positionning" do
      it "within a group" do
        pg = create :partners_group
        p1 = create :partner, group: pg     
        p2 = create :partner, group: pg
        expect(p1.position).to eq(1)
        expect(p2.position).to eq(2)
        p2.move_higher
        expect(p1.reload.position).to eq(2)
        expect(p2.position).to eq(1)  
        p2.move_lower
        expect(p1.reload.position).to eq(1)
        expect(p2.position).to eq(2) 
      end

      it "when changing the group" do
        pg1 = create :partners_group
        pg2 = create :partners_group        
        p1 = create :partner, group: pg1    
        p2 = create :partner, group: pg1
        p3 = create :partner, group: pg2
        p4 = create :partner, group: pg2
        expect(p1.position).to eq(1)
        expect(p2.position).to eq(2)
        expect(p3.position).to eq(1)
        expect(p4.position).to eq(2)
        p3.group = pg1 and p3.save!
        expect(p1.reload.position).to eq(1)
        expect(p2.reload.position).to eq(2)
        expect(p3.reload.position).to eq(3)
        expect(p4.reload.position).to eq(1)                     
      end

      it "with no group" do
        p1 = create :partner
        p2 = create :partner
        expect(p1.position).to eq(1)
        expect(p2.position).to eq(2)
        p2.move_higher
        expect(p1.reload.position).to eq(2)
        expect(p2.position).to eq(1)  
        p2.move_lower
        expect(p1.reload.position).to eq(1)
        expect(p2.position).to eq(2)         
      end
    end
  end
end