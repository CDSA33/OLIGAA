#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper.rb'

describe GoogleAgenda::Event, :vcr do

  let(:event_id) { 'ofpuuouo1o6pbfnsbg0nte4sd4' }
  let(:calendar_id) { '0gq96ive1alurv1s9dbp8qtouc@group.calendar.google.com' }
  let(:event_data) { {
    'id' => event_id,
    'organizer' => { 'email' => calendar_id }
    } }

  before(:all) do
    @key = OpenSSL::PKey::RSA.new 2048
  end

  before(:each) do
    @settings.update_attribute :gcal_user, 'test@gcal.org'
    allow(Google::APIClient::KeyUtils).to receive(:load_from_pkcs12)
      .with(File.join(Rails.root, "config", "google-oauth2", "keyfile.p12"), 'notasecret')
      .and_return(@key)
    GoogleAgenda::Service.reset!
  end

  describe 'self.find_by_id_or_create' do

    it 'returns an empty event if event ID is nil' do
      # When
      result = GoogleAgenda::Event.find_by_id_or_create nil, calendar_id
      # Then
      expect(result).to be_a GoogleAgenda::Event
    end

    it 'returns an empty event if calendar ID is nil' do
      # When
      result = GoogleAgenda::Event.find_by_id_or_create event_id, nil
      # Then
      expect(result).to be_a GoogleAgenda::Event
    end

    it 'returns a fully specified for valid event' do
      # When
      result = GoogleAgenda::Event.find_by_id_or_create event_id, calendar_id
      # Then
      expect(result).to be_a GoogleAgenda::Event
    end

  end

  describe '.save' do

    it 'returns the IDs of the created event and associated calendar' do
      # Given
      event = create(:event, begin_at: Time.now, end_at: Time.now,
                      event_type: create(:event_type, should_have_gcal: true, ga_cal_id: calendar_id))
      # When
      event_result, calendar_result = GoogleAgenda::Event.new.save event
      # Then
      expect(event_result).to be_a String
      expect(calendar_result).to eq calendar_id
    end

  end

  describe '.update' do

    it 'returns the IDs of the created event and associated calendar' do
      # Given
      event = create(:event, begin_at: Time.now, end_at: Time.now,
                      event_type: create(:event_type, should_have_gcal: true, ga_cal_id: calendar_id))
      # When
      event_result, calendar_result = GoogleAgenda::Event.new(event_data).update event
      # Then
      expect(event_result).not_to eq event_id
      expect(calendar_result).to eq calendar_id
    end

  end

  describe '.destroy' do

    it 'returns a result object on success' do
      # When
      result = GoogleAgenda::Event.new(event_data).destroy
      # Then
      expect(result).not_to be_nil
    end

  end

end