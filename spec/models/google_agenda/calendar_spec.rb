#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper.rb'

describe GoogleAgenda::Calendar, :vcr do

  before(:all) do
    @key = OpenSSL::PKey::RSA.new 2048
  end

  before(:each) do
    @settings.update_attribute :gcal_user, 'test@gcal.org'
    allow(Google::APIClient::KeyUtils).to receive(:load_from_pkcs12)
      .with(File.join(Rails.root, "config", "google-oauth2", "keyfile.p12"), 'notasecret')
      .and_return(@key)
    GoogleAgenda::Service.reset!
  end

  describe 'self.find_by_id_or_create' do

    it 'creates a new calendar if not found' do
      # When
      result = GoogleAgenda::Calendar.find_by_id_or_create nil
      # Then
      expect(result.cal['summary']).to start_with 'TEMP'
    end

    it 'returns an existing calendar when ID provided' do
      # Given
      expected_id = '0gq96ive1alurv1s9dbp8qtouc@group.calendar.google.com'
      # When
      result = GoogleAgenda::Calendar.find_by_id_or_create expected_id
      # Then
      expect(result.cal['id']).to eq expected_id
    end

  end

  describe '.update' do

    it 'returns the ID of the updated calendar' do
      # Given
      calendar_id = '0gq96ive1alurv1s9dbp8qtouc@group.calendar.google.com'
      calendar = GoogleAgenda::Calendar.new({'id' => calendar_id})
      # When
      result = calendar.update create(:event_type)
      # Then
      expect(result).to eq calendar_id
    end

  end

end