#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

RSpec.describe Performance, type: :model do
  describe "associations" do
    it { is_expected.to belong_to(:member) }
    it { is_expected.to belong_to(:activity) }
  end

  describe "validations" do
    it "default factory is valid" do
      expect(build(:performance)).to be_valid
    end
    it { is_expected.to validate_presence_of(:day) }
    it { is_expected.to validate_presence_of(:start_at) }
    it { is_expected.to validate_presence_of(:end_at) }
    [:start_at, :end_at].each do |param|
      it { is_expected.to allow_value("09:00","9:00","12:59","15h35").for(param) }
      it { is_expected.not_to allow_value("12:61").for(param) }
    end
    it "end_at is after start_at" do
      perf = build :performance, start_at: "15:00", end_at: "14:59"
      expect(perf).not_to be_valid
      perf.end_at = "15:01"
      expect(perf).to be_valid
    end
  end

  describe "behaviors" do
    before(:each) do
      athle = create :activity, title: "Athlétisme"
      member = create :member, name: "Mon Asso", activities: [athle]
      @performance = build :performance, day: :monday, start_at: "16:00", end_at: "18:00", activity: athle, member: member
    end

    it "#full_title returns self explanatory title" do
      expect(@performance.full_title).to eq "[Athlétisme] Lundi de 16h00 à 18h00"
    end
    it "#title_for_subscriptions_list returns title for subscriptions listing" do
      expect(@performance.title_for_subscriptions_list).to eq "Mon Asso (Athlétisme)"
    end
    it "#title_for_subscription_form returns title for subscription form" do
      expect(@performance.title_for_subscription_form).to eq "Mon Asso - [Athlétisme] Lundi de 16h00 à 18h00"
    end
  end

  describe "scopes" do
    it ".for_member returns only performances of a specific member" do
      member1 = create :member
      member2 = create :member
      perf1 = create :performance, member: member1
      perf2 = create :performance, member: member2
      expect(Performance.for_member(member1)).to eq [perf1]
    end

    it ".disabled returns only disabled performances" do
      perf1 = create :performance, disabled: false
      perf2 = create :performance, disabled: true
      expect(Performance.disabled).to eq [perf2]
    end

    it ".enabled returns only enabled performances" do
      perf1 = create :performance, disabled: false
      perf2 = create :performance, disabled: true
      expect(Performance.enabled).to eq [perf1]
    end
  end
end
