#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

describe Event do
  describe "associations" do
    it {is_expected.to belong_to(:event_type) }
    it {is_expected.to have_many(:attached_files) }
    it {is_expected.to have_and_belong_to_many(:activities) }
  end

  describe "validations" do
    it "default factory is valid" do
      expect(build(:event)).to be_valid
    end
    it { is_expected.to validate_presence_of(:title) }
    it { is_expected.to validate_presence_of(:description) }
    it "end date is after begin date" do
      event = create :event
      event.begin_at = Time.now
      event.end_at = 2.hours.from_now
      expect(event).to be_valid
      event.end_at = 2.hours.ago
      event.valid?
      expect(event.errors[:end_at].size).to eq(1)
    end
  end

  describe "scopes" do
    it "returns events by descending begin date" do
      e1 = create :event, begin_at: Time.now
      e2 = create :event, begin_at: Time.now + 2.hours
      expect(Event.reverse_chronology).to eq [e2, e1]
    end

    it "returns events by ascending begin date" do
      e1 = create :event, begin_at: Time.now + 2.hours
      e2 = create :event, begin_at: Time.now
      expect(Event.chronology).to eq [e2, e1]
    end

    it ".visible returns only events not marked as trashed" do
      e1 = create :event, goto_trash: true
      e2 = create :event, goto_trash: false
      expect(Event.visible).to eq [e2]
    end

    it ".not_in_ga returns only events not synced yet to GCal" do
      e1 = create :event, ga_evt_id: 42
      e2 = create :event, ga_evt_id: nil
      expect(Event.not_in_ga).to eq [e2]
    end

    it ".with_dates returns only events with begin_at and end_at dates" do
      e1 = create :event, begin_at: Time.now, end_at: Time.now + 2.hours
      e2 = create :event, begin_at: nil, end_at: Time.now + 2.hours
      e2 = create :event, begin_at: Time.now + 2.hours, end_at: nil
      expect(Event.with_dates).to eq [e1]
    end

    it ".passed only returns passed events" do
      e1 = create :event, end_at: 2.hours.ago
      e2 = create :event, end_at: Time.now + 2.hours
      expect(Event.passed).to eq [e1]
    end

    it ".to_come only returns events which have not begun yet" do
      e1 = create :event, begin_at: 2.hours.ago
      e2 = create :event, begin_at: Time.now + 2.hours
      expect(Event.to_come).to eq [e2]
    end

    it ".current only returns events that have begun but not finished yet" do
      e1 = create :event, begin_at: 2.hours.ago, end_at: Time.now + 2.hours
      e2 = create :event, begin_at: 2.hours.ago, end_at: 1.hour.ago
      e3 = create :event, begin_at: Time.now + 1.hour, end_at: Time.now + 2.hours
      expect(Event.current).to eq [e1]
    end

    it ".unscheduled only returns events without begin date or end date" do
      e1 = create :event, begin_at: nil, end_at: nil
      e2 = create :event, begin_at: 2.hours.ago, end_at: nil
      e3 = create :event, begin_at: nil, end_at: Time.now + 2.hours
      e4 = create :event, begin_at: 2.hours.ago, end_at: Time.now + 2.hours
      expect(Event.unscheduled).not_to include(e4)
    end

    it ".for_list returns events wich begins or ends within a specified month" do
      # Given
      e1 = create :event, begin_at: "30/08/2017 09:00 +0000", end_at: "31/08/2017 23:59 +0000"
      e2 = create :event, begin_at: "31/08/2017 10:00 +0000", end_at: "01/09/2017 00:01 +0000"
      e3 = create :event, begin_at: "12/09/2017 14:00 +0000", end_at: "12/09/2017 16:00 +0000"
      e4 = create :event, begin_at: "30/09/2017 23:59 +0000", end_at: "01/10/2017 00:01 +0000"
      e5 = create :event, begin_at: "01/10/2017 00:01 +0000", end_at: "01/10/2017 00:02 +0000"
      # When
      result = Event.for_list(9,2017).to_a
      #Then
      expect(result).to eq [e2, e3, e4]
    end

    it ".for_calendar returns events wich begins or ends within a specified month" do
      # Given
      e1 = create :event, begin_at: "23/08/2017 09:00 +0000", end_at: "23/08/2017 23:59 +0000"
      e2 = create :event, begin_at: "24/08/2017 00:00 +0000", end_at: "24/08/2017 00:01 +0000"
      e3 = create :event, begin_at: "12/09/2017 14:00 +0000", end_at: "12/09/2017 16:00 +0000"
      e4 = create :event, begin_at: "08/10/2017 23:59 +0000", end_at: "09/10/2017 00:01 +0000"
      e5 = create :event, begin_at: "09/10/2017 00:00 +0000", end_at: "09/10/2017 00:01 +0000"
      # When
      result = Event.for_calendar(9,2017).to_a
      #Then
      expect(result).to eq [e2, e3, e4]
    end

  end

  describe "behaviors" do
    it "#remaining_days returns the number of days before an event begins" do
      e = create :event, begin_at: Time.now + 5.days
      expect(e.remaining_days).to eq(5)
      e.begin_at = Time.now
      expect(e.remaining_days).to be_nil
      e.begin_at = Time.now - 2.days
      expect(e.remaining_days).to be_nil
    end

    it "#flag_as_destroyable does not destroy the event immediately" do
      e = create :event
      expect(e).to receive(:destroy_ga_event).once
      expect(e.flag_as_destroyable).to be_truthy
      expect(e.reload).not_to be_nil
      expect(e.goto_trash).to be_truthy
    end

    describe '.color' do

      let(:color) { '#FF00FF' }
      subject { create(:event, event_type: create(:event_type, color: color)) }

      it 'returns the event type\'s color' do
        # When
        result = subject.color
        # Then
        expect(result).to eq color
      end
    end

    describe '.ga_event' do

      let(:ga_evt_id) { 'evtId' }
      let(:ga_cal_id) { 'calId@google.com' }
      subject { build(:event, ga_evt_id: ga_evt_id, ga_cal_id: ga_cal_id) }

      it 'returns nil when no event_type associated' do
        # When
        result = subject.ga_event
        # Then
        expect(result).to be_nil
      end

      it 'returns the found Google event when event type associated' do
        # Given
        subject.event_type = create(:event_type)

        ga_evt = double(GoogleAgenda::Event)
        expect(GoogleAgenda::Event).to receive(:find_by_id_or_create).with(ga_evt_id, ga_cal_id).and_return(ga_evt)
        # When
        result = subject.ga_event
        # Then
        expect(result).to be ga_evt
      end

    end

    describe '.save_ga_event' do

      let(:ga_evt_id) { 'evtId' }
      let(:ga_cal_id) { 'calId@google.com' }
      subject { create(:event, event_type: create(:event_type)) }

      it 'saves the newly created Google event IDs' do
        # Given
        ga_evt = double(GoogleAgenda::Event)
        expect(ga_evt).to receive(:update).with(subject).and_return([ga_evt_id, ga_cal_id])
        expect(GoogleAgenda::Event).to receive(:find_by_id_or_create).with(nil, nil).and_return(ga_evt)
        # When
        result = subject.save_ga_event_without_delay
        # Then
        expect(result).to be_truthy
        subject.reload
        expect(subject.ga_evt_id).to eq ga_evt_id
        expect(subject.ga_cal_id).to eq ga_cal_id
      end

    end

    describe '.destroy_ga_event' do

      subject { create(:event, event_type: create(:event_type)) }

      it 'destroys the found Google event' do
        # Given
        expected = double('GoogleClientResult')
        ga_evt = double(GoogleAgenda::Event)
        expect(ga_evt).to receive(:destroy).and_return(expected)
        expect(GoogleAgenda::Event).to receive(:find_by_id_or_create).with(nil, nil).and_return(ga_evt)
        # When
        result = subject.destroy_ga_event_without_delay
        # Then
        expect(result).to be expected
      end

    end

    describe '.reset_ga_event' do

      let(:ga_evt_id) { 'evtId' }
      let(:ga_cal_id) { 'calId@google.com' }
      subject { create(:event, ga_evt_id: ga_evt_id, ga_cal_id: ga_cal_id) }

      it 'returns nil when no event_type associated' do
        # When
        result = subject.reset_ga_event
        # Then
        expect(subject.ga_evt_id).to be_nil
        expect(subject.ga_cal_id).to be_nil
      end

    end
  end

  describe "geocoding" do
    it "geocodes only when location changed" do
      event = create(:event, location: "Bordeaux, France")
      event.location = "Mairie, Arcachon, France"
      expect(event).to receive(:geocode)
      event.save!
    end

    it "does not geocode when location unchanged" do
      event = create(:event, location: "Bordeaux, France")
      event.title = "Titre"
      expect(event).not_to receive(:geocode)
      event.save!
    end
  end
end
