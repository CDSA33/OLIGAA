#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

describe Menu do
  context "validations" do
    specify "default factory is valid" do
      expect(build(:menu)).to be_valid
    end
    it { is_expected.to validate_presence_of :title }
    it { is_expected.to validate_uniqueness_of :title }
    it { is_expected.to validate_uniqueness_of :uid }
  end

  context "scopes" do
    specify "ordered by ASC position by default" do
      menu1 = create :menu
      menu2 = create :menu
      menu2.move_higher
      expect(Menu.all).to eq([menu2, menu1])
    end
  end

  context "behaviors" do
    context "associated with at least one published page" do
      before { @page = create :page, displayed: true, menu: create(:menu) }

      specify "#should_be_displayed? returns TRUE" do
        expect(@page.menu.should_be_displayed?).to be_truthy
      end

      specify "#public_pages returns only published ones"do
        page2 = create :page, menu: @page.menu
        expect(@page.menu.public_pages).to eq [@page]
      end
    end

    context "only associated with privates pages" do
      before { @page = create :page, displayed: false, menu: create(:menu) }

      specify "#should_be_displayed? returns FALSE" do
        expect(@page.menu.should_be_displayed?).not_to be_truthy
      end
    end
    context "regarding footer" do
      before do
        @menu1 = create :menu
        @footer = create :menu, title: "Pied de page", uid: "footer"
        @menu2 = create :menu
      end

      specify ".footer returns the menu 'Pied de page'" do
        expect(Menu.footer).to eq(@footer)
      end

      specify ".sanitized returns menus except 'Pied de page'" do
        expect(Menu.sanitized).not_to include(@footer)
      end
    end
    specify "#destroyable? returns if a menu is destroyable or not" do
      menu = create :menu
      expect(menu.destroyable?).to be_truthy
      menu.update_attribute(:uid, "my_uid")
      expect(menu.destroyable?).to be_falsey
    end
  end
end
