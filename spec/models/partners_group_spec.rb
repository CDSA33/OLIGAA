#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

describe PartnersGroup do
  context "associations" do
    it { is_expected.to have_many(:partners) }
  end

  context "validations" do
    it "default factory is valid" do
      expect(build(:partners_group)).to be_valid
    end
    it { is_expected.to validate_presence_of(:title)}
  end

  context "behaviors" do
    it "can be positionned" do
      pg1 = create :partners_group
      pg2 = create :partners_group
      expect(pg1.position).to eq(1)
      expect(pg2.position).to eq(2)
      pg2.move_higher
      expect(pg1.reload.position).to eq(2)
      expect(pg2.position).to eq(1)  
      pg2.move_lower
      expect(pg1.reload.position).to eq(1)
      expect(pg2.position).to eq(2)        
    end
  end

  context "scopes" do
    it "ordered by ASC position by default" do
      pg1 = create :partners_group
      pg2 = create :partners_group
      pg2.move_higher
      expect(PartnersGroup.all).to eq([pg2, pg1])
    end
  end  
end