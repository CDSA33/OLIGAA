#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

feature "Manage performances" do
  context "as a collaborating club" do
    before { login_as_collaborating_club }

    let(:athle) { create :activity, title: "Athlétisme" }
    let(:swim) { create :activity, title: "Natation" }

    scenario "I can create one" do
      @member.activities << athle
      visit member_path(@member)
      within "aside#performances" do
        click_link "Nouvelle séance"
      end

      expect(current_path).to eq new_member_performance_path(@member)
      within "form" do
        select "mardi", from: "Jour de la semaine"
        fill_in "Heure de début", with: "11:15"
        fill_in "Heure de fin", with: "13:45"
        select "Athlétisme", from: "Activité"
        click_create_button
      end

      expect(current_path).to eq member_path(@member)
      expect(page).to have_content "La séance a bien été créée."
      within "aside#performances ul#enabled" do
        expect(page).to have_selector("li", text: "[Athlétisme] Mardi de 11h15 à 13h45")
      end
    end

    scenario "I can edit one" do
      @member.activities << [athle, swim]
      perf = create :performance, day: :monday, start_at: "18:00", end_at: "20:00", activity: athle, member: @member

      visit member_path(@member)
      within "aside#performances li", text: "[Athlétisme] Lundi de 18h00 à 20h00" do
        click_edit_link
      end

      expect(current_path).to eq edit_member_performance_path(@member, perf)
      within "form" do
        select "mardi", from: "Jour de la semaine"
        fill_in "Heure de début", with: "10:00"
        fill_in "Heure de fin", with: "12:00"
        select "Natation", from: "Activité"
        click_update_button
      end

      expect(current_path).to eq member_path(@member)
      expect(page).to have_content "Séance mise à jour avec succès."
      within "aside#performances ul#enabled" do
        expect(page).to have_selector("li", text: "[Natation] Mardi de 10h00 à 12h00")
      end
    end

    scenario "I can delete one" do
      @member.activities << [athle, swim]
      perf = create :performance, day: :monday, start_at: "18:00", end_at: "20:00", activity: athle, member: @member

      visit member_path(@member)
      within "aside#performances li", text: "[Athlétisme] Lundi de 18h00 à 20h00" do
        click_delete_link
      end

      expect(current_path).to eq member_path(@member)
      expect(page).to have_content "Séance supprimée avec succès."
      expect(page).not_to have_selector("aside#performances ul#enabled")
      expect(page).not_to have_selector("aside#performances ul#disabled")
    end

    scenario "I can disable one and reenable it", :js do
      @member.activities << [athle, swim]
      perf = create :performance, day: :monday, start_at: "18:00", end_at: "20:00", activity: athle, member: @member
      perf = create :performance, day: :monday, start_at: "18:00", end_at: "20:00", activity: swim, member: @member

      visit member_path(@member)
      expect(page).not_to have_selector("aside#performances ul#disabled")

      within "aside#performances ul#enabled li", text: "[Natation] Lundi" do
        click_link "Désactiver"
      end
      expect(current_path).to eq member_path(@member)
      expect(page).to have_content "Séance désactivée avec succès."
      within "aside#performances ul#enabled" do
        expect(page).to have_selector("li", text: "[Athlétisme] Lundi")
      end

      click_link "Séances inactives"
      within "aside#performances ul#disabled li", text: "[Natation] Lundi" do
        click_link "Activer"
      end
      expect(current_path).to eq member_path(@member)
      expect(page).to have_content "Séance activée avec succès."
      expect(page).not_to have_selector("aside#performances ul#disabled")
      within "aside#performances ul#enabled" do
        expect(page).to have_selector("li", text: "[Athlétisme] Lundi")
        expect(page).to have_selector("li", text: "[Natation] Lundi")
      end
    end
  end

  context "as a member" do
    before { login_as_member }

    scenario "I cannot create any" do
      visit member_path(@member)
      expect(page).not_to have_link "Nouvelle séance"

      visit new_member_performance_path(@member)
      expect(page).to have_content "Vous nêtes pas autorisé à accéder à cette zone"
    end
  end
end
