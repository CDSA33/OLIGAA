#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

feature "Manage newsletters" do
  context "as an admin" do
    before { login_as_admin }

    context "regarding creation" do
      scenario "I can create a newsletter" do
        newsletter = build :newsletter
        visit root_path
        within "nav#admin_links" do
          click_link "Newsletters"
        end

        expect(current_path).to eq(newsletters_path)
        within "section#content" do
          click_link "Nouvelle newsletter"
        end

        expect(current_path).to eq(new_newsletter_path)
        within "form" do
          fill_in "Intitulé", with: newsletter.title
          click_create_button
        end

        expect(current_path).to eq(newsletter_path(Newsletter.first))
        within "section#content" do
          expect(page).to have_selector("h1", text: "#{@settings.newsletters_title} #{newsletter.title}")
        end
      end

      scenario "the partners are pre-set" do
        title = "Lettre d'information"
        p1 = create :partner, website: @settings
        p2 = create :partner
        p3 = create :partner, website: @settings
        visit new_newsletter_path
        within "form" do
          within "div#partners" do
            expect(page).to have_checked_field(p1.name)
            expect(page).to have_no_checked_field(p2.name)
            expect(page).to have_checked_field(p3.name)
          end
        end
      end
    end

    scenario "I can destroy one" do
      newsletter = create :newsletter
      visit newsletters_path
      within "section#content" do
        within "ul#newsletters_listing", text: newsletter.title do
          click_delete_link
        end
      end

      expect(current_path).to eq(newsletters_path)
      within "section#content" do
        within "ul#newsletters_listing" do
          expect(page).not_to have_content newsletter.title
        end
      end
    end

    context "regarding edition" do
      scenario "I can edit one" do
        newsletter = create :newsletter
        new_title = "Nouveau titre"
        visit newsletters_path
        within "section#content" do
          within "ul#newsletters_listing", text: newsletter.title do
            click_edit_link
          end
        end

        expect(current_path).to eq(edit_newsletter_path(newsletter))
        within "form" do
          fill_in "Intitulé", with: new_title
          click_update_button
        end

        expect(current_path).to eq(newsletter_path(newsletter))
        within "section#content" do
          expect(page).to have_selector("h1", text: "#{@settings.newsletters_title} #{new_title}")
        end
      end

      scenario "I can publish one" do
        newsletter = create :newsletter
        visit edit_newsletter_path newsletter
        expect do
          within "form" do
            check "Publiée"
            click_update_button
          end
        end.to change(Delayed::Job, :count).by 1

        logout
        visit newsletter_path newsletter
        expect(current_path).to eq(newsletter_path(newsletter))
        within "section#content" do
          expect(page).to have_selector("h1", text: newsletter.title)
        end
      end

      context "regarding items" do
        before { @newsletter = create :newsletter }

        scenario "I can create one", :js do
          item = build :newsletter_item
          visit edit_newsletter_path @newsletter
          within "form" do
            click_link "Informations"
            click_link "Ajouter un item"
            within "div.edit_newsletter_item" do
              fill_in "Intitulé", with: item.title
              fill_in "Contenu", with: item.content
            end
            click_update_button
          end

          expect(current_path).to eq(newsletter_path(@newsletter))
          within "section#content" do
            expect(page).to have_content item.title
            expect(page).to have_content item.content
          end
        end

        scenario "I can delete one", :js do
          item = create :newsletter_item
          @newsletter.items << item
          visit edit_newsletter_path @newsletter
          within "form" do
            click_link "Informations"
            click_link "Supprimer l'item"
            click_update_button
          end

          expect(current_path).to eq(newsletter_path(@newsletter))
          within "section#content" do
            expect(page).not_to have_content item.title
            expect(page).not_to have_content item.content
          end
        end

        context "regarding images", :js do
          scenario "I can attach at least 2 images" do
            item = create :newsletter_item
            image1 = build :image, title: "Image 1"
            image2 = build :image, title: "Image 2"
            @newsletter.items << item
            visit edit_newsletter_path @newsletter
            within "form" do
              click_link "Informations"
              click_link "Images"
              click_link "Ajouter une image"
              first_image_form = all("div.edit_image")[0]
              first_image_form.fill_in("Titre", with: image1.title)
              first_image_form.attach_file("Fichier", sample_image_path)
              click_link "Ajouter une image"
              second_image_form = all("div.edit_image")[1]
              second_image_form.fill_in("Titre", with: image2.title)
              second_image_form.attach_file("Fichier", sample_image_path)
              click_update_button
            end

            expect(current_path).to eq(newsletter_path(@newsletter))
            within "section#content li", text: item.title do
              within "div.item_aside" do
                within "ul.gallery" do
                  expect(first('img')[:alt]).to eq(image1.title)
                  expect(page).to have_css('img', visible: false, count: 2)
                end
                within "span.images-number" do
                  expect(page).to have_content "2"
                end
              end
            end
          end

          scenario "I can remove an image" do
            item = create :newsletter_item, newsletter: @newsletter
            image = create :image, imageable: item
            visit edit_newsletter_path @newsletter
            within "form" do
              click_link "Informations"
              click_link "Images"
              click_link "Supprimer l'image"
              click_update_button
            end

            expect(current_path).to eq(newsletter_path(@newsletter))
            within "section#content li", text: item.title do
              expect(page).not_to have_selector "img"
              expect(page).not_to have_selector "span.images-number"
            end
          end
        end


        context "regarding attached_files" do
          scenario "I can attach a file", :js do
            item = create :newsletter_item
            @newsletter.items << item
            attached_file = build :attached_file
            visit edit_newsletter_path @newsletter
            within "form" do
              click_link "Informations"
              click_link "Pièces jointes"
              click_link "Ajouter une pièce jointe"
              fill_in "Nom", with: attached_file.name
              attach_file "Fichier", sample_text_path
              click_update_button
            end

            expect(current_path).to eq(newsletter_path(@newsletter))
            within "section#content li", text: item.title do
              expect(page).to have_selector("a", text: attached_file.name)
            end
          end

          scenario "I can remove a file", :js do
            item = create :newsletter_item, newsletter: @newsletter
            attached_file = create :attached_file, attachable: item
            visit edit_newsletter_path @newsletter
            within "form" do
              click_link "Informations"
              click_link "Pièces jointes"
              click_link "Supprimer la pièce jointe"
              click_update_button
            end

            expect(current_path).to eq(newsletter_path(@newsletter))
            within "section#content li", text: item.title do
              expect(page).not_to have_selector("a", text: attached_file.name)
            end
          end
        end

        scenario "I can dis-associate activities to any" do
          item = create :newsletter_item
          act1 = create :activity
          act2 = create :activity
          item.activities << act1
          @newsletter.items << item
          visit edit_newsletter_path @newsletter
          within "form" do
            within "div.edit_newsletter_item", text: item.title do
              click_link "Activités"
              within "span.checkbox", text: act1.title do
                expect(find("input[type=checkbox]")).to be_checked
              end
              within "span.checkbox", text: act2.title do
                expect(find("input[type=checkbox]")).not_to be_checked
              end
              check act2.title
              uncheck act1.title
            end
            click_update_button
          end

          expect(current_path).to eq(newsletter_path(@newsletter))
          within "section#content" do
            expect(page).not_to have_content act1.title
            expect(page).to have_content act2.title
          end
        end

        context "regarding position" do
          before do
            @item1 = create :newsletter_item, newsletter: @newsletter
            @item2 = create :newsletter_item, newsletter: @newsletter
          end

          scenario "I can move one up" do
            visit newsletter_path @newsletter
            within "section#content" do
              within "ul#newsletter_items" do
                within "div.item_desc", text: @item2.title do
                  expect(page).not_to have_link "Descendre"
                  click_move_up_link
                end
              end
            end

            expect(current_path).to eq(newsletter_path(@newsletter.reload))
            within "ul#newsletter_items" do
              within "div.item_desc", text: @item2.title do
                expect(page).to have_link "Descendre"
                expect(page).not_to have_link "Monter"
              end
            end
          end

          scenario "I can move one down" do
            visit newsletter_path @newsletter
            within "section#content" do
              within "ul#newsletter_items" do
                within "div.item_desc", text: @item1.title do
                  expect(page).not_to have_link "Monter"
                  click_move_down_link
                end
              end
            end

            expect(current_path).to eq(newsletter_path(@newsletter.reload))
            within "ul#newsletter_items" do
              within "div.item_desc", text: @item1.title do
                expect(page).to have_link "Monter"
                expect(page).not_to have_link "Descendre"
              end
            end
          end
        end
      end

      scenario "I can (dis-)associate partners" do
        p1 = create :partner
        p2 = create :partner
        news = create :newsletter, partners: [p1]
        visit edit_newsletter_path(news)
        within "form" do
          within "div#partners" do
            expect(page).to have_checked_field(p1.name)
            expect(page).to have_no_checked_field(p2.name)
            uncheck p1.name
            check p2.name
          end
          click_update_button
        end

        expect(current_path).to eq(newsletter_path(news))
        within "ul.partners" do
          expect(page).not_to have_content p1.name
          expect(page).to have_content p2.name
        end
      end
    end
  end

  context "as a guest" do
    before do
      @newsletter_title = "LETTRES INFORMATION"
      create :menu, title: @newsletter_title, uid: "newsletters"
    end

    context "wihtout any newsletter" do
      scenario "the menu does not appear" do
        visit root_path

        expect(page).not_to have_selector("li.menu_item", text: @newsletter_title)
      end
    end

    context "regarding a private newsletter" do
      let(:newsletter) { create :newsletter, published: false }

      scenario "the menu does not appear" do
        visit root_path

        expect(page).not_to have_selector("li.menu_item", text: @newsletter_title)
      end

      scenario "I cannot see it" do
        visit newsletter_path newsletter

        expect(current_path).to eq(root_path)
        expect(page).to have_content "Désolé, vous n'êtes pas autorisé à accéder à cette partie du site."
      end
    end

    context "regarding a published newsletter" do
      scenario "I can see it" do
        newsletter = create :newsletter, published: true
        visit newsletter_path newsletter

        expect(current_path).to eq(newsletter_path(newsletter))
        within "section#content" do
          expect(page).to have_selector("h1", text: newsletter.title)
        end
      end

      scenario "I can see the 3 last ones in the nav bar" do
        n1 = create :newsletter, published: true
        nls = []
        3.times { nls << create(:newsletter, published: true) }
        visit root_path
        within "li.menu_item", text: @newsletter_title do
          expect(page).not_to have_selector("a", text: n1.title)
          nls.each do |nl|
            expect(page).to have_selector("a", text: nl.title)
          end
        end
      end

      scenario "I can access to the list of all the newsletters" do
        n1 = create :newsletter, published: true
        n2 = create :newsletter, published: false
        item = create :newsletter_item
        n1.items << item
        visit root_path
        within "li.menu_item", text: @newsletter_title do
          click_link "Archives"
        end

        expect(current_path).to eq(newsletters_path)
        within "section#content" do
          expect(page).to have_content n1.title
          expect(page).to have_content item.title
          expect(page).not_to have_content n2.title
        end
      end

      scenario "I can see the items of global settings" do
        nl_title = "L'actualité du #{ENV['APP_NAME']}"
        @settings.newsletters_title = nl_title
        motto = "Le Sport Adapté aux personnes en situation de handicap mental ou psychique."
        @settings.motto = motto
        @settings.save!
        partner = create :partner_with_logo
        newsletter = create :newsletter, published: true, partners: [partner]
        visit newsletter_path newsletter
        within "section#content" do
          expect(page).to have_selector("h1", text: "#{nl_title} - #{newsletter.title}")
          within "ul.partners" do
            expect(page).to have_link(partner.name)
            expect(page).to have_selector("img")
          end
        end
      end

      scenario "I can print it" do
        newsletter = create :newsletter, published: true, items: [create(:newsletter_item)]
        allow_any_instance_of(Newsletter).to receive_messages(has_pdf?: true)
        visit newsletter_path newsletter
        within "div#partners" do
          expect(page).to have_link("Version imprimable")
        end
      end

      scenario "I can display images using a lightbox", :js do
        item = create :newsletter_item_with_photo
        newsletter = create :newsletter, published: true, items: [item]
        visit newsletter_path newsletter

        within "li.newsletter_item", text: item.title do
          within "div.item_aside" do
            find(:xpath, "//a[contains(@rel,'prettyPhoto')]").click
          end
        end

        expect(page).to have_selector "div.pp_pic_holder"
      end
    end


  end
end
