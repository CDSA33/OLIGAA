#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

feature "Manage Pages" do
  context "as an admin" do
    before { login_as_admin }

    scenario "I can create a page" do
      my_page = build :page
      visit root_path
      within "nav#admin_links" do
        click_link "Pages"
      end
      within "section#content" do
        click_link "Nouvelle page"
      end

      expect(current_path).to eq(new_page_path)
      within "form#new_page" do
        fill_in "Titre", with: my_page.title
        fill_in "Contenu", with: my_page.content
        click_create_button
      end

      within "section#content" do
        expect(page).to have_selector("h1", text: my_page.title)
        expect(page).to have_content my_page.content
      end
    end

    scenario "I can delete a page", :js do
      my_page = create :page
      visit pages_path
      within "#page_list" do
        within "li", text: 'Non rattachée(s) à un menu' do
          click_link 'Non rattachée(s) à un menu'
          within "li", text: my_page.title do
            click_delete_link
          end
        end
      end

      expect(current_path).to eq(pages_path)
      within "#page_list" do
        expect(page).not_to have_content my_page.title
      end
    end

    context "regarding edition" do
      before { @my_page = create :page }

      scenario "I can publish a page" do
        visit edit_page_path @my_page
        within "form" do
          choose "Oui"
          click_update_button
        end

        logout
        visit page_path @my_page
        within "section#content" do
          expect(page).to have_selector("h1", text: @my_page.title)
        end
      end

      scenario "I can associate it to the footer menu", :js do
        create :menu, title: "Pied de page", uid: "footer"
        visit edit_page_path @my_page
        within "form" do
          select "Pied de page", from: "Ajouter au menu"
          click_update_button
        end
        expect(page).to have_content "Page mise à jour avec succès."

        visit pages_path
        click_link "Pied de page"
        within "li", text: "Pied de page" do
          expect(page).to have_selector("li", text: @my_page.title)
        end
      end

      context "regarding images" do
        scenario "I can attach one", :js do
          image = build :logo
          visit edit_page_path @my_page
          within "form" do
            click_link "Images (0)"
            click_link "Ajouter une image"
            attach_file "Fichier", sample_image_path
            click_update_button
          end

          expect(current_path).to eq(page_path(@my_page))
          expect(page).to have_content "Page mise à jour avec succès."
        end

        scenario "I can remove one", :js do
          image = build :logo, displayed: true
          @my_page.logos << image
          visit page_path @my_page
          within "section#content" do
            expect(page).to have_selector("img")
          end
          visit edit_page_path @my_page
          within "form" do
            click_link "Images"
            click_link "Supprimer l'image"
            click_update_button
          end

          expect(current_path).to eq(page_path(@my_page))
          within "section#content" do
            expect(page).not_to have_selector("img")
          end
        end
      end

      context "regarding attached_files" do
        scenario "I can attach a file", :js do
          attached_file = build :attached_file
          visit edit_page_path @my_page
          within "form" do
            click_link "Pièces jointes"
            click_link "Ajouter une pièce jointe"
            fill_in "Nom", with: attached_file.name
            attach_file "Fichier", sample_text_path
            click_update_button
          end

          expect(current_path).to eq(page_path(@my_page))
          within "section#content dl.attachments" do
            expect(page).to have_selector("a", text: attached_file.name)
          end
        end

        scenario "I can remove a file", :js do
          attached_file = create :attached_file, attachable: @my_page
          visit edit_page_path @my_page
          within "form" do
            click_link "Pièces jointes"
            click_link "Supprimer la pièce jointe"
            click_update_button
          end

          expect(current_path).to eq(page_path(@my_page))
          expect(page).not_to have_selector("dl.attachments")
        end
      end

      scenario "I can integrate a video" do
        @my_page.content = '<iframe width="420" height="315" src="//www.youtube.com/embed/o3IYXxouhTU" frameborder="0" allowfullscreen></iframe>'
        @my_page.save!
        visit page_path @my_page
        within "section#content" do
          expect(page).to have_selector("iframe")
        end
      end

      scenario "I am informed if something went wrong" do
        visit edit_page_path @my_page
        within "form" do
          fill_in "Titre", with: ""
          click_update_button
        end

        expect(current_path).to eq(page_path(@my_page))
        expect(page).to have_content "Au moins une erreur a été rencontrée. Merci de vérifier vos éléments dans les différents onglets."
      end
    end

    context "regarding positionning" do
      before do
        @menu = create :menu
        @page1 = create :page, menu: @menu
        @page2 = create :page, menu: @menu
      end

      scenario "I can move one down within a menu", :js do
        visit pages_path
        within "li", text: @menu.title do
          click_link @menu.title
          within "li", text: @page1.title do
            click_move_down_link
          end
        end

        expect(current_path).to eq(pages_path)
        within "li", text: @menu.title do
          click_link @menu.title
          within "li:first-child" do
            expect(page).to have_content @page2.title
          end
        end
      end

      scenario "I can move one up within a menu", :js do
        visit pages_path
        within "li", text: @menu.title do
          click_link @menu.title
          within "li", text: @page2.title do
            click_move_up_link
          end
        end

        expect(current_path).to eq(pages_path)
        within "li", text: @menu.title do
          click_link @menu.title
          within "li:first-child" do
            expect(page).to have_content @page2.title
          end
        end
      end
    end
  end

  context "as a guest" do
    context "regarding published pages" do
      scenario "I can access one" do
        my_page = create :page, displayed: true, menu: create(:menu)
        visit root_path
        within "nav#main-nav" do
          expect(page).to have_selector("span.menu_title", text: my_page.menu.title)
          click_link my_page.title
        end

        expect(current_path).to eq(page_path(my_page))
        within "section#content" do
          expect(page).to have_selector("h1", text: my_page.title)
        end
      end
    end

    context "regarding private pages" do
      scenario "I cannot acces them" do
        my_page = create :page, menu: create(:menu)
        visit root_path
        within "nav#main-nav" do
          expect(page).not_to have_selector("span.menu_title", text: my_page.menu.title.upcase)
          expect(page).not_to have_content(my_page.title)
        end
      end
    end

    context "regarding missing page" do
      scenario "I am redirected to root path" do
        visit page_path(id: "150-fake")

        expect(current_path).to eq(root_path)
      end
    end

    context "regarding custom footer pages" do
      before do
        footer_menu = create :menu, title: "Pied de page", uid: "footer"
        @page = create :page, menu: footer_menu
      end

      scenario "a private one is not accessible" do
        visit root_path

        within "footer" do
          expect(page).not_to have_link @page.title
        end
      end

      scenario "a published one is accessible" do
        @page.update_attribute(:displayed, true)

        visit root_path

        within "footer" do
          expect(page).to have_link @page.title
        end
      end
    end
  end
end
