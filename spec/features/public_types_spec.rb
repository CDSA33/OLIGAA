#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

feature "Manage public types" do
  context "as an admin" do
    before { login_as_admin }

    scenario "I can create one" do
      type = build :public_type
      visit root_path
      within "nav#admin_links" do
        click_link "Membres"
      end
      click_link "Types de public"
      click_link "Nouveau type de public"

      expect(current_path).to eq(new_public_type_path)
      within "form" do
        fill_in "Intitulé", with: type.title
        fill_in "Description", with: type.description
        click_create_button
      end

      expect(current_path).to eq(public_type_path(PublicType.first))
      within "section#content" do
        expect(page).to have_content "Type de public créé avec succès"
        expect(page).to have_selector("h1", text: type.title)
        expect(page).to have_content type.description
      end
    end

    scenario "I can edit one" do
      type = create :public_type
      new_title = "Nouvel intitulé"

      visit public_types_path
      within "ul#public_type_listing > li", text: type.title do
        click_edit_link
      end

      expect(current_path).to eq(edit_public_type_path(type))
      within "form" do
        fill_in "Intitulé", with: new_title
        click_update_button
      end

      expect(current_path).to eq(public_type_path(type.reload))
      within "section#content" do
        expect(page).to have_content "Type de public mis à jour avec succès"
        expect(page).to have_selector("h1", text: new_title)
      end
    end

    scenario "I can delete one" do
      type = create :public_type
      new_title = "Nouvel intitulé"

      visit public_types_path
      within "ul#public_type_listing > li", text: type.title do
        click_delete_link
      end

      expect(current_path).to eq(public_types_path)
      within "section#content" do
        expect(page).to have_content "Type de public supprimé avec succès"
        expect(page).not_to have_content type.title
      end
    end
  end
end