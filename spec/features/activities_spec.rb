#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

feature "Manage Activities" do
  context "as an admin" do
    before { login_as_admin }

    scenario "I can create one" do
      activity = build :activity
      visit root_path
      within "nav#admin_links" do
        click_link "Activités"
      end

      expect(current_path).to eq(activities_path)
      within "section#content" do
        click_link "Nouvelle activité"
      end

      expect(current_path).to eq(new_activity_path)
      within "form" do
        fill_in "Intitulé", with: activity.title
        fill_in "Présentation", with: activity.presentation
        fill_in "Description", with: activity.description
        click_create_button
      end

      expect(current_path).to eq(activity_path(Activity.first))
      within "section#content" do
        expect(page).to have_content "Activité créée avec succès."
        expect(page).to have_selector("h1", text: activity.title)
        expect(page).to have_content activity.presentation
        expect(page).to have_content activity.description
      end
    end

    scenario "I can delete one", :js do
      activity = create :activity
      visit activities_path
      click_link "Sans type de catégorie"
      within "#activity_list ul > li", text: activity.title do
        click_delete_link
      end

      expect(current_path).to eq(activities_path)
      within "section#content" do
        expect(page).to have_content "Activité supprimée avec succès."
        expect(page).not_to have_content activity.title
      end
    end

    context "regarding edition" do
      before { @activity = create :activity }

      scenario "I can change the title", :js do
        new_title = "Nouvel intitulé"
        visit activities_path
        click_link "Sans type de catégorie"
        within "#activity_list ul > li", text: @activity.title do
          click_edit_link
        end

        expect(current_path).to eq(edit_activity_path(@activity))
        within "form" do
          fill_in "Intitulé", with: new_title
          click_update_button
        end

        expect(current_path).to eq(activity_path(@activity))
        within "section#content" do
          expect(page).to have_content "Activité mise à jour avec succès."
          expect(page).to have_selector("h1", text: new_title)
        end
      end

      scenario "I can (dis-)associate categories" do
        create :menu, title: "Activités", uid: "activity_categories"
        cat1 = create :activity_category
        cat1.activities << @activity
        cat2 = create :activity_category

        visit edit_activity_path @activity
        within "form" do
          uncheck cat1.title
          check cat2.title
          click_update_button
        end

        expect(current_path).to eq(activity_path(@activity))
        within "section#content" do
          expect(page).to have_content "Activité mise à jour avec succès."
        end

        within "nav#main-nav" do
          click_link cat1.title
        end
        within "section#content" do
          expect(page).not_to have_selector("h2", text: @activity.title)
        end

        within "nav#main-nav" do
          click_link cat2.title
        end
        within "section#content" do
          expect(page).to have_selector("h2", text: @activity.title)
          expect(page).to have_content @activity.presentation
          expect(page).not_to have_content @activity.description
        end
      end

      scenario "I can remove it from the map filter",:js do
        member_group = create :member_group
        member_group.members << create(:member, activities: [@activity])
        visit member_group_path member_group
        click_link "Trier les clubs"
        within "form#members_filter ul#activities_filter" do
          expect(page).to have_content @activity.title
        end

        visit edit_activity_path @activity
        within "form" do
          uncheck "Utilisable dans le filtre des membres (cf. carte)"
          click_update_button
        end

        expect(current_path).to eq(activity_path(@activity))
        visit member_group_path member_group
        click_link "Trier les clubs"
        within "form#members_filter ul#activities_filter" do
          expect(page).not_to have_content @activity.title
        end
      end

      context "regarding the poster" do
        scenario "I can provide one" do
          visit edit_activity_path @activity
          within "form" do
            attach_file "Image", sample_image_path
            click_update_button
          end

          expect(current_path).to eq(activity_path(@activity))
          within "section#content" do
            expect(page).to have_selector("img.poster_right")
          end
        end

        scenario "I can remove it" do
          @activity.poster = File.open(sample_image_path)
          @activity.save!
          visit activity_path @activity
          within "section#content" do
            expect(page).to have_selector("img.poster_right")
          end

          visit edit_activity_path @activity
          within "form" do
            check "Supprimer l'image"
            click_update_button
          end

          expect(current_path).to eq(activity_path(@activity))
          within "section#content" do
            expect(page).not_to have_selector("img.poster_right")
          end
        end
      end

      context "regarding attached_files" do
        scenario "I can attach a file", :js do
          attached_file = build :attached_file
          visit edit_activity_path @activity
          within "form" do
            click_link "Pièces jointes"
            click_link "Ajouter une pièce jointe"
            fill_in "Nom", with: attached_file.name
            attach_file "Fichier", sample_text_path
            click_update_button
          end

          expect(current_path).to eq(activity_path(@activity))
          within "section#content dl.attachments" do
            expect(page).to have_selector("a", text: attached_file.name)
          end
        end

        scenario "I can remove a file", :js do
          attached_file = create :attached_file, attachable: @activity
          visit edit_activity_path @activity
          within "form" do
            click_link "Pièces jointes"
            click_link "Supprimer la pièce jointe"
            click_button "Mettre à jour"
          end

          expect(current_path).to eq(activity_path(@activity))
          expect(page).not_to have_selector("dl.attachments")
        end
      end

      scenario "I can remove it from the activity category show page" do
        cat = create :activity_category
        @activity.activity_categories << cat
        visit activity_category_path cat
        within "section#content" do
          expect(page).to have_selector("h2", text: @activity.title)
        end

        visit edit_activity_path @activity
        within "form" do
          uncheck "Affichée dans le listing"
          click_update_button
        end

        expect(current_path).to eq(activity_path(@activity))
        within "section#content" do
          expect(page).to have_content "Activité mise à jour avec succès."
        end

        visit activity_category_path cat
        within "section#content" do
          expect(page).not_to have_selector("h2", text: @activity.title)
        end
      end
    end
  end

  context "as a guest" do
    scenario "I can see one associated to a category" do
      create :menu, title: "Activités", uid: "activity_categories"
      cat = create :activity_category
      activity = create :activity
      cat.activities << activity
      visit root_path
      within "nav#main-nav" do
        click_link cat.title
      end
      within "section#content" do
        expect(page).to have_selector("h2", text: activity.title)
        expect(page).to have_content activity.presentation
        expect(page).not_to have_content activity.description
        click_link "En savoir plus"
      end
      within "section#content" do
        expect(page).to have_selector("h1", text: activity.title)
        expect(page).to have_content activity.presentation
        expect(page).to have_content activity.description
      end
    end

    scenario "I can access to clubs where I can practice it" do
      athle = create :activity, title: "athle"
      foot = create :activity, title: "foot"
      mg1 = create :member_group
      mg2 = create :member_group
      @settings.map_member_groups << [mg1, mg2]
      m1 = create :member, published: true, member_groups: [mg1], activities: [athle]
      m2 = create :member, published: true, member_groups: [mg1], activities: [foot]
      visit activity_path(athle)
      within "div#where2practice" do
        expect(page).not_to have_link mg2.title
        click_link mg1.title
      end

      expect(current_path).to eq(filter_members_member_group_path(mg1))
      within "ul#published_members" do
        expect(page).to have_link m1.name
        expect(page).not_to have_link m2.name
      end
    end
  end
end
