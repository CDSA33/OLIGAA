#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'


feature "Manage menus" do
  context "as an admin" do
    before { login_as_admin }

    scenario "I can create a menu" do
      menu = build :menu
      visit root_path
      within "nav#admin_links" do
        click_link "Pages"
      end

      expect(current_path).to eq(pages_path)
      click_link "Nouveau menu"

      expect(current_path).to eq(new_menu_path)
      within "form#new_menu" do
        fill_in "Intitulé", with: menu.title
        click_create_button
      end

      expect(current_path).to eq(pages_path)
      within "#page_list" do
        expect(page).to have_content menu.title
      end
    end

    scenario "I can edit a menu", :js do
      menu = create :menu
      new_title = "Menu modifié"
      visit pages_path
      within "#page_list" do
        within "li", text: menu.title do
          click_link menu.title
          within "p.AdminCard-headingActions" do
            click_edit_link
          end
        end
      end

      expect(current_path).to eq(edit_menu_path(menu))
      within "form#edit_menu_#{menu.id}" do
        fill_in "Intitulé", with: new_title
        click_update_button
      end

      expect(current_path).to eq(pages_path)
      within "#page_list" do
        expect(page).to have_content new_title
      end
    end

    scenario "I can delete a menu", :js do
      menu = create :menu
      visit pages_path
      within "#page_list" do
        within 'li', text: menu.title do
          click_link menu.title
          within "p.AdminCard-headingActions" do
            click_delete_link
          end
        end
      end

      expect(current_path).to eq(pages_path)
      within "#page_list" do
        expect(page).not_to have_content menu.title
      end
    end

    context "regarding positionning" do

      before do
        @menu1 = create :menu
        @menu2 = create :menu
      end

      scenario "I can move a menu up", :js do
        visit pages_path
        within "li", text: @menu2.title do
          click_link @menu2.title
          within "p.AdminCard-headingActions" do
            click_move_up_link
          end
        end

        expect(first('#page_list li')).to have_content @menu2.title
      end

      scenario "I can move a menu down", :js do
        visit pages_path
        within "li", text: @menu1.title do
          click_link @menu1.title
          within "p.AdminCard-headingActions" do
            click_move_down_link
          end
        end

        expect(first('#page_list li')).to have_content @menu2.title
      end
    end

    context "regarding fixed menus", :js do
      scenario "I cannot delete one" do
        menu = create :menu, uid: "my_uid"
        visit pages_path
        within "#page_list" do
          within 'li', text: menu.title do
            click_link menu.title
            within "p.AdminCard-headingActions" do
              expect(page).not_to have_link "Supprimer"
            end
          end
        end
      end
    end
  end
end
