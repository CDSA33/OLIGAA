#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

feature "Manage Partners Groups" do
  context "as an admin" do
    before { login_as_admin }

    scenario "I can create one" do
      pg = build :partners_group
      visit partners_path
      within "section#content" do
        click_link "Nouveau groupe"
      end

      expect(current_path).to eq(new_partners_group_path)
      within "form" do
        fill_in "Nom", with: pg.title
        click_create_button
      end

      expect(current_path).to eq(partners_path)
      within "section#content" do
        expect(page).to have_content "Groupe de partenaires créé avec succès."
        expect(page).to have_selector("li", text: pg.title)
      end
    end

    scenario "I can edit one", :js do
      pg = create :partners_group
      new_name = "Nouveau Nom"
      visit partners_path
      within "#partner_list" do
        within "li", text: pg.title do
          click_link pg.title
          within "p.AdminCard-headingActions" do
            click_edit_link
          end
        end
      end

      expect(current_path).to eq(edit_partners_group_path(pg))
      within "form" do
        fill_in "Nom", with: new_name
        click_update_button
      end

      expect(current_path).to eq(partners_path)
      within "section#content" do
        expect(page).to have_content "Groupe de partenaires mis à jour avec succès."
        expect(page).to have_selector("li", text: new_name)
        expect(page).not_to have_content pg.title
      end
    end

    scenario "I can delete one", :js do
      pg = create :partners_group
      visit partners_path
      within "#partner_list" do
        within "li", text: pg.title do
          click_link pg.title
          within "p.AdminCard-headingActions" do
            click_delete_link
          end
        end
      end

      expect(current_path).to eq(partners_path)
      within "section#content" do
        expect(page).to have_content "Groupe de partenaires supprimé avec succès."
        expect(page).not_to have_content pg.title
      end
    end

      context "regarding positionning" do

        before do
          @pg1 = create :partners_group
          @pg2 = create :partners_group
        end

        scenario "I can move a partners group up", :js do
          visit partners_path
          within '#partner_list' do
            within "li", text: @pg2.title do
              click_link @pg2.title
              within "p.AdminCard-headingActions" do
                click_move_up_link
              end
            end
          end

          expect(first('#partner_list > li')).to have_content @pg2.title
        end

        scenario "I can move a partners group down", :js do
          visit partners_path
          within '#partner_list' do
            within "li", text: @pg1.title do
              click_link @pg1.title
              within "p.AdminCard-headingActions" do
                click_move_down_link
              end
            end
          end

          expect(first('#partner_list > li')).to have_content @pg2.title
        end
      end
  end
end