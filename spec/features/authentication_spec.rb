#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'


feature "Authentication" do

  before do
    create :menu, title: "ZONE MEMBRE", uid: "member_zone"
  end

  let(:new_pass) { 'secret' }

  def change_password
    within "ul#tabs-links" do
      click_link "Mot de passe"
    end
    within "div#change-password" do
      fill_in :member_password, with: new_pass
      fill_in :member_password_confirmation, with: new_pass
    end
    click_button "Mettre à jour"
  end

  context "as a guest" do
    scenario "I can login as a member" do
      member = create :member
      visit root_path
      within "nav#main-nav" do
        click_link "Se connecter"
      end

      expect(current_path).to eq(login_path)
      within "form#sign_in" do
        fill_in :login, with: member.login
        fill_in :password, with: default_password
        click_button "S'identifier"
      end

      expect(current_path).to eq(root_path)
      expect(page).to have_content "Vous êtes maintenant connecté à la zone membre."
    end
  end

  context "as a new member" do
    scenario "new member can't do anything unless he changes his password" do
      new_member = create :member, :new
      new_pass = 'secret'

      login_as(new_member.login, new_member.default_password)
      expect(current_path).to eq(edit_member_path(new_member))
      expect(page).to have_content "Vous devez modifier votre mot de passe avant toute chose !"

      visit root_path
      expect(current_path).to eq(edit_member_path(new_member))
      expect(page).to have_content "Vous devez modifier votre mot de passe avant toute chose !"

      change_password
      visit root_path
      expect(current_path).to eq(root_path)
      expect(page).not_to have_content "Vous devez modifier votre mot de passe avant toute chose !"
    end
  end

  context "as a member" do

    before { login_as_member }

    scenario "I can logout" do
      visit root_path
      within "nav#main-nav" do
        click_link "Se déconnecter"
      end

      expect(current_path).to eq(root_path)
      expect(page).to have_content "Vous êtes maintenant déconnecté de la zone membre."
    end

    scenario "I can change my password" do
      within "nav#main-nav" do
        click_link "Ma fiche"
      end
      within "article" do
        click_edit_link
      end
      change_password

      expect(page).to have_content 'Compte mis à jour avec succès.'
      logout
      login_as(@member.login, new_pass)

      expect(current_path).to eq(root_path)
      expect(page).to have_content "Vous êtes maintenant connecté à la zone membre."
      expect(page).not_to have_content "Vous devez aller sur votre profil pour modifier votre mot de passe!"
    end
  end
end
