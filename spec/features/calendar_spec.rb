#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

feature "Use calendar" do
  context "as a guest" do
    before(:all) { Timecop.freeze("01/07/2013 00:00") }
    after(:all) { Timecop.return }

    before do
      @et = create :event_type
      @event = create :event, begin_at: "02/07/2013 09:00", end_at: "02/07/2013 12:00", event_type: @et
    end

    scenario "I can see an event with dates on the calendar" do
      create :menu, title: "Calendrier", uid: "calendar"
      visit root_path
      within "nav#main-nav" do
        click_link "Calendrier"
      end
      within "section#content" do
        expect(page).to have_content @event.title
      end
    end

    scenario "I cannot see an event with no begin date on the calendar" do
      @event.update_attribute(:begin_at, nil)
      visit calendar_path(year: 2013, month: 7)
      within "section#content" do
        expect(page).not_to have_content @event.title
      end
    end

    scenario "I cannot see an event with no end date on the calendar" do
      @event.update_attribute(:end_at, nil)
      visit calendar_path(year: 2013, month: 7)
      within "section#content" do
        expect(page).not_to have_content @event.title
      end
    end

    scenario "I cannot see an event associated with no event type on the calendar" do
      @event.update_attribute(:event_type, nil)
      visit calendar_path(year: 2013, month: 7)
      within "section#content" do
        expect(page).not_to have_content @event.title
      end
    end

    scenario "I can download a PDF version of each month" do
      allow(PDFKit.configuration).to receive(:wkhtmltopdf).and_return(File.dirname(__FILE__))
      allow_any_instance_of(PDFKit).to receive(:to_pdf).and_return(File.new(sample_text_path))

      visit calendar_path(year: 2013, month: 7)
      click_link "Télécharger le PDF"
      expect(page.response_headers["Content-Type"]).to eq("application/pdf")
    end

    context "regarding calendar navigation" do
      before do
        @et2 = create :event_type
        @event2 = create :event, begin_at: "02/08/2013 09:00", end_at: "02/08/2013 12:00", event_type: @et2
        @event3 = create :event, begin_at: "02/02/2014 09:00", end_at: "02/02/2014 12:00", event_type: @et2
      end

      scenario "I can go to next month", :js do
        visit calendar_path(year: 2013, month: 7)
        set_laptop_size
        within "div.fc-header-toolbar" do
          click_button "août >"
        end

        expect(current_path).to eq(calendar_path(year: 2013, month: 8))
        expect(page).to have_content @event2.title
      end

      scenario "I can go to previous month", :js do
        visit calendar_path(year: 2013, month: 8)
        set_laptop_size
        within "div.fc-header-toolbar" do
          click_on "< juillet"
        end

        expect(current_path).to eq(calendar_path(year: 2013, month: 7))
        expect(page).to have_content @event.title
      end

      scenario "I can select month and year" do
        visit calendar_path(year: 2013, month: 7)
        within "form#event_types_filter_form" do
          select "février"
          select "2014"
          click_button "Rafraîchir"
        end

        expect(current_path).to eq(calendar_path(year: 2014, month: 2))
        expect(page).to have_content @event3.title
      end

      scenario "I can choose which event types to show" do
        visit calendar_path(year: 2013, month: 7)
        within "form#event_types_filter_form" do
          uncheck @et.title
          click_button "Rafraîchir"
        end

        expect(current_path).to eq(calendar_path(year: 2013, month: 7))
        expect(page).not_to have_content @event.title
      end
    end
  end
end
