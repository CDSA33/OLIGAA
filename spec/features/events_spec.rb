#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

feature "Manage events" do
  context "as an admin" do
    before { login_as_admin }

    scenario "I can create one" do
      event = build :event
      visit root_path
      within "nav#admin_links" do
        click_link "Evénements"
      end

      expect(current_path).to eq(events_path)
      within "section#content" do
        click_link "Nouvel événement"
      end

      expect(current_path).to eq(new_event_path)
      within "form" do
        fill_in "Intitulé", with: event.title
        fill_in "Description", with: event.description
        fill_in "Lieu", with: event.location
        fill_in "Tarif", with: event.price
        click_button "Créer"
      end

      expect(current_path).to eq(event_path(Event.first))
      within "section#content" do
        expect(page).to have_content "Evénément créé avec succès."
        expect(page).to have_selector("h1", text: event.title)
        expect(page).to have_content event.description
        expect(page).to have_content event.location
        expect(page).to have_content event.price
      end
    end

    scenario "I can delete one", :js do
      event = create :event
      visit events_path
      within "section#content" do
        click_link "Non planifiés"
        within "h3", text: event.title do
          click_delete_link
        end
      end

      expect(current_path).to eq(events_path)
      within "section#content" do
        expect(page).to have_content "Evénément supprimé avec succès."
        expect(page).not_to have_selector("td", text: event.title)
      end
    end

    context "regarding edition" do
      before { @event = create :event }

      scenario "I can provide begin and end dates", :js do
        visit events_path
        within "section#content" do
          click_link "Non planifiés"
          within "h3", text: @event.title do
            click_link "Éditer"
          end
        end

        expect(current_path).to eq(edit_event_path(@event))
        within "form" do
          fill_in "Date et heure de début", with: "02/07/2013 09:00"
          fill_in "Date et heure de fin", with: "02/07/2013 12:00"
          click_button "Mettre à jour"
        end

        expect(current_path).to eq(event_path(@event.reload))
        within "section#content" do
          expect(page).to have_content "Evénément modifié avec succès."
          expect(page).to have_selector("h1", text: @event.title)
          expect(page).to have_content "mardi 02 juillet 2013 09:00"
          expect(page).to have_content "mardi 02 juillet 2013 12:00"
        end
      end

      scenario "I can (dis-)associate with activities" do
        act1 = create :activity
        act2 = create :activity
        @event.activities << act1
        visit edit_event_path @event
        within "form" do
          within "#event_activities" do
            uncheck act1.title
            check act2.title
          end
          click_button "Mettre à jour"
        end

        expect(current_path).to eq(event_path(@event.reload))
        within "section#content" do
          expect(page).to have_content "Evénément modifié avec succès."
          expect(page).not_to have_selector("li", text: act1.title)
          expect(page).to have_selector("li", text: act2.title)
        end
      end

      scenario "I can associate with an event type" do
        et = create :event_type
        visit edit_event_path @event
        within "form" do
          select et.title
          click_button "Mettre à jour"
        end

        expect(current_path).to eq(event_path(@event.reload))
        within "section#content" do
          expect(page).to have_content "Evénément modifié avec succès."
          expect(page).to have_content et.title
        end
      end

      context "regarding attached_files" do
        scenario "I can attach a file", :js do
          attached_file = build :attached_file
          visit edit_event_path @event
          within "form" do
            click_link "Pièces jointes"
            click_link "Ajouter une pièce jointe"
            fill_in "Nom", with: attached_file.name
            attach_file "Fichier", sample_text_path
            click_button "Mettre à jour"
          end

          expect(current_path).to eq(event_path(@event))
          within "section#content dl.attachments" do
            expect(page).to have_selector("a", text: attached_file.name)
          end
        end

        scenario "I can remove a file", :js do
          attached_file = create :attached_file, attachable: @event
          visit edit_event_path @event
          within "form" do
            click_link "Pièces jointes"
            click_link "Supprimer la pièce jointe"
            click_button "Mettre à jour"
          end

          expect(current_path).to eq(event_path(@event))
          expect(page).not_to have_selector("dl.attachments")
        end
      end
    end
  end
end
