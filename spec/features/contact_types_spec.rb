#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

feature "Manage contact types" do
  context "as an admin" do
    before { login_as_admin }

    scenario "I can create one" do
      type = build :contact_type

      visit root_path
      within "nav#admin_links" do
        click_link "Membres"
      end
      click_link "Types de contact"
      click_link "Nouveau type de contact"

      expect(current_path).to eq(new_contact_type_path)
      within "form" do
        fill_in "Intitulé", with: type.title
        click_create_button
      end

      expect(current_path).to eq(contact_types_path)
      within "section#content" do
        expect(page).to have_content "Type de contact créé avec succès"
        expect(page).to have_content type.title
      end
    end

    scenario "I can edit one" do
      type = create :contact_type
      new_title = "Nouvel intitulé"

      visit contact_types_path
      within "ul#contact_type_listing > li", text: type.title do
        click_edit_link
      end

      expect(current_path).to eq(edit_contact_type_path(type))
      within "form" do
        fill_in "Intitulé", with: new_title
        click_update_button
      end

      expect(current_path).to eq(contact_types_path)
      within "section#content" do
        expect(page).to have_content "Type de contact mis à jour avec succès"
        expect(page).to have_content new_title
      end
    end

    scenario "I can delete one" do
      type = create :contact_type
      new_title = "Nouvel intitulé"

      visit contact_types_path
      within "ul#contact_type_listing > li", text: type.title do
        click_delete_link
      end

      expect(current_path).to eq(contact_types_path)
      within "section#content" do
        expect(page).to have_content "Type de contact supprimé avec succès"
        expect(page).not_to have_content type.title
      end
    end
  end
end