#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

feature "Manage subscriptions" do
  context "as an admin" do
    before do
      Timecop.freeze(Date.new(2017,10,15))
      login_as_admin
    end

    after { Timecop.freeze }

    scenario "I can delete one", :js do
      collaborating_clubs_mg = create :member_group
      asso = create :member, member_groups: [collaborating_clubs_mg]
      @settings.platform_collaborating_clubs << collaborating_clubs_mg
      adherent = build :adherent, last_name: "Dupont", first_name: "Jean"
      perf = create :performance, member: asso
      subscription = create :subscription, adherent: adherent, performances: [perf], member_id: asso.id

      visit member_path(asso)
      within "aside#subscriptions div#internal" do
        click_link "Saison 2017-2018"
        within "li", text: "Jean Dupont" do
          click_delete_link
        end
      end

      expect(current_path).to eq member_path(asso)
      expect(page).to have_content "L'adhésion a bien été supprimée."
      within "aside#subscriptions div#internal" do
        expect(page).not_to have_content "Saison 2017-2018"
      end
    end

    context "regarding edition" do
      scenario "I can edit one", :js do
        collaborating_clubs_mg = create :member_group
        asso = create :member, member_groups: [collaborating_clubs_mg]
        @settings.platform_collaborating_clubs << collaborating_clubs_mg
        adherent = build :adherent, last_name: "Dupont", first_name: "Jean", medico_social_org: nil, other_medicosocial_org: "Une structure"
        perf = create :performance, member: asso
        subscription = create :subscription, adherent: adherent, performances: [perf], member_id: asso.id

        visit member_path(asso)
        within "aside#subscriptions div#internal" do
          click_link "Saison 2017-2018"
          within "li", text: "Jean Dupont" do
            click_edit_link
          end
        end

        expect(current_path).to eq edit_member_subscription_path(asso, subscription)
        within "form" do
          fill_in "Nom", with: "Dupond"
          click_update_button
        end

        expect(current_path).to eq member_path(asso)
        expect(page).to have_content "L'adhésion a bien été mise à jour."
        within "aside#subscriptions div#internal" do
          click_link "Saison 2017-2018"
          expect(page).not_to have_content "Jean Dupont"
          expect(page).to have_content "Jean Dupond"
        end
      end

      scenario "email sent when activity removed", type: :mailer do
        collaborating_clubs_mg = create :member_group
        @settings.platform_collaborating_clubs << collaborating_clubs_mg
        usg = create :member, name: "USG", member_groups: [collaborating_clubs_mg]
        mouettes = create :member, name: "Les Mouettes", member_groups: [collaborating_clubs_mg]
        athle = create :activity, title: "Athlétisme"
        adherent = build :adherent, last_name: "Dupont", first_name: "Jean", medico_social_org: nil, other_medicosocial_org: "Une structure"
        usg_perf = create :performance, member: usg, activity: athle
        mouettes_perf = create :performance, member: mouettes, activity: athle
        subscription = create :subscription, adherent: adherent, performances: [usg_perf, mouettes_perf], member_id: usg.id

        visit edit_member_subscription_path(usg, subscription)
        within "form" do
          expect(all("input[type=checkbox]")[1]).to be_checked
          uncheck "Les Mouettes - [Athlétisme] Lundi"
          click_update_button
        end

        expect(ActionMailer::Base.deliveries.count).to eq 1
      end

      scenario "email sent when activity added", type: :mailer do
        collaborating_clubs_mg = create :member_group
        @settings.platform_collaborating_clubs << collaborating_clubs_mg
        usg = create :member, name: "USG", member_groups: [collaborating_clubs_mg]
        mouettes = create :member, name: "Les Mouettes", member_groups: [collaborating_clubs_mg]
        athle = create :activity, title: "Athlétisme"
        adherent = build :adherent, last_name: "Dupont", first_name: "Jean", medico_social_org: nil, other_medicosocial_org: "Une structure"
        usg_perf = create :performance, member: usg, activity: athle
        mouettes_perf = create :performance, member: mouettes, activity: athle
        subscription = create :subscription, adherent: adherent, performances: [usg_perf], member_id: usg.id

        visit edit_member_subscription_path(usg, subscription)
        within "form" do
          expect(all("input[type=checkbox]")[1]).not_to be_checked
          check "Les Mouettes - [Athlétisme] Lundi"
          click_update_button
        end

        expect(ActionMailer::Base.deliveries.count).to eq 1
      end
    end

    context "regarding listing by collaborating club" do

      scenario "I can list all susbscriptions" do
        collaborating_clubs_mg = create :member_group
        usg = create :member, name: "USG", member_groups: [collaborating_clubs_mg]
        mouettes = create :member, name: "Les Mouettes", member_groups: [collaborating_clubs_mg]
        athle = create :activity, title: "Athlétisme"
        jean_dupont = build :adherent, last_name: "Dupont", first_name: "Jean"
        arthur_latrouille = build :adherent, last_name: "Latrouille", first_name: "Arthur"
        pascal_durand = build :adherent, last_name: "Durand", first_name: "Pascal"
        perf_usg = create :performance, member: usg, activity: athle
        perf_mouettes = create :performance, member: mouettes, activity: athle
        create :subscription, adherent: jean_dupont, performances: [perf_usg], member_id: usg.id
        create :subscription, adherent: arthur_latrouille, performances: [perf_usg], member_id: usg.id
        create :subscription, adherent: pascal_durand, performances: [perf_mouettes], member_id: mouettes.id
        @settings.platform_collaborating_clubs << collaborating_clubs_mg

        visit root_path
        within "nav#admin_links" do
          click_link "Plateforme"
        end
        expect(page).not_to have_selector("div#subscriptions-by-club")

        within "form#season-selector" do
          select "2017-2018", from: "Saison"
          click_button "Afficher"
        end
        expect(current_path).to eq platform_path

        click_link "Adhésions par club"

        expect(page).to have_selector("h2", text: "Liste des adhésions par club partenaire")
        lists = all("#subscriptions-by-club > ul")
        expect(lists.length).to eq 2
        expect(lists[0]['id']).to eq "member-#{mouettes.id}"
        expect(lists[1]['id']).to eq "member-#{usg.id}"
        expect(page).to have_selector "h3", text: "USG"
        expect(page).to have_selector "h3", text: "Les Mouettes"
        within "ul#member-#{usg.id}" do
          items = all("li")
          expect(items.length).to eq 2
          expect(items[0]).to have_content "Arthur Latrouille"
          expect(items[1]).to have_content "Jean Dupont"
        end
        within "ul#member-#{mouettes.id}" do
          items = all("li")
          expect(items.length).to eq 1
          expect(items[0]).to have_content "Pascal Durand"
        end
      end

      scenario "I can read the number of mono and multi activities for each collaborating club" do
        collaborating_clubs_mg = create :member_group
        @settings.platform_collaborating_clubs << collaborating_clubs_mg
        club1 = create :member, member_groups: [collaborating_clubs_mg], name: "USG"
        club2 = create :member, member_groups: [collaborating_clubs_mg], name: "Les Mouettes"
        perf1 = create :performance, member: club1
        perf2 = create :performance, member: club2
        nb_club1_mono = 5.times { create :subscription, performances: [perf1], member_id: club1.id }
        nb_club1_multi = 3.times { create :subscription, performances: [perf1, perf2], member_id: club1.id }
        nb_club2_mono = 4.times { create :subscription, performances: [perf1], member_id: club2.id }
        nb_club2_multi = 6.times { create :subscription, performances: [perf1, perf2], member_id: club2.id }

        visit platform_path(season: "2017-2018")
        expect(current_path).to eq platform_path
        within "h3", text: "USG" do
          expect(page).to have_selector("span", text: "(3 multi, 5 mono sur 8)")
        end
        within "h3", text: "Les Mouettes" do
          expect(page).to have_selector("span", text: "(6 multi, 4 mono sur 10)")
        end
        within "h2", text: "Liste des adhésions par club partenaire" do
          expect(page).to have_selector("span", text: "(9 multi, 9 mono sur 18)")
        end
      end

      scenario "I can toggle the facturation status of multi actitivies subscriptions" do
        collaborating_clubs_mg = create :member_group
        @settings.platform_collaborating_clubs << collaborating_clubs_mg
        usg = create :member, member_groups: [collaborating_clubs_mg], name: "USG"
        perf1 = create :performance, member: usg
        perf2 = create :performance, member: usg
        jean_dupont = build :adherent, last_name: "Dupont", first_name: "Jean"
        pascal_durand = build :adherent, last_name: "Durand", first_name: "Pascal"
        create :subscription, adherent: jean_dupont, performances: [perf1, perf2], member_id: usg.id, created_at: "15/09/2017"
        create :subscription, adherent: pascal_durand, performances: [perf1], member_id: usg.id, created_at: "15/09/2017"

        visit platform_path(season: "2017-2018")
        expect(current_path).to eq platform_path

        click_link "Adhésions par club"
        within "h2", text: "Liste des adhésions par club partenaire" do
          expect(page).to have_selector("span", text: "(€ : 0 / 1)")
        end
        within "h3", text: "USG" do
          expect(page).to have_content '(€ : 0 / 1)'
        end
        within "li", text: pascal_durand.name do
          expect(page).not_to have_link '€'
        end
        within "li", text: jean_dupont.name do
          expect(page).to have_selector "a.not-billed[title='€']"
          click_link '€'
        end

        expect(current_path).to eq platform_path
        within "h2", text: "Liste des adhésions par club partenaire" do
          expect(page).to have_selector("span", text: "(€ : 1 / 1)")
        end
        within "h3", text: "USG" do
          expect(page).to have_content '(€ : 1 / 1)'
        end
        expect(page).to have_content "L'adhésion a bien été facturée."
        within "li", text: jean_dupont.name do
          expect(page).to have_selector "a.billed[title='€']"
          click_link '€'
        end

        expect(current_path).to eq platform_path
        within "h2", text: "Liste des adhésions par club partenaire" do
          expect(page).to have_selector("span", text: "(€ : 0 / 1)")
        end
        within "h3", text: "USG" do
          expect(page).to have_content '(€ : 0 / 1)'
        end
        expect(page).to have_content "L'adhésion a bien été défacturée."
        within "li", text: jean_dupont.name do
          expect(page).to have_selector "a.not-billed[title='€']"
          click_link '€'
        end
      end
    end

    context "regarding listing by collaborating medicosocial org" do

      scenario "I can list all subscriptions" do
        medicosocial_orgs_mg = create :member_group
        @settings.platform_medicosocial_orgs << medicosocial_orgs_mg
        collaborating_medicosocial_orgs_mg = create :member_group
        @settings.platform_collaborating_medicosocial_orgs << collaborating_medicosocial_orgs_mg
        collaborating_clubs_mg = create :member_group
        @settings.platform_collaborating_clubs << collaborating_clubs_mg
        usg = create :member, name: "USG", member_groups: [collaborating_clubs_mg]
        ime_trucmuche = create :member, name: "IME Trucmuche", member_groups: [medicosocial_orgs_mg, collaborating_medicosocial_orgs_mg]
        ime_machin = create :member, name: "IME Machin", member_groups: [medicosocial_orgs_mg]
        athle = create :activity, title: "Athlétisme"
        aviron = create :activity, title: "Aviron"
        perf1 = create :performance, member: usg, activity: athle
        perf2 = create :performance, member: usg, activity: aviron
        jean_dupont = build :adherent, last_name: "Dupont", first_name: "Jean", medico_social_org: ime_trucmuche
        pascal_durand = build :adherent, last_name: "Durand", first_name: "Pascal", medico_social_org: ime_machin
        arthur_latrouille = build :adherent, last_name: "Latrouille", first_name: "Arthur", medico_social_org: nil, other_medicosocial_org: "IME Bidule"
        igor_jovovitch = build :adherent, last_name: "Jovovitch", first_name: "Igor", medico_social_org: nil, other_medicosocial_org: "IME Bidule"
        create :subscription, adherent: jean_dupont, performances: [perf1, perf2], member_id: usg.id, created_at: "15/09/2017"
        create :subscription, adherent: pascal_durand, performances: [perf1], member_id: usg.id, created_at: "15/09/2017"
        create :subscription, adherent: arthur_latrouille, performances: [perf1, perf2], member_id: usg.id, created_at: "15/09/2017"
        create :subscription, adherent: igor_jovovitch, performances: [perf2], member_id: usg.id, created_at: "15/09/2017"

        visit platform_path(season: "2017-2018")
        click_link "Adhésions par structure"

        expect(page).to have_selector("h2", text: "Liste des adhésions par structure")
        lists = all("#subscriptions-by-org > ul")
        expect(lists.length).to eq 3
        expect(lists[0]['id']).to eq "member-#{ime_machin.id}"
        expect(lists[1]['id']).to eq "member-#{ime_trucmuche.id}"
        expect(lists[2]['id']).to eq "member-naYTnbcDSe5o"
        expect(page).to have_selector "h3", text: "Structures référencées"
        expect(page).to have_selector "h4", text: "IME Trucmuche"
        expect(page).to have_selector "h4", text: "IME Machin"
        within "ul#member-#{ime_machin.id}" do
          items = all("li")
          expect(items.length).to eq 1
          expect(items[0]).to have_content "Pascal Durand"
        end
        within "ul#member-#{ime_trucmuche.id}" do
          items = all("li")
          expect(items.length).to eq 1
          expect(items[0]).to have_content "Jean Dupont"
        end
        expect(page).to have_selector "h3", text: "Structures non référencées"
        expect(page).to have_selector "h4", text: "IME Bidule"
        within "ul#member-naYTnbcDSe5o" do
          items = all("li")
          expect(items.length).to eq 2
          expect(items[0]).to have_content "Arthur Latrouille"
          expect(items[1]).to have_content "Igor Jovovitch"
        end
      end
    end

  end

  context "as a collaborating club" do
    before do
      Timecop.freeze(Date.new(2017,10,15))
      login_as_collaborating_club
    end

    let(:athle) { create :activity, title: "Athlétisme" }
    let(:swim) { create :activity, title: "Natation" }

    context "I can create one", :js do

      scenario "only if afffiliation number provided" do
        @member.update_attribute(:affiliation_number, nil)

        visit member_path(@member)
        within "aside#subscriptions" do
          expect(page).not_to have_link "Nouvelle adhésion"
          click_edit_link
        end

        expect(current_path).to eq edit_member_path(@member)
        within "form" do
          fill_in "Numéro d'affiliation", with: "123456"
          click_update_button
        end

        expect(current_path).to eq member_path(@member)
        within "aside#subscriptions" do
          expect(page).to have_link "Nouvelle adhésion"
        end
      end

      scenario "with existing medico social org" do
        medico_social_orgs_group = create :member_group
        msi = create :member, member_groups: [medico_social_orgs_group]
        @settings.platform_medicosocial_orgs << medico_social_orgs_group
        collaborating_club = create :member, name: "Mon asso", member_groups: @member.member_groups
        perf = create :performance, member: collaborating_club, activity: athle

        visit member_path(@member)
        within "aside#subscriptions" do
          click_link "Nouvelle adhésion"
        end

        expect(current_path).to eq new_member_subscription_path(@member)
        within "form" do
          fill_in "Nom", with: "Dupont"
          fill_in "Prénom", with: "Jean-Charles"
          fill_in "Date de naissance", with: "14/07/1989"
          fill_in "N° licence", with: "123456"
          select msi.name, from: "Structure médico-sociale"
          check "Mon asso - [Athlétisme] Lundi de"
          expect(page).to have_selector("div#subscription-option", text: "Formule retenue")
          click_create_button
        end

        expect(current_path).to eq member_path(@member)
        expect(page).to have_content "L'adhésion de Jean-Charles Dupont a bien été créée."
        within "aside#subscriptions div#internal" do
          click_link "Saison 2017-2018"
          within "li", text: "Jean-Charles Dupont" do
            find("i.fa-info-circle").hover
            expect(page).to have_selector("span.activities", text: "Mon asso (Athlétisme)")
          end
        end

        @settings.platform_medicosocial_orgs.clear
      end

      scenario "with custom medico social org" do
        collaborating_club = create :member, name: "Mon asso", member_groups: @member.member_groups
        perf = create :performance, member: collaborating_club, activity: athle, day: :monday, start_at: "10:00", end_at: "15:00"

        visit member_path(@member)
        within "aside#subscriptions" do
          click_link "Nouvelle adhésion"
        end

        expect(current_path).to eq new_member_subscription_path(@member)
        within "form" do
          fill_in "Nom", with: "Dupont"
          fill_in "Prénom", with: "Jean-Charles"
          fill_in "Date de naissance", with: "14/07/1989"
          fill_in "N° licence", with: "123456"
          select "Autre", from: "Structure médico-sociale"
          fill_in "Autre structure médico-sociale", with: "Une structure quelconque"
          check "Mon asso - [Athlétisme] Lundi de"
          expect(page).to have_selector("div#subscription-option", text: "Formule retenue")
          click_create_button
        end

        expect(current_path).to eq member_path(@member)
        expect(page).to have_content "L'adhésion de Jean-Charles Dupont a bien été créée."
        within "aside#subscriptions div#internal" do
          click_link "Saison 2017-2018"
          within "li", text: "Jean-Charles Dupont" do
            find("i.fa-info-circle").hover
            expect(page).to have_selector("span.activities", text: "Mon asso (Athlétisme)")
          end
        end
      end

      scenario "regarding mail notifications", type: :mailer do
        main_club = @member
        other_club1 = create :member, name: "Autre asso 1", member_groups: @member.member_groups
        other_club2 = create :member, name: "Autre asso 2", member_groups: @member.member_groups
        perf1 = create :performance, member: main_club, activity: athle
        perf2 = create :performance, member: other_club1, activity: athle
        perf3 = create :performance, member: other_club2, activity: athle
        perf4 = create :performance, member: other_club2, activity: swim, disabled: true

        visit new_member_subscription_path(@member)
        within "form" do
          fill_in "Nom", with: "Dupont"
          fill_in "Prénom", with: "Jean-Charles"
          fill_in "Date de naissance", with: "14/07/1989"
          fill_in "N° licence", with: "123456"
          select "Autre", from: "Structure médico-sociale"
          fill_in "Autre structure médico-sociale", with: "Une structure quelconque"
          check "Collaborating club - [Athlétisme] Lundi"
          check "Autre asso 1 - [Athlétisme] Lundi"
          check "Autre asso 2 - [Athlétisme] Lundi"
          expect(page).not_to have_content "Autre asso 2 - [Natation] Lundi"
          expect(page).to have_selector("div#subscription-option", text: "Formule retenue")
          click_create_button
        end

        expect(current_path).to eq member_path(@member)
        expect(page).to have_content "L'adhésion de Jean-Charles Dupont a bien été créée."
        expect(ActionMailer::Base.deliveries.count).to eq 2
      end
    end

    scenario "I can see internal and external subscriptions", :js do
      athle = create :activity, title: "Athlétisme"
      parapente = create :activity, title: "Parapente"
      voile = create :activity, title: "Voile"
      @member.update_attribute(:name, "Mon asso")
      perf1 = create :performance, member: @member, activity: athle, start_at: "18:00", end_at: "20:00"
      perf2 = create :performance, member: @member, activity: parapente, disabled: true
      external_club = create :member, name: "Les Mouettes"
      perf3 = create :performance, member: external_club, activity: voile
      internal_adherent = build :adherent, last_name: "Dupont", first_name: "Jean", permit_number: "123456"
      internal_subscription = create :subscription, adherent: internal_adherent, performances: [perf2,perf3], member_id: @member.id
      internal_subscription.update_attribute(:platform_number, "3311/1718/042")
      external_adherent = build :adherent, last_name: "Durand", first_name: "Pascal", permit_number: "654321"
      external_subscription = create :subscription, adherent: external_adherent, performances: [perf1, perf3], member_id: external_club.id

      visit member_path(@member)
      within "aside#subscriptions div#internal" do
        click_link "Saison 2017-2018"
        within "li", text: "3311/1718/042 Jean Dupont" do
          find("i.fa-info-circle").hover
          expect(page).to have_selector("span.activities", text: "Mon asso (Parapente), Les Mouettes (Voile)")
        end
      end

      within "aside#subscriptions div#external" do
        click_link "Saison 2017-2018"
        within "li", text: "Pascal Durand (Les Mouettes) →" do
          expect(page).to have_content "[Athlétisme] Lundi"
          expect(page).not_to have_content "[Voile]"
        end
      end
    end

    scenario "I cannot delete any", :js do
      athle = create :activity, title: "Athlétisme"
      adherent = build :adherent, last_name: "Dupont", first_name: "Jean"
      perf = create :performance, member: @member, activity: athle
      subscription = create :subscription, adherent: adherent, performances: [perf], member_id: @member.id

      visit member_path(@member)
      within "aside#subscriptions div#internal" do
        click_link "Saison 2017-2018"
        within "li", text: "Jean Dupont" do
          expect(page).not_to have_link "Supprimer"
        end
      end
    end

    scenario "I cannot edit any", :js do
      athle = create :activity, title: "Athlétisme"
      adherent = build :adherent, last_name: "Dupont", first_name: "Jean"
      perf = create :performance, member: @member, activity: athle
      subscription = create :subscription, adherent: adherent, performances: [perf], member_id: @member.id

      visit member_path(@member)
      within "aside#subscriptions div#internal" do
        click_link "Saison 2017-2018"
        within "li", text: "Jean Dupont" do
          expect(page).not_to have_link "Éditer"
        end
      end
    end

    scenario "I can print the licence paper", :js do
      @member.update_attribute(:affiliation_number, "3369")
      @settings.platform_collaborating_clubs << @member.member_groups
      athle = create :activity, title: "Athlétisme"
      foot = create :activity, title: "Football"
      adherent = build :adherent, last_name: "Dupont", first_name: "Jean",medico_social_org: nil, other_medicosocial_org: "Une structure", permit_number: "142536"
      perf1 = create :performance, member: @member, activity: athle
      perf2 = create :performance, member: @member, activity: foot
      subscription = create :subscription, adherent: adherent, performances: [perf1, perf2], member_id: @member.id, created_at: Date.new(2017,9,19)

      visit card_member_subscription_path(@member, subscription)
      within "section#data" do
        expect(page).to have_selector("dd", text: "Dupont")
        expect(page).to have_selector("dd", text: "Jean")
        expect(page).to have_selector("dd", text: "Collaborating club")
        expect(page).to have_selector("dd", text: "Une structure")
        expect(page).to have_selector("dd", text: "142536")
        expect(page).to have_selector("dd", text: "3369")
      end
      within "section#cover" do
        expect(page).to have_content "SAISON 2017-2018"
      end

      visit member_path(@member)
      within "aside#subscriptions div#internal" do
        click_link "Saison 2017-2018"
        within "li", text: "Jean Dupont" do
          expect(page).to have_link "Imprimer carte"
        end
      end
    end

    scenario "I can read the number of mono and multi activities ones" do
      perf1 = create :performance, member: @member
      perf2 = create :performance, member: @member
      nb_of_monoactivity_subscriptions = 5.times { create :subscription, performances: [perf1], member_id: @member.id, created_at: "15/09/2017" }
      nb_of_multiactivities_subscriptions = 3.times { create :subscription, performances: [perf1, perf2], member_id: @member.id, created_at: "15/09/2017" }

      visit member_path(@member)
      within "aside#subscriptions div#internal" do
        expect(page).to have_link "Saison 2017-2018 (3 multi, 5 mono sur 8)"
      end
    end

    scenario "I can see the billing status of any one", :js do
      perf1 = create :performance, member: @member
      perf2 = create :performance, member: @member
      jean_dupont = build :adherent, first_name: "Jean", last_name: "Dupont"
      pascal_durand = build :adherent, first_name: "Pascal", last_name: "Durand"
      billed_subscription = create :subscription, adherent: jean_dupont, performances: [perf1], member_id: @member.id, billed_at: Time.now
      not_billed_subscription = create :subscription, adherent: pascal_durand, performances: [perf1, perf2], member_id: @member.id, billed_at: nil

      visit member_path(@member)
      within "aside#subscriptions div#internal" do
        click_link "Saison 2017-2018"
        within "li", text: "Jean Dupont" do
          expect(page).to have_selector("span.billed")
        end
        within "li", text: "Pascal Durand" do
          expect(page).to have_selector("span.not-billed")
        end
      end
    end

    scenario "I can renew one", :js do
      medico_social_orgs_group = create :member_group
      msi = create :member, member_groups: [medico_social_orgs_group], name: "IME Untel"
      @settings.platform_medicosocial_orgs << medico_social_orgs_group
      athle = create :activity, title: "Athlétisme"
      adherent = build :adherent, last_name: "Dupont", first_name: "Jean", born_on: "14/07/1989", permit_number: "123456", medico_social_org: msi
      @member.update_attribute(:name, "Club partenaire")
      perf = create :performance, member: @member, activity: athle
      subscription = create :subscription, adherent: adherent, performances: [perf], member_id: @member.id
      Timecop.travel(Date.new(2018,10,15))

      visit member_path(@member)
      within "aside#subscriptions div#internal" do
        click_link "Saison 2017-2018"
        within "ul#saison_2017-2018_int li", text: "Jean Dupont" do
          click_link "Renouveler"
        end
      end

      expect(current_path).to eq renew_member_subscription_path(@member, subscription)
      within "form" do
        expect(page).to have_field("Nom", with: "Dupont")
        expect(page).to have_field("Prénom", with: "Jean")
        expect(page).to have_field("Date de naissance", with: "14/07/1989")
        expect(page).to have_field("N° licence", with: "123456")
        expect(page).to have_select("Structure médico-sociale", selected: 'IME Untel')
        expect(page).to have_checked_field("Club partenaire - [Athlétisme] Lundi")
        click_create_button
      end

      expect(current_path).to eq member_path(@member)
      expect(page).to have_content "L'adhésion de Jean Dupont a bien été créée."
      within "aside#subscriptions div#internal" do
        click_link "Saison 2018-2019"
        within "ul#saison_2018-2019_int" do
          expect(page).to have_selector("li", text: "Jean Dupont")
        end
        click_link "Saison 2017-2018"
        within "ul#saison_2017-2018_int" do
          expect(page).to have_selector("li", text: "Jean Dupont")
        end
      end
    end
  end

  context "as a collaborating medicosocial org" do
    before do
      Timecop.freeze(Date.new(2017,10,15))
      login_as_collaborating_medicosocial_org
    end

    after { Timecop.return }

    scenario "I can list susbscriptions of my patients", :js do
      @settings.platform_collaborating_medicosocial_orgs << @member.member_groups
      medicosocial_orgs_mg = create :member_group
      @settings.platform_medicosocial_orgs << medicosocial_orgs_mg
      @member.member_groups << medicosocial_orgs_mg
      collaborating_clubs_mg = create :member_group
      @settings.platform_collaborating_clubs << collaborating_clubs_mg
      usg = create :member, name: "USG", member_groups: [collaborating_clubs_mg]
      mouettes = create :member, name: "Les Mouettes", member_groups: [collaborating_clubs_mg]
      athle = create :activity, title: "Athlétisme"
      surf = create :activity, title: "Surf"
      jean_dupont = build :adherent, last_name: "Dupont", first_name: "Jean", medico_social_org: @member
      pascal_durand = build :adherent, last_name: "Durand", first_name: "Pascal", medico_social_org: @member
      perf_usg = create :performance, member: usg, activity: athle
      perf_mouettes = create :performance, member: mouettes, activity: surf
      create :subscription, adherent: jean_dupont, performances: [perf_usg, perf_mouettes], member_id: usg.id, created_at: "15/09/2017"
      create :subscription, adherent: pascal_durand, performances: [perf_mouettes], member_id: mouettes.id, created_at: "15/09/2017"

      visit member_path(@member)
      within "aside#subscriptions div#internal" do
        expect(page).to have_link "Saison 2017-2018 (1 multi, 1 mono sur 2)"
      end
      within "aside#subscriptions div#internal" do
        click_link "Saison 2017-2018"
        within "li", text: "Jean Dupont" do
          find("i.fa-info-circle").hover
          expect(page).to have_selector("span.activities", text: "USG (Athlétisme), Les Mouettes (Surf)")
        end
        within "li", text: "Pascal Durand" do
          find("i.fa-info-circle").hover
          expect(page).to have_selector("span.activities", text: "Les Mouettes (Surf)")
        end
      end
    end

  end

  context "as a member" do
    before { login_as_member }

    scenario "I cannot create any" do
      visit member_path(@member)
      expect(page).not_to have_link "Nouvelle adhésion"

      visit new_member_subscription_path(@member)
      expect(page).to have_content "Vous nêtes pas autorisé à accéder à cette zone"
    end
  end
end
