#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

feature "ContactUS mails" do
  context "as a guest" do

    context "without email contact in settings" do
      before { @settings.update_attribute(:contact_email, nil) }

      scenario "I don't have any contact form" do
        visit root_path
        within "footer" do
          click_link "Nous contacter"
        end

        expect(current_path).to eq(new_contact_us_path)
        expect(page).not_to have_selector "form"
      end
    end

    context "with email contact in settings" do
      def fill_in_contact_us_correctly
        fill_in "Nom", with: "Dupont"
        fill_in "Prénom", with: "Jean"
        fill_in "Adresse email", with: "jean-dupont@nowhere.com"
        fill_in "Numéro de téléphone", with: "0102030405"
        fill_in "Objet", with: "Contact"
        fill_in "Message", with: "Bonjour, j'aurais voulu..."
      end

      scenario "I can send a correct email to the webmaster" do
        visit root_path
        within "footer" do
          click_link "Nous contacter"
        end

        expect(current_path).to eq(new_contact_us_path)
        within "form" do
          fill_in_contact_us_correctly
          click_button "Envoyer"
        end

        expect(current_path).to eq(root_path)
        expect(page).to have_content "Votre email a été envoyé avec succès."
      end

      scenario "I can see errors when form not properly filled in" do
        visit new_contact_us_path
        within "form" do
          click_button "Envoyer"
        end

        expect(current_path).to eq(contact_us_path)
        expect(page).to have_selector("span.error", text: "doit être rempli(e)")
      end

      scenario "I can see errors when nickname provided" do
        visit new_contact_us_path
        within "form" do
          fill_in_contact_us_correctly
          find("#contact_us_mail_nickname", visible: false).set "bibi"
          click_button "Envoyer"
        end

        expect(current_path).to eq(contact_us_path)
        expect(page).to have_selector("span.error", text: "n'est pas valide")
      end

      scenario "I am informed if an error occured while sending off the email" do
        #Given
        allow(Mailer).to receive_message_chain(:contact_us, :deliver_now).and_raise("Error")
        # When
        visit new_contact_us_path
        within "form" do
          fill_in_contact_us_correctly
          click_button "Envoyer"
        end
        # Then
        expect(current_path).to eq(contact_us_path)
        expect(page).to have_content "Nous n'avons pu envoyer votre email pour une raison quelconque. Merci d'essayer un peu plus tard. Si le problème persiste, merci de nous contacter par téléphone."
      end
    end
  end

end
