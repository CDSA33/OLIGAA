#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

feature "Manage Novels" do
  context "as an admin" do
    before { login_as_admin }

    scenario "I can create one" do
      novel = build :novel
      visit root_path
      within "nav#admin_links" do
        click_link "Informations"
      end

      expect(current_path).to eq(novels_path)
      within "section#content" do
        click_link "Nouvelle information"
      end

      expect(current_path).to eq(new_novel_path)
      within "form" do
        fill_in "Sujet", with: novel.title
        fill_in "Contenu", with: novel.content
        click_create_button
      end

      expect(current_path).to eq(novel_path(Novel.first))
      within "section#content" do
        expect(page).to have_content "Information créée avec succès."
        expect(page).to have_selector("h1", text: novel.title)
        expect(page).to have_content novel.content
      end
    end

    scenario "I can delete one", :js do
      novel = create :novel
      visit novels_path
      click_link "Informations sans type"
      within "#novel_list ul > li", text: novel.title do
        click_delete_link
      end

      expect(current_path).to eq(novels_path)
      within "section#content" do
        expect(page).to have_content "Information supprimée avec succès."
        expect(page).not_to have_selector("li", text: novel.title)
      end
    end

    context "regarding edition" do
      before { @novel = create :novel }

      scenario "I can change its info", :js do
        new_title = "Nouveau sujet"
        visit novels_path
        within "section#content" do
          click_link "Informations sans type"
          within "#novel_list ul > li", text: @novel.title do
            click_edit_link
          end
        end

        expect(current_path).to eq(edit_novel_path(@novel))
        within "form" do
          fill_in "Sujet", with: new_title
          click_update_button
        end

        expect(current_path).to eq(novel_path(@novel.reload))
        within "section#content" do
          expect(page).to have_content "Information mise à jour avec succès."
          expect(page).to have_selector("h1", text: new_title)
        end
      end

      scenario "I can associate to a novel type", :js do
        nt = create :novel_type
        visit edit_novel_path @novel
        within "form" do
          select nt.title
          click_update_button
        end

        expect(current_path).to eq(novel_path(@novel.reload))
        within "section#content" do
          expect(page).to have_content "Information mise à jour avec succès."
        end
        visit novels_path
        within "section#content" do
          click_link nt.title
          within "div#nt-#{nt.id}" do
            expect(page).to have_selector("li", text: @novel.title)
          end
        end
      end

      scenario "I can make one to become a template" do
        nt = create :novel_type
        visit edit_novel_path @novel
        within "form" do
          within "div.novel_template" do
            choose "Oui"
          end
          click_update_button
        end

        expect(current_path).to eq(novel_path(@novel.reload))
        within "section#content" do
          expect(page).to have_content "Information mise à jour avec succès."
          within "p#novel_template" do
            expect(page).to have_content "OUI"
          end
        end
      end

      scenario "I can publish one" do
        nt = create :novel_type
        visit edit_novel_path @novel
        within "form" do
          within "div.novel_public" do
            choose "Oui"
          end
          click_update_button
        end

        expect(current_path).to eq(novel_path(@novel.reload))
        within "section#content" do
          expect(page).to have_content "Information mise à jour avec succès."
          within "p#novel_public" do
            expect(page).to have_content "OUI"
          end
        end
      end

      context "regarding attached_files" do
        scenario "I can attach a file", :js do
          attached_file = build :attached_file
          visit edit_novel_path @novel
          within "form" do
            click_link "Pièces jointes"
            click_link "Ajouter une pièce jointe"
            fill_in "Nom", with: attached_file.name
            attach_file "Fichier", sample_text_path
            click_update_button
          end

          expect(current_path).to eq(novel_path(@novel))
          within "section#content dl.attachments" do
            expect(page).to have_selector("a", text: attached_file.name)
          end
        end

        scenario "I can remove a file", :js do
          attached_file = create :attached_file, attachable: @novel
          visit edit_novel_path @novel
          within "form" do
            click_link "Pièces jointes"
            click_link "Supprimer la pièce jointe"
            click_update_button
          end

          expect(current_path).to eq(novel_path(@novel))
          expect(page).not_to have_selector("dl.attachments")
        end
      end

      scenario "I can associate to many activities" do
        a1 = create :activity
        a2 = create :activity
        a3 = create :activity
        visit edit_novel_path @novel
        within "form" do
          check a1.title
          check a3.title
          click_update_button
        end

        expect(current_path).to eq(novel_path(@novel.reload))
        within "section#content" do
          expect(page).to have_content "Information mise à jour avec succès."
          expect(page).to have_content a1.title
          expect(page).not_to have_content a2.title
          expect(page).to have_content a3.title
        end
      end

      context "regarding the picture" do
        scenario "I can attach one" do
          visit edit_novel_path @novel
          within "form" do
            attach_file "Illustration", sample_image_path
            click_update_button
          end

          expect(current_path).to eq(novel_path(@novel))
          within "section#content" do
            expect(page).to have_content "Information mise à jour avec succès."
            expect(page).to have_selector("img#novel_picture")
          end
        end

        scenario "I can remove it" do
          novel = create :novel_with_picture
          visit edit_novel_path novel
          within "form" do
            check "Supprimer l'illustration"
            click_update_button
          end

          expect(current_path).to eq(novel_path(novel))
          expect(page).not_to have_selector("img#novel_picture")
        end
      end
    end

    scenario "I can duplicate one" do
      novel = create :novel, template: true, public: true
      visit novel_path novel
      within "section#content" do
        click_link "Dupliquer"
      end

      expect(current_path).to eq(duplicate_novel_path(novel))
      within "form" do
        expect(page).to have_field("Sujet", with: novel.title)
        expect(page).to have_field("Contenu", with: novel.content)
        expect(page).to have_selector('input#novel_template_false[checked=checked]')
        expect(page).to have_selector('input#novel_public_false[checked=checked]')
        expect(page).to have_button "Mettre à jour"
      end
    end

    scenario "I can send one by email", :js do
      novel = create :novel
      visit novel_path novel
      click_link "Envoyer par mail"

      expect(current_path).to eq(new_email_path)
      within "form" do
        find("h3", text: "Information").click
        within "div#information" do
          expect(page).to have_field("Sujet", with: novel.title)
          expect(page).to have_field("Contenu", with: novel.content)
        end
      end
    end
  end

  context "as a guest" do
    scenario "I have access to the 6 latest ones on the home page" do
      7.times { |n| create :novel, public: true, title: "Information n°#{n+1}" }
      visit root_path
      within "section#content" do
        expect(page).to have_content "Information n°2"
        expect(page).not_to have_content "Information n°1"
        click_link "Suivants"
      end

      within "section#content" do
        expect(page).not_to have_content "Information n°2"
        expect(page).to have_content "Information n°1"
      end
    end

    scenario "I can access to a published one" do
      novel = create :novel, public: true
      visit root_path
      within "section#home_news" do
        click_link novel.title
      end

      expect(current_path).to eq(novel_path(novel))
      within "section#content" do
        expect(page).to have_selector("h1", text: novel.title)
        expect(page).to have_content novel.content
      end
    end

    scenario "I cannot access to a private one" do
      novel = create :novel, public: false
      visit root_path
      expect(page).not_to have_selector("section#home_news")
      visit novel_path novel

      expect(current_path).to eq(root_path)
      within "section#content" do
        expect(page).to have_content "Désolé, vous n'êtes pas autorisé à accéder à cette partie du site."
      end
    end

    scenario "I can access to the atom feed of published novels" do
      pub_novel1 = create :novel, public: true
      pub_novel2 = create :novel, public: true
      priv_novel = create :novel, public: false

      visit root_path
      within "section#home_news" do
        click_link "Flux RSS"
      end

      expect(current_path).to eq(feed_novels_path(format: :atom))
      expect(page.source["<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<feed xml:lang=\"fr\" xmlns=\"http://www.w3.org/2005/Atom\">"]).not_to be_nil
      expect(page).to have_content pub_novel1.title
      expect(page).to have_content pub_novel2.title
      expect(page).not_to have_content priv_novel.title
    end
  end
end
