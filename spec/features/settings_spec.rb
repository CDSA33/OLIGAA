#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

feature "Manage Settings" do
  context "As an admin" do
    before { login_as_admin }

    scenario "I can set the website title" do
      new_title = "Nouveau titre"
      visit edit_settings_path
      within "form#edit_settings" do
        fill_in "Titre du site", with: new_title
        click_button "Mettre à jour"
      end

      expect(current_path).to eq(edit_settings_path)
      expect(page).to have_content "Les paramètres ont bien été mis à jour"
      within "header h1" do
        expect(page).to have_content new_title
      end
    end

    scenario "I can set the website motto" do
      new_motto = "Nouveau slogan"
      visit edit_settings_path
      within "form#edit_settings" do
        fill_in "Slogan du site", with: new_motto
        click_button "Mettre à jour"
      end

      expect(current_path).to eq(edit_settings_path)
      expect(page).to have_content "Les paramètres ont bien été mis à jour"
      within "header#banner" do
        expect(page).to have_selector("h2", text: new_motto)
      end
    end

    scenario "I can set the home message" do
      new_home_message = "Nouveau message d accueil"
      visit edit_settings_path
      within "form#edit_settings" do
        fill_in "Message d'accueil", with: new_home_message
        click_button "Mettre à jour"
      end

      expect(current_path).to eq(edit_settings_path)
      expect(page).to have_content "Les paramètres ont bien été mis à jour"
      visit root_path
      within "section#content" do
        expect(page).to have_content new_home_message
      end
    end

    scenario "I can set the season switch day and month" do
      day_select = "settings_season_birthdate_3i"
      month_select = "settings_season_birthdate_2i"
      visit edit_settings_path

      expect(page).to have_field(day_select, with: "15")
      expect(page).to have_field(month_select, with: "8")

      within "form#edit_settings" do
        select "1", from: day_select
        select "septembre", from: month_select
        click_button "Mettre à jour"
      end

      expect(current_path).to eq(edit_settings_path)
      expect(page).to have_content "Les paramètres ont bien été mis à jour"
      within "section#content" do
        expect(page).to have_field(day_select, with: "1")
        expect(page).to have_field(month_select, with: "9")
      end
    end

    context "regarding images" do
      scenario "I can provide a site logo" do
        visit root_path
        within "header h1" do
          expect(page).to have_content @settings.title
        end

        visit edit_settings_path
        within "form#edit_settings" do
          attach_file "Logo site", sample_image_path
          click_button "Mettre à jour"
        end

        expect(current_path).to eq(edit_settings_path)
        expect(page).to have_content "Les paramètres ont bien été mis à jour"
        within "header h1" do
          expect(page.find('img.logo')['src']).to have_content "sample_image.png"
        end
      end

      scenario "I can remove the site logo" do
        @settings.site_logo = File.new(sample_image_path)
        @settings.save!

        visit root_path
        within "header h1" do
          expect(page.find('img.logo')['src']).to have_content "sample_image.png"
        end

        visit edit_settings_path
        within "form#edit_settings" do
          check "Supprimer le logo du site"
          click_button "Mettre à jour"
        end

        expect(current_path).to eq(edit_settings_path)
        expect(page).to have_content "Les paramètres ont bien été mis à jour"

        within "header h1" do
          expect(page).not_to have_selector "img"
          expect(page).to have_content @settings.title
        end
      end

      scenario "I can provide a motto logo" do
        visit root_path
        within "header h2" do
          expect(page).to have_content @settings.motto
        end

        visit edit_settings_path
        within "form#edit_settings" do
          attach_file "Logo slogan", sample_image_path
          click_button "Mettre à jour"
        end

        expect(current_path).to eq(edit_settings_path)
        expect(page).to have_content "Les paramètres ont bien été mis à jour"
        within "header h2" do
          expect(page).to have_content @settings.motto
          expect(page.find('img.motto_logo')['src']).to have_content "sample_image.png"
        end
      end

      scenario "I can remove the motto logo" do
        @settings.motto_logo = File.new(sample_image_path)
        @settings.save!

        visit root_path
        within "header h2" do
          expect(page).to have_content @settings.motto
          expect(page.find('img.motto_logo')['src']).to have_content "sample_image.png"
        end

        visit edit_settings_path
        within "form#edit_settings" do
          check "Supprimer le logo du slogan"
          click_button "Mettre à jour"
        end

        expect(current_path).to eq(edit_settings_path)
        expect(page).to have_content "Les paramètres ont bien été mis à jour"

        within "header h2" do
          expect(page).not_to have_selector "img"
          expect(page).to have_content @settings.motto
        end
      end
    end

    context "regarding contact page" do

      scenario "I can set the contact message" do
        contact_message = "Message du formulaire de contact"
        visit edit_settings_path
        within "form#edit_settings" do
          fill_in "Message de contact", with: contact_message
          click_button "Mettre à jour"
        end

        expect(current_path).to eq(edit_settings_path)
        expect(page).to have_content "Les paramètres ont bien été mis à jour"
        visit new_contact_us_path
        within "section#content" do
          expect(page).to have_content contact_message
        end
      end

      scenario "I can set the email for the contact form" do
        new_contact_email = "test@example.com"
        visit edit_settings_path
        within "form#edit_settings" do
          fill_in "Adresse email de contact", with: new_contact_email
          click_button "Mettre à jour"
        end

        expect(current_path).to eq(edit_settings_path)
        expect(page).to have_content "Les paramètres ont bien été mis à jour"
        within "form#edit_settings" do
          expect(page).to have_field("Adresse email de contact", with: new_contact_email)
        end
        within "footer"do
          expect(page).to have_link("Nous contacter")
        end
      end

      context "regarding contact us link" do
        scenario "exists if a contact message is provided" do
          @settings.update_attribute(:contact_message, "Message de contact")

          visit root_path
          within "footer" do
            expect(page).to have_link "Nous contacter"
          end
        end

        scenario "exists if a contact message is provided" do
          @settings.update_attribute(:contact_email, "contact@ip.fr")

          visit root_path
          within "footer" do
            expect(page).to have_link "Nous contacter"
          end

        end

        scenario "does not exist if neihter a contact message nor a contact email is provided" do
          @settings.update_attributes(contact_message: nil, contact_email: nil)

          visit root_path
          within "footer" do
            expect(page).not_to have_link "Nous contacter"
          end
        end
      end
    end

    context "regarding map display" do
      scenario "I can attach member groups" do
        create :menu, title: "Où pratiquer", uid: "member_groups"
        group1 = create :member_group
        group2 = create :member_group
        visit edit_settings_path
        within "form#edit_settings" do
          within "div#map" do
            check group2.title
          end
          click_button "Mettre à jour"
        end
        expect(current_path).to eq(edit_settings_path)
        expect(page).to have_content "Les paramètres ont bien été mis à jour"
        visit root_path
        within "nav#main-nav" do
          expect(page).to have_selector("span.menu_title", text: "Où pratiquer")
          expect(page).not_to have_content group1.title
          expect(page).to have_content group2.title
        end
      end
    end


    context "regarding gcal access" do
      def fill_in_gcal_credentials
        @email = 'test-gcal@example.org'
        @pass = 'secret'
        visit edit_settings_path
        within "form#edit_settings" do
          fill_in "Identifiant", with: @email
          click_button "Mettre à jour"
        end
      end

      scenario "I can set the credentials" do
        fill_in_gcal_credentials

        expect(current_path).to eq(edit_settings_path)
        expect(page).to have_content "Les paramètres ont bien été mis à jour"
        within "form#edit_settings" do
          expect(page).to have_field("Identifiant", with: @email)
        end
      end

      context "with good credentials" do
        scenario "I can see an OK status" do
          fill_in_gcal_credentials
          expect(page).to have_content "Les paramètres ont bien été mis à jour"
          gcal = GoogleAgenda::Service.instance
          allow(gcal).to receive(:api_client).and_return({})
          visit edit_settings_path

          within "form#edit_settings" do
            expect(page).to have_selector("span.notice_message", text: "OK")
          end
        end
      end

      context "with bad credentials" do
        scenario "I can see a NOK status" do
          fill_in_gcal_credentials
          expect(page).to have_content "Les paramètres ont bien été mis à jour"
          gcal = GoogleAgenda::Service.instance
          allow(gcal).to receive(:api_client).and_raise("Unauthorized")
          visit edit_settings_path

          within "form#edit_settings" do
            expect(page).to have_selector("span.error_message", text: "Status Unauthorized")
          end
        end
      end
    end

    context "regarding newsletters" do
      scenario "I can set the global title" do
        newsletters_title = "Titre global aux newsletters"
        visit edit_settings_path
        within "form#edit_settings" do
          fill_in "Titre global", with: newsletters_title
          click_button "Mettre à jour"
        end

        expect(current_path).to eq(edit_settings_path)
        expect(page).to have_content "Les paramètres ont bien été mis à jour"
        within "form#edit_settings" do
          expect(page).to have_field("Titre global", with: newsletters_title)
        end
      end

      scenario "I can check the default partners" do
        pg = create :partners_group
        p1 = create :partner, group: pg
        p2 = create :partner, group: pg
        p3 = create :partner
        visit edit_settings_path
        within "form#edit_settings" do
          within "div#newsletters" do
            check p1.name
            check p3.name
          end
          click_button "Mettre à jour"
        end

        expect(current_path).to eq(edit_settings_path)
        expect(page).to have_content "Les paramètres ont bien été mis à jour"
        within "form#edit_settings" do
          within "div#newsletters" do
            expect(page).to have_checked_field(p1.name)
            expect(page).to have_checked_field(p3.name)
            expect(page).to have_no_checked_field(p2.name)
            expect(page).to have_content p2.name
          end
        end
      end
    end

    context "regarding design" do
      context " regarding colors" do
        scenario "I can set the top color" do
          main_color = '#003F7E'
          visit edit_settings_path
          within "form#edit_settings" do
            fill_in "Couleur principale", with: main_color
            click_button "Mettre à jour"
          end

          expect(current_path).to eq(edit_settings_path)
          expect(page).to have_content "Les paramètres ont bien été mis à jour"
          within "form#edit_settings" do
            expect(find('input#settings_main_color').value).to eq(main_color)
          end
        end

        scenario "I can set the bottom color" do
          secondary_color = '#003F7E'
          visit edit_settings_path
          within "form#edit_settings" do
            fill_in "Couleur secondaire", with: secondary_color
            click_button "Mettre à jour"
          end

          expect(current_path).to eq(edit_settings_path)
          expect(page).to have_content "Les paramètres ont bien été mis à jour"
          within "form#edit_settings" do
            expect(find('input#settings_secondary_color').value).to eq(secondary_color)
          end
        end

        scenario "I can set the color for h1" do
          h1_color = '#003F7E'
          visit edit_settings_path
          within "form#edit_settings" do
            fill_in "Couleur titres de niveau 1", with: h1_color
            click_button "Mettre à jour"
          end

          expect(current_path).to eq(edit_settings_path)
          expect(page).to have_content "Les paramètres ont bien été mis à jour"
          within "form#edit_settings" do
            expect(find('input#settings_h1_color').value).to eq(h1_color)
          end
        end

        scenario "I can set the color for h2" do
          h2_color = '#003F7E'
          visit edit_settings_path
          within "form#edit_settings" do
            fill_in "Couleur titres de niveau 2", with: h2_color
            click_button "Mettre à jour"
          end

          expect(current_path).to eq(edit_settings_path)
          expect(page).to have_content "Les paramètres ont bien été mis à jour"
          within "form#edit_settings" do
            expect(find('input#settings_h1_color').value).to eq(h2_color)
          end
        end
      end
    end

    context "regading platform" do
      scenario "I can choose collaborating clubs" do
        group1 = create :member_group
        group2 = create :member_group
        visit edit_settings_path
        within "form#edit_settings" do
          within "div.settings_platform_collaborating_clubs" do
            check group2.title
          end
          click_button "Mettre à jour"
        end
        expect(current_path).to eq(edit_settings_path)
        expect(page).to have_content "Les paramètres ont bien été mis à jour"
        within "form#edit_settings div.settings_platform_collaborating_clubs" do
          expect(page).to have_checked_field(group2.title)
          expect(page).to have_no_checked_field(group1.title)
        end
      end

      scenario "I can choose medico-social organizations" do
        group1 = create :member_group
        group2 = create :member_group
        visit edit_settings_path
        within "form#edit_settings" do
          within "div.settings_platform_medicosocial_orgs" do
            check group2.title
          end
          click_button "Mettre à jour"
        end
        expect(current_path).to eq(edit_settings_path)
        expect(page).to have_content "Les paramètres ont bien été mis à jour"
        within "form#edit_settings div.settings_platform_medicosocial_orgs" do
          expect(page).to have_checked_field(group2.title)
          expect(page).to have_no_checked_field(group1.title)
        end
      end

      scenario "I can choose collaborating medico-social organizations" do
        group1 = create :member_group
        group2 = create :member_group
        visit edit_settings_path
        within "form#edit_settings" do
          within "div.settings_platform_collaborating_medicosocial_orgs" do
            check group2.title
          end
          click_button "Mettre à jour"
        end
        expect(current_path).to eq(edit_settings_path)
        expect(page).to have_content "Les paramètres ont bien été mis à jour"
        within "form#edit_settings div.settings_platform_collaborating_medicosocial_orgs" do
          expect(page).to have_checked_field(group2.title)
          expect(page).to have_no_checked_field(group1.title)
        end
      end

      scenario "I can provide the prices" do
        visit edit_settings_path
        within "form#edit_settings" do
          within "div#platform" do
            within "div#mono-activity" do
              fill_in "Structure partenaire", with: "80.00"
              fill_in "Structure non partenaire", with: "90.00"
            end
            within "div#multi-activities" do
              fill_in "Structure partenaire", with: "120.00"
              fill_in "Structure non partenaire", with: "150.00"
            end
          end
          click_button "Mettre à jour"
        end

        expect(current_path).to eq(edit_settings_path)
        expect(page).to have_content "Les paramètres ont bien été mis à jour"
        within "form#edit_settings" do
          within "div#platform" do
            within "div#mono-activity" do
              expect(find('input#settings_mono_activity_collaborating_price').value).to eq("80.0")
              expect(find('input#settings_mono_activity_standard_price').value).to eq("90.0")
            end
            within "div#multi-activities" do
              expect(find('input#settings_multi_activities_collaborating_price').value).to eq("120.0")
              expect(find('input#settings_multi_activities_standard_price').value).to eq("150.0")
            end
          end
        end
      end
    end
  end
end
