#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

feature "Manage Partners" do
  context "as an admin", :js do
    before { login_as_admin }

    scenario "I can create a partner" do
      partner = build :partner
      visit root_path
      within "nav#admin_links" do
        click_link "Partenaires"
      end

      expect(current_path).to eq(partners_path)
      within "section#content" do
        click_link "Nouveau partenaire"
      end

      expect(current_path).to eq(new_partner_path)
      within "section#content form" do
        fill_in "Nom", with: partner.name
        fill_in "URL", with: partner.url
        click_create_button
      end

      expect(current_path).to eq(partners_path)
      within "section#content" do
        expect(page).to have_content "Partenaire créé avec succès."
        click_link "Divers"
        expect(page).to have_selector("li", text: partner.name)
        expect(page).to have_selector("li", text: partner.url)
      end
    end

    scenario "I can destroy a partner" do
      partner = create :partner
      visit partners_path
      click_link "Divers"
      within "#partner_list ul > li", text: partner.name do
        click_delete_link
      end

      expect(current_path).to eq(partners_path)
      within "section#content" do
        expect(page).to have_content "Partenaire supprimé avec succès."
        expect(page).not_to have_content partner.name
      end
    end

    context "regarding edition" do
      before(:each) { @partner = create :partner }

      scenario "I can change basic data" do
        new_name = "Nouveau nom"
        visit partners_path
        click_link "Divers"
        within "#partner_list ul > li", text: @partner.name do
          click_edit_link
        end

        expect(current_path).to eq(edit_partner_path(@partner))
        within "section#content form" do
          fill_in "Nom", with: new_name
          click_update_button
        end

        expect(current_path).to eq(partners_path)
        within "section#content" do
          expect(page).to have_content "Partenaire mis à jour avec succès."
          click_link "Divers"
          expect(page).to have_selector("li", text: new_name)
          expect(page).not_to have_content @partner.name
        end
      end

      context "regarding the logo" do
        scenario "I can attach one or change it" do
          visit edit_partner_path(@partner)
          within "section#content form" do
            attach_file "Ajouter ou modifier le logo", sample_image_path
            click_update_button
          end

          expect(current_path).to eq(partners_path)
          within "section#content" do
            expect(page).to have_content "Partenaire mis à jour avec succès."
            click_link "Divers"
            within "#partner_list ul > li", text: @partner.name do
              expect(page).to have_selector "img"
            end
          end
        end

        scenario "I can remove it" do
          partner = create :partner_with_logo
          visit edit_partner_path(partner)
          within "section#content form" do
            check "Supprimer le logo"
            click_update_button
          end

          expect(current_path).to eq(partners_path)
          within "section#content" do
            expect(page).to have_content "Partenaire mis à jour avec succès."
            click_link "Divers"
            within "#partner_list ul > li", text: @partner.name do
              expect(page).not_to have_selector "img"
              expect(page).to have_selector "i[title='Pas de logo']"
            end
          end
        end
      end

      scenario "I can (dis-)associate it to a partners group" do
        pg1 = create :partners_group
        pg2 = create :partners_group
        partner = create :partner, group: pg1
        visit partners_path
        within "section#content" do
          expect(page).not_to have_content partner.name
          click_link pg1.title
          expect(page).to have_selector("li", text: partner.name)
        end


        visit edit_partner_path(partner)
        within "form" do
          select pg2.title, from: "Groupe"
          click_update_button
        end

        expect(current_path).to eq(partners_path)
        within "section#content" do
          expect(page).to have_content "Partenaire mis à jour avec succès."
          click_link pg1.title
          expect(page).not_to have_content partner.name
          click_link pg2.title
          expect(page).to have_selector("li", text: partner.name)
        end
      end
    end

    context "regarding positionning within a given group" do

        before do
          @pg = create :partners_group
          @p1 = create :partner, group: @pg
          @p2 = create :partner, group: @pg
        end

      scenario "I can move a partner up", :js do
        visit partners_path

        click_link @pg.title
        within "div#pg-#{@pg.id}" do
          expect(first('li')).to have_content @p1.name
          within 'li', text: @p2.name do
            click_move_up_link
          end
        end

        expect(current_path).to eq(partners_path)
        click_link @pg.title
        within "div#pg-#{@pg.id}" do
          expect(first('li')).to have_content @p2.name
        end
      end

      scenario "I can move a partner down", :js do
        visit partners_path

        click_link @pg.title
        within "div#pg-#{@pg.id}" do
          expect(first('li')).to have_content @p1.name
          within 'li', text: @p1.name do
            click_move_down_link
          end
        end

        expect(current_path).to eq(partners_path)
        click_link @pg.title
        within "div#pg-#{@pg.id}" do
          expect(first('#partner_list ul > li')).to have_content @p2.name
        end
      end
    end
  end

  context "as a guest" do
    scenario "I can access to the partners page" do
      pg = create :partners_group
      p1 = create :partner, group: pg, url: nil
      p2 = create :partner_with_logo
      p3 = create :partner_with_logo, url: nil
      visit root_path
      within "footer" do
        click_link "Partenaires"
      end

      expect(current_path).to eq(display_partners_path)
      within "section#content" do
        expect(page).to have_selector("h1", text: "Nos partenaires")
        expect(page).to have_selector("h2", text: pg.title)
        expect(page).to have_selector("h2", text: "Divers")
        within "li#p-#{p1.id}" do
          expect(page).to have_content p1.name
          expect(page).not_to have_link p1.name
          expect(page).not_to have_selector "img"
        end
        within "li#p-#{p2.id}" do
          expect(page).to have_link p2.name
          expect(page).to have_selector "img"
        end
        within "li#p-#{p3.id}" do
          expect(page).to have_content p3.name
          expect(page).not_to have_link p3.name
          expect(page).to have_selector "img"
        end
      end
    end
  end
end
