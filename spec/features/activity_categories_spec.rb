#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

feature "Manage Activity Categories" do
  context "as an admin" do
    before { login_as_admin }

    scenario "I can create one" do
      cat = build :activity_category
      visit root_path
      within "nav#admin_links" do
        click_link "Activités"
      end
      within "section#content" do
        click_link "Nouvelle catégorie d'activité"
      end

      expect(current_path).to eq(new_activity_category_path)
      within "form" do
        fill_in "Intitulé", with: cat.title
        fill_in "Description", with: cat.description
        click_create_button
      end

      expect(current_path).to eq(activity_category_path(ActivityCategory.first))
      within "section#content" do
        expect(page).to have_content "La catégorie d'activité a été créée avec succès."
        expect(page).to have_selector("h1", text: cat.title)
        expect(page).to have_content cat.description
      end
    end

    scenario "I can delete one", :js do
      cat = create :activity_category
      visit activities_path
      within "#activity_list"  do
        within "li", text: cat.title do
          click_link cat.title
          within "p.AdminCard-headingActions" do
            click_delete_link
          end
        end
      end

      expect(current_path).to eq(activities_path)
      within "section#content" do
        expect(page).to have_content "La catégorie d'activité a été supprimée avec succès."
        expect(page).not_to have_content cat.title
      end
    end

    scenario "I can edit one", :js do
      cat = create :activity_category
      new_title = "Nouvel intitulé"
      visit activities_path
      within "#activity_list" do
        within "li", text: cat.title do
          click_link cat.title
          within "p.AdminCard-headingActions" do
            click_edit_link
          end
        end
      end

      expect(current_path).to eq(edit_activity_category_path(cat))
      within "form" do
        fill_in "Intitulé", with: new_title
        click_update_button
      end

      expect(current_path).to eq(activity_category_path(cat.reload))
      within "section#content" do
        expect(page).to have_content "La catégorie d'activité a été modifiée avec succès."
        expect(page).to have_selector("h1", text: new_title)
      end
    end
  end
end