require 'rails_helper'

shared_examples_for "seasoned_events" do

  let(:factory) { described_class.name.underscore.to_sym }

  it "#seasoned_events returns seasons with corresponding events" do
    model = create factory
    ev1 = create :event, begin_at: nil, end_at: nil
    ev2 = create :event, begin_at: "02/07/2013 09:00", end_at: "02/07/2013 11:00"
    ev3 = create :event, begin_at: "02/07/2012 09:00", end_at: "02/07/2012 11:00"
    ev4 = create :event, begin_at: "02/01/2012 09:00", end_at: "02/01/2012 11:00"
    model.events << [ev1, ev2, ev3, ev4]
    expected = [["Saison 2012-2013", [ev2]], ["Saison 2011-2012", [ev3, ev4]]]
    expect(model.seasoned_events).to eq expected
  end
end
