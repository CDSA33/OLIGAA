#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

describe SubscriptionsMailer do

  before(:each) do
    c1 = create :contact, email: "c1@ip.fr"
    c2 = create :contact, email: "c2@ip.fr"
    @member = create :member, name: "Mon asso"
    @member.update_attribute(:contacts, [c1,c2])
    athle = create :activity, title: "Athlétisme"
    @perf1 = create :performance, member: @member, activity: athle, day: :monday, start_at: "18:00", end_at: "20:00"
    @perf2 = create :performance, member: @member, activity: athle, day: :wednesday, start_at: "14:00", end_at: "16:00"
    adherent = build :adherent, last_name: "Dupont", first_name: "Jean", born_on: "25/08/1986", permit_number: "123456"
    @subscription = create :subscription, adherent: adherent, performances: [@perf1, @perf2], member_id: @member.id
  end

  describe "notify_collaborating_club" do
    before(:each) do
      @mail = SubscriptionsMailer.notify_collaborating_club(@subscription, @member)
    end

    it "renders the headers" do
      expect(@mail.subject).to eq("Nouveau participant à une de vos séances")
      expect(@mail.from).to eq(["contact@example.com"])
      expect(@mail.to).to eq(["c1@ip.fr", "c2@ip.fr"])
    end

    it "renders the body" do
      expected_body = <<EOS
Bonjour,

Par ce mail, nous vous informons que le sportif suivant :
- Prénom Nom : Jean Dupont
- Club d'origine : Mon asso
- Date de naissance : 25/08/1986
- N° licence FFSA : 123456

viendra pratiquer certaines de vos activités lors des séances suivantes :
- [Athlétisme] Lundi de 18h00 à 20h00
- [Athlétisme] Mercredi de 14h00 à 16h00

Vous devez maintenant lui prendre une licence Multiclub, via l'espace licence de la FFSA (http://espacelicenceffsa.fr/user/login) afin qu'il soit couvert sur les activités sportives de votre club.

Nous vous remercions de bien vouloir vérifier sa présence et qu'il est en possession de son Pass'Multiactivités lors de chaque séance.
EOS

      expect(@mail.body.encoded.gsub(/\r/,'')).to match(expected_body)
    end
  end

  describe "removed_performances" do
    before(:each) do
      @mail = SubscriptionsMailer.removed_performances(@subscription, [@perf2])
    end

    it "renders the headers" do
      expect(@mail.subject).to eq("Un participant quitte une de vos séances")
      expect(@mail.from).to eq(["contact@example.com"])
      expect(@mail.to).to eq(["c1@ip.fr", "c2@ip.fr"])
    end

    it "renders the body" do
      expected_body = <<EOS
Bonjour,

Par ce mail, nous vous informons que le sportif suivant :
- Prénom Nom : Jean Dupont
- Club d'origine : Mon asso
- Date de naissance : 25/08/1986
- N° licence FFSA : 123456

ne participera plus à votre (vos) séance(s) suivante(s) :
- [Athlétisme] Mercredi de 14h00 à 16h00

Nous vous remercions de ne plus noter sa présence à cette (ces) séance(s) sportive(s).
EOS

      expect(@mail.body.encoded.gsub(/\r/,'')).to match(expected_body)
    end
  end

  describe "added_performances" do
    before(:each) do
      @mail = SubscriptionsMailer.added_performances(@subscription, [@perf2])
    end

    it "renders the headers" do
      expect(@mail.subject).to eq("Nouveau participant à une de vos séances")
      expect(@mail.from).to eq(["contact@example.com"])
      expect(@mail.to).to eq(["c1@ip.fr", "c2@ip.fr"])
    end

    it "renders the body" do
      expected_body = <<EOS
Bonjour,

Par ce mail, nous vous informons que le sportif suivant :
- Prénom Nom : Jean Dupont
- Club d'origine : Mon asso
- Date de naissance : 25/08/1986
- N° licence FFSA : 123456

viendra pratiquer certaines de vos activités lors des séances suivantes :
- [Athlétisme] Mercredi de 14h00 à 16h00

Si ce n'est pas déjà fait, vous devez maintenant lui prendre une licence Multiclub, via l'espace licence de la FFSA (http://espacelicenceffsa.fr/user/login) afin qu'il soit couvert sur les activités sportives de votre club.

Nous vous remercions de bien vouloir vérifier sa présence et qu'il est en possession de son Pass'Multiactivités lors de chaque séance.
EOS

      expect(@mail.body.encoded.gsub(/\r/,'')).to match(expected_body)
    end
  end
end
