#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
FactoryGirl.define do
  factory :member do
    sequence(:login) { |n| "member-login-#{n}" }
    name { "Membre #{Faker::Lorem.unique.word}" }
    description "Member description"
    address "Member address"
    website "http://member.website.com"
    more_info "Member info"
    other_activities "Member other activities text"
    join_us "Member join us text"
    partners "Member partners text"
    updating_password true
    password 'pass'
    affiliation_number { "#{Faker::Lorem.unique.word}" }

    after :build do |member, evaluator|
      member.contacts << FactoryGirl.build_list(:contact, 1, member: nil)
    end

    trait :admin do
      admin true
    end

    trait :blocked do
      blocked true
    end

    trait :published do
      published true
    end

    trait :new do
      updating_password false
      password nil

      after :create do |member, evaluator|
        member.create_default_password
        member.save!
      end
    end
  end
end
