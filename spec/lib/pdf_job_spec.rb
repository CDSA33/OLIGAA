#
# OLIGAA - Stands for "Outil en Ligne d'Information et de Gestion des Activités
# des Associations" which is an online tool for associations to manage news,
# actors and activities.
# Copyright (C) 2010-2019 Comité Départemental du Sport Adapté de la Gironde (CDSA
# 33) <contact@cdsa33.org>
#
# This file is part of OLIGAA.
#
# OLIGAA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OLIGAA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with OLIGAA.  If not, see <http://www.gnu.org/licenses/>.
#
require 'rails_helper'

describe PdfJob do
  before do
    @pdf_job = PdfJob.new("http://localhost/newsletters/1", "#{ENV['APP_NAME']} - ma_newsletter.pdf")
  end

  it "is run only once" do
    expect(@pdf_job.max_attempts).to eq(1)
  end

  it "launches PDFKit with good args" do
    # Given
    # Fake PDFKit
    pdfkit = double "PDFKit"
    allow(pdfkit).to receive(:to_file).and_return(true)
    # Make PDFKit think it is well configured
    allow(PDFKit.configuration).to receive(:wkhtmltopdf).and_return(File.dirname(__FILE__))
    allow(PDFKit).to receive(:new).and_return(pdfkit)

    # Then
    expect(PDFKit).to receive(:new).with("http://localhost/newsletters/1")
    expect(pdfkit).to receive(:to_file).with(Pathname.new("#{Rails.root}/public/system/pdfs/newsletters/#{ENV['APP_NAME']} - ma_newsletter.pdf"))

    # When
    @pdf_job.perform
  end
end
