/* French translation for the jQuery Timepicker Addon */
/* Written by Thomas Lété */
(function($) {
	$.timepicker.regional['fr'] = {
		timeOnlyTitle: 'Choisir une heure',
		timeText: 'Heure',
		hourText: 'Heures',
		minuteText: 'Minutes',
		secondText: 'Secondes',
		millisecText: 'Millisecondes',
		timezoneText: 'Fuseau horaire',
		currentText: 'Maintenant',
		closeText: 'Terminé',
		amNames: ['AM', 'A'],
		pmNames: ['PM', 'P'],
		timeFormat: 'HH:mm',
		timeSuffix: '',
		isRTL: false
	};
	$.timepicker.setDefaults($.timepicker.regional['fr']);
})(jQuery);
